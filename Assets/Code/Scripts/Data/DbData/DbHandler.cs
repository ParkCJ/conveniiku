using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DbHandler
{
    // Masterdata
    public DefDataMst DefDataMst { get; private set; }
    public Dictionary<int, StageMst> StageMstDic { get; private set; }
    public Dictionary<string,  Dictionary<string, int>> BlockMstDic { get; private set; }
    public Dictionary<string, ItemMst> ItemMstDic { get; private set; }
    public Dictionary<string, Dictionary<string, int>> StageItemMstDic { get; private set; }
    public Dictionary<string, SweetMst> SweetMstDic { get; private set; }
    public Dictionary<GameMode, Dictionary<string, bool>> StageSweetMstDic { get; private set; }
    public Dictionary<string, Dictionary<string, int>> ObstMstDic { get; private set; }
    public Dictionary<int, Dictionary<Language, string>> LocMstDic { get; private set; }
    public Dictionary<string, Dictionary<string, Dictionary<Language, UITextMst>>> UITextDic { get; private set; }

    public DbHandler(Font jaDefaultFont, Font enDefaultFont, Font koDefaultFont, Font jaFont, Font enFont, Font koFont)
    {
        DefDataMst = DefDataMst.GetData();
        StageMstDic = StageMst.GetDic();
        BlockMstDic = BlockMst.GetDic();
        ItemMstDic = ItemMst.GetDic();
        StageItemMstDic = ItemMst.GetStageDic();
        SweetMstDic = SweetMst.GetDic();
        StageSweetMstDic = SweetMst.GetStageDic();
        ObstMstDic = ObstMst.GetDic();
        LocMstDic = LocMst.GetData();
        UITextDic = UITextMst.GetData(DefDataMst, jaDefaultFont, enDefaultFont, koDefaultFont, jaFont, enFont, koFont);
    }
}