using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefDataMst
{
    public int DefLifeCnt { get; private set; }
    public float DefSpeedGr { get; private set; }
    public float DefJumpForce { get; private set; }
    public int DefBombCnt { get; private set; }
    public int DefMaxLifeCnt { get; private set; }
    public int DefMaxMoney { get; private set; }
    public int DefMaxScore { get; private set; }
    public int DefMaxBlock { get; private set; }
    public int DefMaxBomb { get; private set; }
    public string SaveDataPath { get; private set; }
    public int ContinueMoneyCnt { get; private set; }

    public static DefDataMst GetData()
    {
        DefDataMst defDataMst = new DefDataMst();
        defDataMst.DefLifeCnt = 3;
        defDataMst.DefSpeedGr = 1;
        defDataMst.DefJumpForce = 225f;
        defDataMst.DefBombCnt = 2;
        defDataMst.DefMaxLifeCnt = 99;
        defDataMst.DefMaxMoney = 999;
        defDataMst.DefMaxScore = 999999;
        defDataMst.DefMaxBlock = 999999;
        defDataMst.DefMaxBomb = 99;
        defDataMst.SaveDataPath = Application.persistentDataPath + "/conveniikuReboot.json";
        defDataMst.ContinueMoneyCnt = 1;

        return defDataMst;
    }
}