using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstMst
{
    public string Name { get; private set; }
    public int MaxCnt { get; set; }

    public ObstMst(string name, int maxCnt)
    {
        this.Name = name;
        this.MaxCnt = maxCnt;
    }

    public static Dictionary<string, Dictionary<string, int>> GetDic()
    {
        Dictionary<string, Dictionary<string, int>> dic = new Dictionary<string, Dictionary<string, int>>()
        {
            {
                "Tutorial", new Dictionary<string, int>()
                {
                    { "Cow", 0 },
                    { "Fox", 0 },
                    { "Kite", 0 },
                    { "Chick", 0 },
                    { "Dragon", 0 },
                    { "HammerObst", 0}
                }
            },
            {
                "Arcade1", new Dictionary<string, int>()
                {
                    { "Cow", 2 },
                    { "Fox", 0 },
                    { "Kite", 0 },
                    { "Chick", 0 },
                    { "Dragon", 0 },
                    { "HammerObst", 0}
                }
            },
            {
                "Arcade2", new Dictionary<string, int>()
                {
                    { "Cow", 0 },
                    { "Fox", 3 },
                    { "Kite", 0 },
                    { "Chick", 0 },
                    { "Dragon", 0 },
                    { "HammerObst", 0}
                }
            },
            {
                "Arcade3", new Dictionary<string, int>()
                {
                    { "Cow", 0 },
                    { "Fox", 0 },
                    { "Kite", 4 },
                    { "Chick", 0 },
                    { "Dragon", 0 },
                    { "HammerObst", 0}
                }
            },
            {
                "Arcade4", new Dictionary<string, int>()
                {
                    { "Cow", 0 },
                    { "Fox", 0 },
                    { "Kite", 3 },
                    { "Chick", 0 },
                    { "Dragon", 0 },
                    { "HammerObst", 3}
                }
            },
            {
                "Arcade5", new Dictionary<string, int>()
                {
                    { "Cow", 0 },
                    { "Fox", 0 },
                    { "Kite", 0 },
                    { "Chick", 5 },
                    { "Dragon", 0 },
                    { "HammerObst", 0}
                }
            },
            {
                "Arcade6", new Dictionary<string, int>()
                {
                    { "Cow", 2 },
                    { "Fox", 2 },
                    { "Kite", 2 },
                    { "Chick", 2 },
                    { "Dragon", 0 },
                    { "HammerObst", 0}
                }
            },
            {
                "Arcade7", new Dictionary<string, int>()
                {
                    { "Cow", 0 },
                    { "Fox", 0 },
                    { "Kite", 5 },
                    { "Chick", 0 },
                    { "Dragon", 1 },
                    { "HammerObst", 0}
                }
            },
            {
                "Arcade8", new Dictionary<string, int>()
                {
                    { "Cow", 3 },
                    { "Fox", 3 },
                    { "Kite", 3 },
                    { "Chick", 3 },
                    { "Dragon", 0 },
                    { "HammerObst", 0}
                }
            },
            {
                "MyStage", new Dictionary<string, int>()
                {
                    { "Cow", 0 },
                    { "Fox", 0 },
                    { "Kite", 0 },
                    { "Chick", 0 },
                    { "Dragon", 0 },
                    { "HammerObst", 0}
                }
            },
            {
                "Mission1", new Dictionary<string, int>()
                {
                    { "Cow", 2 },
                    { "Fox", 2 },
                    { "Kite", 1 },
                    { "Chick", 2 },
                    { "Dragon", 0 },
                    { "HammerObst", 0}
                }
            },
            {
                "Mission2", new Dictionary<string, int>()
                {
                    { "Cow", 1 },
                    { "Fox", 1 },
                    { "Kite", 0 },
                    { "Chick", 1 },
                    { "Dragon", 0 },
                    { "HammerObst", 0}
                }
            },
            {
                "Mission3", new Dictionary<string, int>()
                {
                    { "Cow", 5 },
                    { "Fox", 5 },
                    { "Kite", 5 },
                    { "Chick", 5 },
                    { "Dragon", 0 },
                    { "HammerObst", 0}
                }
            },
            {
                "Mission4", new Dictionary<string, int>()
                {
                    { "Cow", 0 },
                    { "Fox", 0 },
                    { "Kite", 30 },
                    { "Chick", 0 },
                    { "Dragon", 0 },
                    { "HammerObst", 0}
                }
            },
            {
                "Mission5", new Dictionary<string, int>()
                {
                    { "Cow", 30 },
                    { "Fox", 0 },
                    { "Kite", 0 },
                    { "Chick", 0 },
                    { "Dragon", 0 },
                    { "HammerObst", 0}
                }
            },
            {
                "Mission6", new Dictionary<string, int>()
                {
                    { "Cow", 5 },
                    { "Fox", 5 },
                    { "Kite", 5 },
                    { "Chick", 5 },
                    { "Dragon", 1 },
                    { "HammerObst", 0}
                }
            },
            {
                "Mission7", new Dictionary<string, int>()
                {
                    { "Cow", 0 },
                    { "Fox", 0 },
                    { "Kite", 0 },
                    { "Chick", 0 },
                    { "Dragon", 0 },
                    { "HammerObst", 0}
                }
            }
        };

        return dic;
    }
}
