using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageMst
{
    public int Idx { get; set; }
    public string Name { get; private set; }
    public int LocMStId { get; private set; }
    public int XSize { get; set; }
    public int ZSize { get; set; }
    public float ObstBlockRate { get; set; }
    public FourTime FourTime { get; set; }
    public float WindStrength { get; set; }
    public int CloudCnt { get; set; }
    public bool Effect { get; set; }
    public int Next { get; private set; }

    public StageMst(int idx, string name, int locMStId, int xSize, int zSize, float obstBlockRate, FourTime fourTime, float windStrength, int cloudCnt, bool effect, int next)
    {
        this.Idx = idx;
        this.Name = name;
        this.LocMStId = locMStId;
        this.XSize = xSize;
        this.ZSize = zSize;
        this.ObstBlockRate = obstBlockRate;
        this.FourTime = fourTime;
        this.WindStrength = windStrength;
        this.CloudCnt = cloudCnt;
        this.Effect = effect;
        this.Next = next;
    }

    public static Dictionary<int, StageMst> GetDic()
    {
        Dictionary<int, StageMst> dic = new Dictionary<int, StageMst>()
        {
            {1, new StageMst(1, "Tutorial", -1, 5, 5, 0, FourTime.Morning, 0, 0, false, -1) },
            {100, new StageMst(100, "Arcade1", 901, 7, 7, 0.3f, FourTime.Morning, 3, 30, false, 101) },
            {101, new StageMst(101, "Arcade2", 902, 7, 7, 0.4f, FourTime.Afternoon, 3, 30, false, 102) },
            {102, new StageMst(102, "Arcade3", 903, 7, 7, 0.5f, FourTime.Evening, 3, 30, false, 103) },
            {103, new StageMst(103, "Arcade4", 904, 1, 30, 1f, FourTime.Night, 7, 50, false, 104) },
            {104, new StageMst(104, "Arcade5", 905, 10, 10, 0.7f, FourTime.Morning, 5, 50, false, 105) },
            {105, new StageMst(105, "Arcade6", 906, 12, 12, 0.8f, FourTime.Afternoon, 5, 75, false, 106) },
            {106, new StageMst(106, "Arcade7", 907, 15, 15, 0.9f, FourTime.Evening, 7, 75, false, 107) },
            {107, new StageMst(107, "Arcade8", 908, 15, 15, 1, FourTime.Night, 10, 100, false, 108) },
            {108, new StageMst(108, "Shop", 909, 1, 1, 0, FourTime.Morning, 0, 0, false, -1) },
            {300, new StageMst(300, "MyStage", -1, 5, 5, 1, FourTime.Morning, 3, 75, false, -1) },
            {500, new StageMst(500, "Mission1", 910, 7, 7, 1, FourTime.Morning, 3, 75, true, -1) },
            {501, new StageMst(501, "Mission2", 911, 15, 1, 1, FourTime.Afternoon, 10, 100, false, -1) },
            {502, new StageMst(502, "Mission3", 912, 30, 30, 1, FourTime.Evening, 7, 50, false, -1) },
            {503, new StageMst(503, "Mission4", 913, 15, 15, 1, FourTime.Night, 3, 50, false, -1) },
            {504, new StageMst(504, "Mission5", 914, 15, 15, 1, FourTime.Morning, 3, 50, false, -1) },
            {505, new StageMst(505, "Mission6", 915, 2, 50, 1, FourTime.Afternoon, 10, 50, false, -1) },
            {506, new StageMst(506, "Mission7", 916, 30, 30, 1, FourTime.Evening, 100, 100, false, -1) },
            {1000, new StageMst(1000, "Test1", -1, 15, 15, 0, FourTime.Morning, 3, 75, false, -1) }
        };

        return dic;
    }
}