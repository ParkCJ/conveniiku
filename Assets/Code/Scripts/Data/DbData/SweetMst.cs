using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SweetMst
{
    public string Name { get; private set; }
    public int LocMStId { get; private set; }
    public int CoinCnt { get; private set; }
    public Ability Ability { get; private set; }
    public float AbilityCnt { get; private set; }

    public SweetMst(string name, int LocMStId, int coinCnt, Ability ability, float abilityCnt)
    {
        this.Name = name;
        this.LocMStId = LocMStId;
        this.CoinCnt = coinCnt;
        this.Ability = ability;
        this.AbilityCnt = abilityCnt;
    }

    public static Dictionary<string, SweetMst> GetDic()
    {
        Dictionary<string, SweetMst> dic = new Dictionary<string, SweetMst>()
        {
            { "Snack", new SweetMst("Snack", 2001, 1, Ability.None, 1) },
            { "Bread", new SweetMst("Bread", 2011, 1, Ability.None, 1) },
            { "Ice cream", new SweetMst("Ice cream", 2021, 1, Ability.None, 1) },
            { "Candy", new SweetMst("Candy", 2031, 1, Ability.None, 1) },
            { "Chocolate", new SweetMst("Chocolate", 2041, 1, Ability.None, 1) },
            { "Pudding", new SweetMst("Pudding", 2051, 1, Ability.None, 1) },
            { "Milk", new SweetMst("Milk", 2061, 1, Ability.None, 1) },
            { "Juice", new SweetMst("Juice", 2071, 10, Ability.Life, 1) },
            { "Jelly", new SweetMst("Jelly", 2081, 10, Ability.Jump, 1) },
            { "Apple pie", new SweetMst("Apple pie", 2091, 10, Ability.Speed, 0.1f) },
            { "Macaron", new SweetMst("Macaron", 2101, 50, Ability.Bomb, 1) },
            { "Churros", new SweetMst("Churros", 2111, 100, Ability.MoveAir, 1) },
            { "Cake", new SweetMst("Cake", 2121, 100, Ability.MyStage, 1) }
        };

        return dic;
    }

    public static Dictionary<GameMode, Dictionary<string, bool>> GetStageDic()
    {
        Dictionary<GameMode, Dictionary<string, bool>> dic = new Dictionary<GameMode, Dictionary<string, bool>>()
        {
            {
                GameMode.Arcade, new Dictionary<string, bool>()
                {
                    { "Snack", true },
                    { "Bread", true },
                    { "Ice cream", true },
                    { "Candy", true },
                    { "Chocolate", true },
                    { "Pudding", true },
                    { "Milk", true },
                    { "Juice", true },
                    { "Jelly", true },
                    { "Apple pie", true },
                    { "Macaron", true },
                    { "Churros", true },
                    { "Cake", true }
                }
            },
            {
                GameMode.Tour, new Dictionary<string, bool>()
                {
                    { "Snack", true },
                    { "Bread", true },
                    { "Ice cream", true },
                    { "Candy", true },
                    { "Chocolate", true },
                    { "Pudding", true },
                    { "Milk", true },
                    { "Juice", true },
                    { "Jelly", true },
                    { "Apple pie", true },
                    { "Macaron", true },
                    { "Churros", true },
                    { "Cake", true }
                }
            },
            {
                GameMode.MyStage, new Dictionary<string, bool>()
                {
                    { "Snack", true },
                    { "Bread", true },
                    { "Ice cream", true },
                    { "Candy", true },
                    { "Chocolate", true },
                    { "Pudding", true },
                    { "Milk", true },
                    { "Juice", true },
                    { "Jelly", true },
                    { "Apple pie", true },
                    { "Macaron", true },
                    { "Churros", true },
                    { "Cake", true }
                }
            }
        };

        return dic;
    }
}