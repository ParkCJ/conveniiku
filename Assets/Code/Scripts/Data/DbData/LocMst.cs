﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocMst
{
    public static Dictionary<int, Dictionary<Language, string>> GetData()
    {
        Dictionary<int, Dictionary<Language, string>> locMstDic = new Dictionary<int, Dictionary<Language, string>>();

        /******************************************
        * 900 ~ 1000: Stage
        * 1000 ~ 1100: Block
        * 1101 ~ 1200: Obstacle
        * 1201 ~ 1300: Item
        * 2001 ~ 3000: Shop
        * 
        * 10001 ~ 10100: OptionSlider
        ******************************************/
        // Stage
        locMstDic.Add(901, new Dictionary<Language, string>());
        locMstDic[901][Language.JA] = "牛牧場";
        locMstDic[901][Language.EN] = "Cow Farm";
        locMstDic[901][Language.KO] = "소 목장";

        locMstDic.Add(902, new Dictionary<Language, string>());
        locMstDic[902][Language.JA] = "狐の森";
        locMstDic[902][Language.EN] = "Fox Forest";
        locMstDic[902][Language.KO] = "여우의 숲";

        locMstDic.Add(903, new Dictionary<Language, string>());
        locMstDic[903][Language.JA] = "鳶絶壁";
        locMstDic[903][Language.EN] = "Kite Cliff";
        locMstDic[903][Language.KO] = "솔개 절벽";

        locMstDic.Add(904, new Dictionary<Language, string>());
        locMstDic[904][Language.JA] = "揺れる橋";
        locMstDic[904][Language.EN] = "Swing Bridge";
        locMstDic[904][Language.KO] = "흔들다리";

        locMstDic.Add(905, new Dictionary<Language, string>());
        locMstDic[905][Language.JA] = "鳥小屋";
        locMstDic[905][Language.EN] = "Chicken Coop";
        locMstDic[905][Language.KO] = "닭장";

        locMstDic.Add(906, new Dictionary<Language, string>());
        locMstDic[906][Language.JA] = "野生動物区域";
        locMstDic[906][Language.EN] = "Wildlife Area";
        locMstDic[906][Language.KO] = "야생동물구역";

        locMstDic.Add(907, new Dictionary<Language, string>());
        locMstDic[907][Language.JA] = "ドラゴンの巣";
        locMstDic[907][Language.EN] = "Dragon Nest";
        locMstDic[907][Language.KO] = "드래곤의 둥지";

        locMstDic.Add(908, new Dictionary<Language, string>());
        locMstDic[908][Language.JA] = "活火山地帯";
        locMstDic[908][Language.EN] = "Volcanic Field";
        locMstDic[908][Language.KO] = "활화산 지대";

        locMstDic.Add(909, new Dictionary<Language, string>());
        locMstDic[909][Language.JA] = "コンビニ";
        locMstDic[909][Language.EN] = "Convenience store";
        locMstDic[909][Language.KO] = "편의점";

        locMstDic.Add(910, new Dictionary<Language, string>());
        locMstDic[910][Language.JA] = "宝島";
        locMstDic[910][Language.EN] = "Treasure Island";
        locMstDic[910][Language.KO] = "보물섬";

        locMstDic.Add(911, new Dictionary<Language, string>());
        locMstDic[911][Language.JA] = "イルカ島";
        locMstDic[911][Language.EN] = "Dolphin Island";
        locMstDic[911][Language.KO] = "돌고래섬";

        locMstDic.Add(912, new Dictionary<Language, string>());
        locMstDic[912][Language.JA] = "魚島";
        locMstDic[912][Language.EN] = "Fish Island";
        locMstDic[912][Language.KO] = "물고기섬";

        locMstDic.Add(913, new Dictionary<Language, string>());
        locMstDic[913][Language.JA] = "ハート島";
        locMstDic[913][Language.EN] = "Heart Island";
        locMstDic[913][Language.KO] = "하트섬";

        locMstDic.Add(914, new Dictionary<Language, string>());
        locMstDic[914][Language.JA] = "牛島";
        locMstDic[914][Language.EN] = "Cow Island";
        locMstDic[914][Language.KO] = "소 섬";

        locMstDic.Add(915, new Dictionary<Language, string>());
        locMstDic[915][Language.JA] = "研究所橋";
        locMstDic[915][Language.EN] = "Lab Bridge";
        locMstDic[915][Language.KO] = "연구소 다리";

        locMstDic.Add(916, new Dictionary<Language, string>());
        locMstDic[916][Language.JA] = "研究所";
        locMstDic[916][Language.EN] = "Lab";
        locMstDic[916][Language.KO] = "연구소";

        // Block
        locMstDic.Add(1001, new Dictionary<Language, string>());
        locMstDic[1001][Language.JA] = "ジャンプキューブ";
        locMstDic[1001][Language.EN] = "Jump cube";
        locMstDic[1001][Language.KO] = "점프 상자";

        locMstDic.Add(1002, new Dictionary<Language, string>());
        locMstDic[1002][Language.JA] = "落下キューブ";
        locMstDic[1002][Language.EN] = "Falling cube";
        locMstDic[1002][Language.KO] = "낙하 상자";

        locMstDic.Add(1003, new Dictionary<Language, string>());
        locMstDic[1003][Language.JA] = "トランポリンキューブ";
        locMstDic[1003][Language.EN] = "Trampoline cube";
        locMstDic[1003][Language.KO] = "트램플린 상자";

        locMstDic.Add(1004, new Dictionary<Language, string>());
        locMstDic[1004][Language.JA] = "火キューブ";
        locMstDic[1004][Language.EN] = "Fire cube";
        locMstDic[1004][Language.KO] = "불 상자";

        locMstDic.Add(1005, new Dictionary<Language, string>());
        locMstDic[1005][Language.JA] = "氷キューブ";
        locMstDic[1005][Language.EN] = "Ice cube";
        locMstDic[1005][Language.KO] = "얼음 상자";

        locMstDic.Add(1007, new Dictionary<Language, string>());
        locMstDic[1007][Language.JA] = "動物キューブ";
        locMstDic[1007][Language.EN] = "Animal cube";
        locMstDic[1007][Language.KO] = "동물 상자";

        locMstDic.Add(1008, new Dictionary<Language, string>());
        locMstDic[1008][Language.JA] = "鳥キューブ";
        locMstDic[1008][Language.EN] = "Bird cube";
        locMstDic[1008][Language.KO] = "새 상자";

        locMstDic.Add(1009, new Dictionary<Language, string>());
        locMstDic[1009][Language.JA] = "無重力キューブ";
        locMstDic[1009][Language.EN] = "Zero Gravity cube";
        locMstDic[1009][Language.KO] = "무중력 상자";

        locMstDic.Add(1010, new Dictionary<Language, string>());
        locMstDic[1010][Language.JA] = "ハンマーキューブ";
        locMstDic[1010][Language.EN] = "Hammer cube";
        locMstDic[1010][Language.KO] = "망치 상자";

        locMstDic.Add(1011, new Dictionary<Language, string>());
        locMstDic[1011][Language.JA] = "ドラゴンキューブ";
        locMstDic[1011][Language.EN] = "Dragon cube";
        locMstDic[1011][Language.KO] = "용 상자";

        locMstDic.Add(1012, new Dictionary<Language, string>());
        locMstDic[1012][Language.JA] = "火山キューブ";
        locMstDic[1012][Language.EN] = "Volcano cube";
        locMstDic[1012][Language.KO] = "화산 상자";

        // Obstacle
        locMstDic.Add(1101, new Dictionary<Language, string>());
        locMstDic[1101][Language.JA] = "牛";
        locMstDic[1101][Language.EN] = "Cow";
        locMstDic[1101][Language.KO] = "소";

        locMstDic.Add(1102, new Dictionary<Language, string>());
        locMstDic[1102][Language.JA] = "キツネ";
        locMstDic[1102][Language.EN] = "Fox";
        locMstDic[1102][Language.KO] = "여우";

        locMstDic.Add(1103, new Dictionary<Language, string>());
        locMstDic[1103][Language.JA] = "ひよこ";
        locMstDic[1103][Language.EN] = "Chick";
        locMstDic[1103][Language.KO] = "병아리";

        locMstDic.Add(1104, new Dictionary<Language, string>());
        locMstDic[1104][Language.JA] = "トビ";
        locMstDic[1104][Language.EN] = "Kite";
        locMstDic[1104][Language.KO] = "솔개";

        locMstDic.Add(1105, new Dictionary<Language, string>());
        locMstDic[1105][Language.JA] = "ドラゴン";
        locMstDic[1105][Language.EN] = "Dragon";
        locMstDic[1105][Language.KO] = "용";

        locMstDic.Add(1106, new Dictionary<Language, string>());
        locMstDic[1106][Language.JA] = "ハンマー";
        locMstDic[1106][Language.EN] = "Hammer";
        locMstDic[1106][Language.KO] = "망치";

        // Item
        locMstDic.Add(1201, new Dictionary<Language, string>());
        locMstDic[1201][Language.JA] = "コイン";
        locMstDic[1201][Language.EN] = "Coin";
        locMstDic[1201][Language.KO] = "동전";

        locMstDic.Add(1202, new Dictionary<Language, string>());
        locMstDic[1202][Language.JA] = "ステーキ";
        locMstDic[1202][Language.EN] = "Steak";
        locMstDic[1202][Language.KO] = "스테이크";

        locMstDic.Add(1203, new Dictionary<Language, string>());
        locMstDic[1203][Language.JA] = "骨";
        locMstDic[1203][Language.EN] = "Bone";
        locMstDic[1203][Language.KO] = "뼈";

        locMstDic.Add(1204, new Dictionary<Language, string>());
        locMstDic[1204][Language.JA] = "リンゴ";
        locMstDic[1204][Language.EN] = "Apple";
        locMstDic[1204][Language.KO] = "사과";

        locMstDic.Add(1205, new Dictionary<Language, string>());
        locMstDic[1205][Language.JA] = "バナナ";
        locMstDic[1205][Language.EN] = "Banana";
        locMstDic[1205][Language.KO] = "바나나";

        locMstDic.Add(1206, new Dictionary<Language, string>());
        locMstDic[1206][Language.JA] = "ピザ";
        locMstDic[1206][Language.EN] = "Pizza";
        locMstDic[1206][Language.KO] = "피자";

        locMstDic.Add(1207, new Dictionary<Language, string>());
        locMstDic[1207][Language.JA] = "うんこ";
        locMstDic[1207][Language.EN] = "Poo";
        locMstDic[1207][Language.KO] = "똥";

        // Sweets
        locMstDic.Add(2001, new Dictionary<Language, string>());
        locMstDic[2001][Language.JA] = "お菓子";
        locMstDic[2001][Language.EN] = "Snack";
        locMstDic[2001][Language.KO] = "과자";

        locMstDic.Add(2011, new Dictionary<Language, string>());
        locMstDic[2011][Language.JA] = "パン";
        locMstDic[2011][Language.EN] = "Bread";
        locMstDic[2011][Language.KO] = "빵";

        locMstDic.Add(2021, new Dictionary<Language, string>());
        locMstDic[2021][Language.JA] = "アイス";
        locMstDic[2021][Language.EN] = "Ice cream";
        locMstDic[2021][Language.KO] = "아이스크림";

        locMstDic.Add(2031, new Dictionary<Language, string>());
        locMstDic[2031][Language.JA] = "キャンディ";
        locMstDic[2031][Language.EN] = "Candy";
        locMstDic[2031][Language.KO] = "캔디";

        locMstDic.Add(2041, new Dictionary<Language, string>());
        locMstDic[2041][Language.JA] = "チョコレート";
        locMstDic[2041][Language.EN] = "Chocolate";
        locMstDic[2041][Language.KO] = "초콜렛";

        locMstDic.Add(2051, new Dictionary<Language, string>());
        locMstDic[2051][Language.JA] = "プリン";
        locMstDic[2051][Language.EN] = "Pudding";
        locMstDic[2051][Language.KO] = "푸딩";

        locMstDic.Add(2061, new Dictionary<Language, string>());
        locMstDic[2061][Language.JA] = "牛乳";
        locMstDic[2061][Language.EN] = "Milk";
        locMstDic[2061][Language.KO] = "우유";

        locMstDic.Add(2071, new Dictionary<Language, string>());
        locMstDic[2071][Language.JA] = "ジュース";
        locMstDic[2071][Language.EN] = "Juice";
        locMstDic[2071][Language.KO] = "주스";

        locMstDic.Add(2081, new Dictionary<Language, string>());
        locMstDic[2081][Language.JA] = "ゼリー";
        locMstDic[2081][Language.EN] = "Jelly";
        locMstDic[2081][Language.KO] = "젤리";

        locMstDic.Add(2091, new Dictionary<Language, string>());
        locMstDic[2091][Language.JA] = "アップルパイ";
        locMstDic[2091][Language.EN] = "Apple Pie";
        locMstDic[2091][Language.KO] = "애플파이";

        locMstDic.Add(2101, new Dictionary<Language, string>());
        locMstDic[2101][Language.JA] = "マカロン";
        locMstDic[2101][Language.EN] = "Macaron";
        locMstDic[2101][Language.KO] = "마카롱";

        locMstDic.Add(2111, new Dictionary<Language, string>());
        locMstDic[2111][Language.JA] = "チュロス";
        locMstDic[2111][Language.EN] = "Churros";
        locMstDic[2111][Language.KO] = "츄러스";

        locMstDic.Add(2121, new Dictionary<Language, string>());
        locMstDic[2121][Language.JA] = "ケーキ";
        locMstDic[2121][Language.EN] = "Cake";
        locMstDic[2121][Language.KO] = "케잌";

        locMstDic.Add(2072, new Dictionary<Language, string>());
        locMstDic[2072][Language.JA] = "ハート";
        locMstDic[2072][Language.EN] = "Heart";
        locMstDic[2072][Language.KO] = "하트";

        locMstDic.Add(2082, new Dictionary<Language, string>());
        locMstDic[2082][Language.JA] = "ジャンプ";
        locMstDic[2082][Language.EN] = "Jump";
        locMstDic[2082][Language.KO] = "점프";

        locMstDic.Add(2092, new Dictionary<Language, string>());
        locMstDic[2092][Language.JA] = "スピード";
        locMstDic[2092][Language.EN] = "Speed";
        locMstDic[2092][Language.KO] = "속도";

        locMstDic.Add(2102, new Dictionary<Language, string>());
        locMstDic[2102][Language.JA] = "爆弾";
        locMstDic[2102][Language.EN] = "Bomb";
        locMstDic[2102][Language.KO] = "폭탄";

        locMstDic.Add(2112, new Dictionary<Language, string>());
        locMstDic[2112][Language.JA] = "空中移動";
        locMstDic[2112][Language.EN] = "Move In Air";
        locMstDic[2112][Language.KO] = "공중 이동";

        locMstDic.Add(2122, new Dictionary<Language, string>());
        locMstDic[2122][Language.JA] = "マイステージ";
        locMstDic[2122][Language.EN] = "My Stage";
        locMstDic[2122][Language.KO] = "나의 스테이지";

        // OptionSlider
        locMstDic.Add(10001, new Dictionary<Language, string>());
        locMstDic[10001][Language.JA] = "命";
        locMstDic[10001][Language.EN] = "Life";
        locMstDic[10001][Language.KO] = "생명";

        locMstDic.Add(10002, new Dictionary<Language, string>());
        locMstDic[10002][Language.JA] = "ステージサイズ X";
        locMstDic[10002][Language.EN] = "Stage size X";
        locMstDic[10002][Language.KO] = "스테이지 크기 X";

        locMstDic.Add(10003, new Dictionary<Language, string>());
        locMstDic[10003][Language.JA] = "ステージサイズ Z";
        locMstDic[10003][Language.EN] = "Stage size Z";
        locMstDic[10003][Language.KO] = "스테이지 크기 Z";

        locMstDic.Add(10004, new Dictionary<Language, string>());
        locMstDic[10004][Language.JA] = "反応キューブ確率";
        locMstDic[10004][Language.EN] = "React cube rate";
        locMstDic[10004][Language.KO] = "반응 큐브 확률";

        return locMstDic;
    }
}