﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UITextMst
{
    public string Text { get; set; }
    public Font Font { get; set; }

    public UITextMst(string text, Font font)
    {
        this.Text = text;
        this.Font = font;
    }

    public static Dictionary<Language, UITextMst> CreateLangSet(string jaText, string enText, string koText, Font jaFont, Font enFont, Font koFont)
    {
        Dictionary<Language, UITextMst> langSet = new Dictionary<Language, UITextMst>();
        langSet[Language.JA] = new UITextMst(jaText, jaFont);
        langSet[Language.EN] = new UITextMst(enText, enFont);
        langSet[Language.KO] = new UITextMst(koText, koFont);

        return langSet;
    }

    public static Dictionary<string, Dictionary<string, Dictionary<Language, UITextMst>>> GetData(DefDataMst defDataMst, Font jaDefaultFont, Font enDefaultFont, Font koDefaultFont, Font jaFont, Font enFont, Font koFont)
    {
        Dictionary<string, Dictionary<string, Dictionary<Language, UITextMst>>> uITextDic = new Dictionary<string, Dictionary<string, Dictionary<Language, UITextMst>>>();

        string sceneNm = "Title";
        uITextDic[sceneNm] = new Dictionary<string, Dictionary<Language, UITextMst>>();
        uITextDic[sceneNm]["TitleText"] = UITextMst.CreateLangSet("コンビニ\r\n行く", "I'M GOING SHOPPING", "편의점\r\n간다", jaFont, enFont, koFont);
        uITextDic[sceneNm]["StartBtn"] = UITextMst.CreateLangSet("スタート", "START", "시작하기", jaFont, enFont, koFont);
        uITextDic[sceneNm]["ArcadeBtn"] = UITextMst.CreateLangSet("アーケード", "Arcade", "아케이드", jaFont, enFont, koFont);
        uITextDic[sceneNm]["TourBtn"] = UITextMst.CreateLangSet("旅行", "Tour", "여행", jaFont, enFont, koFont);
        uITextDic[sceneNm]["MyStageBtn"] = UITextMst.CreateLangSet("マイステージ", "My stage", "나의 스테이지", jaFont, enFont, koFont);
        uITextDic[sceneNm]["BackBtn"] = UITextMst.CreateLangSet("<- 戻る", "<- BACK", "<- 돌아가기", jaFont, enFont, koFont);
        uITextDic[sceneNm]["ConfigBtn"] = UITextMst.CreateLangSet("設定", "Config", "설정", jaFont, enFont, koFont);

        uITextDic[sceneNm]["ModeBtn"] = UITextMst.CreateLangSet("モード", "Mode", "모드", jaFont, enFont, koFont);
        uITextDic[sceneNm]["ModeOriginalBtn"] = UITextMst.CreateLangSet("オリジナル", "ORIGINAL", "기본", jaFont, enFont, koFont);
        uITextDic[sceneNm]["ModeBattleBtn"] = UITextMst.CreateLangSet("戦闘", "BATTLE", "전투", jaFont, enFont, koFont);
        
        uITextDic[sceneNm]["CamBtn"] = UITextMst.CreateLangSet("カメラ", "Camera", "카메라", jaFont, enFont, koFont);
        uITextDic[sceneNm]["CamHRotDesc"] = UITextMst.CreateLangSet("横回転", "H.Rotation", "수평회전", jaFont, enFont, koFont);
        uITextDic[sceneNm]["CamHRotNorBtn"] = UITextMst.CreateLangSet("ノーマル", "NORMAL", "정상", jaFont, enFont, koFont);
        uITextDic[sceneNm]["CamHRotRevBtn"] = UITextMst.CreateLangSet("リバース", "REVERSE", "반대", jaFont, enFont, koFont);
        uITextDic[sceneNm]["CamVRotDesc"] = UITextMst.CreateLangSet("縦回転", "V.Rotation", "수직회전", jaFont, enFont, koFont);
        uITextDic[sceneNm]["CamVRotNorBtn"] = UITextMst.CreateLangSet("ノーマル", "NORMAL", "정상", jaFont, enFont, koFont);
        uITextDic[sceneNm]["CamVRotRevBtn"] = UITextMst.CreateLangSet("リバース", "REVERSE", "반대", jaFont, enFont, koFont);
        uITextDic[sceneNm]["CamShakeDesc"] = UITextMst.CreateLangSet("揺れ", "Shake", "흔들림", jaFont, enFont, koFont);
        uITextDic[sceneNm]["CamShakeRateMin"] = UITextMst.CreateLangSet("0", "0", "0", jaFont, enFont, koFont);
        uITextDic[sceneNm]["CamShakeRateMax"] = UITextMst.CreateLangSet("10", "10", "10", jaFont, enFont, koFont);

        uITextDic[sceneNm]["LangBtn"] = UITextMst.CreateLangSet("言語", "Language", "언어", jaFont, enFont, koFont);
        uITextDic[sceneNm]["LangJaBtn"] = UITextMst.CreateLangSet("日本語", "日本語", "日本語", jaFont, jaFont, jaFont);
        uITextDic[sceneNm]["LangEnBtn"] = UITextMst.CreateLangSet("ENGLISH", "ENGLISH", "ENGLISH", enFont, enFont, enFont);
        uITextDic[sceneNm]["LangKoBtn"] = UITextMst.CreateLangSet("한국어", "한국어", "한국어", koFont, koFont, koFont);
        
        uITextDic[sceneNm]["SaveDataBtn"] = UITextMst.CreateLangSet("データ", "Data", "데이터", jaFont, enFont, koFont);
        uITextDic[sceneNm]["DelSaveDataDesc"] = UITextMst.CreateLangSet("セーブデータを削除しますか？", "DELETE THE SAVE DATA ?", "저장 데이터를 삭제하시겠습니까 ?", jaFont, enFont, koFont);
        uITextDic[sceneNm]["DelSaveDataYesBtn"] = UITextMst.CreateLangSet("はい", "Yes", "네", jaFont, enFont, koFont);
        uITextDic[sceneNm]["DelSaveDataNoBtn"] = UITextMst.CreateLangSet("いいえ", "No", "아니오", jaFont, enFont, koFont);

        sceneNm = "Book";
        uITextDic[sceneNm] = new Dictionary<string, Dictionary<Language, UITextMst>>();
        uITextDic[sceneNm]["PrologueText"] = UITextMst.CreateLangSet("プロローグ", "PROLOGUE", "  프롤로그  ", jaFont, enFont, koFont);
        uITextDic[sceneNm]["BookText"] = UITextMst.CreateLangSet("本", "BOOK", "책", jaFont, enFont, koFont);
        uITextDic[sceneNm]["Page1TopTitleText"] = UITextMst.CreateLangSet("このゲームのひとたち", "CHARACTERS", "게임에 나오는 사람들", jaFont, enFont, koFont);
        uITextDic[sceneNm]["PlayerNameText"] = UITextMst.CreateLangSet("ひより", "Hiyori", "히요리", jaFont, enFont, koFont);
        uITextDic[sceneNm]["PlayerDescText"] = UITextMst.CreateLangSet("遊ぶのが好き\r\n走るのが好き\r\nそして\r\nお菓子が大好き", "Likes playing\r\nLikes running\r\nAnd\r\nLoves sweets", "노는거 좋아\r\n달리기 좋아\r\n근데 과자가 좋아", jaFont, enFont, koFont);
        uITextDic[sceneNm]["ConveniNameText"] = UITextMst.CreateLangSet("コンビニ", "Convenience store", "편의점", jaFont, enFont, koFont);
        uITextDic[sceneNm]["ConveniDescText"] = UITextMst.CreateLangSet("お菓子が多い！\r\nワクワクする！", "A lot of sweets\r\nPit-a-pat", "과자가 많아!\r\n두근♡두근♡", jaFont, enFont, koFont);
        uITextDic[sceneNm]["CowNameText"] = UITextMst.CreateLangSet("牛", "Cow", "소", jaFont, enFont, koFont);
        uITextDic[sceneNm]["CowDescText"] = UITextMst.CreateLangSet("森に住む野生動物。\r\n体が大きくて良く走る。\r\n得意は頭突つき。", "Wild animal in the forest\r\nHeadbutt player", "숲에 사는 야생동물\r\n크고 잘달려\r\n박치기 선수", jaFont, enFont, koFont);
        uITextDic[sceneNm]["FoxNameText"] = UITextMst.CreateLangSet("キツネ", "Fox", "여우", jaFont, enFont, koFont);
        uITextDic[sceneNm]["FoxDescText"] = UITextMst.CreateLangSet("森に住む野生動物。\r\n尻尾が大きい", "Wild animal in the forest\r\nHas big tail", "숲에 사는 야생동물\r\n꼬리가 커", jaFont, enFont, koFont);
        uITextDic[sceneNm]["DragonNameText"] = UITextMst.CreateLangSet("ドラゴン", "Dragon", "용", jaFont, enFont, koFont);
        uITextDic[sceneNm]["DragonDescText"] = UITextMst.CreateLangSet("火を吐く\r\n伝説の動物\r\n鼻口が大きい", "Legendary creature that\r\nbreathes fire\r\nHas big nostril", "불을 뿜는\r\n전설의 동물\r\n콧쿠멍이 커", jaFont, enFont, koFont);
        uITextDic[sceneNm]["KiteNameText"] = UITextMst.CreateLangSet("トビ", "Kite", "솔개", jaFont, enFont, koFont);
        uITextDic[sceneNm]["KiteDescText"] = UITextMst.CreateLangSet("空を飛ぶ動物。\r\nどこで寝ているか分からない。\r\nパンを奪いに襲って来る。", "Flying animal\r\nLoves blue sky\r\nComes to steal bread", "하늘을 나는 동물\r\n어디서 자는 걸까?\r\n빵 뺏아가는 애", jaFont, enFont, koFont);
        uITextDic[sceneNm]["ChickNameText"] = UITextMst.CreateLangSet("ひよこ", "Chick", "병아리", jaFont, enFont, koFont);
        uITextDic[sceneNm]["ChickDescText"] = UITextMst.CreateLangSet("家に住む動物。\r\n得意は秘密！", "Animal living in the house\r\nHas secret!", "집에 사는 동물\r\n왠지 엄청 커\r\n특기는 비밀!", jaFont, enFont, koFont);
        uITextDic[sceneNm]["VolcanoBlockName"] = UITextMst.CreateLangSet("火山キューブ", "Volcano cube", "화산 상자", jaFont, enFont, koFont);
        uITextDic[sceneNm]["VolcanoBlockDesc"] = UITextMst.CreateLangSet("地震を伴う爆発をする。\r\n溶岩を吹き出す。\r\n当たると。。", "It explodes and\r\ncauses\r\nearthquake\r\nLava spouts", "지진을 일으키고 폭발해\r\n용암이 나와\r\n닿으면..", jaFont, enFont, koFont);
        
        uITextDic[sceneNm]["AppleItemName"] = UITextMst.CreateLangSet("りんご", "Apple", "사과", jaFont, enFont, koFont);
        uITextDic[sceneNm]["AppleItemDesc"] = UITextMst.CreateLangSet("赤くて美味しい\r\n命＋１", "Red and delicious\r\nLife + 1", "빨갛고 맛있어\r\n생명 + 1", jaFont, enFont, koFont);
        uITextDic[sceneNm]["SteakItemName"] = UITextMst.CreateLangSet("ステーキ", "Steak", "스테이크", jaFont, enFont, koFont);
        uITextDic[sceneNm]["SteakItemDesc"] = UITextMst.CreateLangSet("肉の味がする\r\n身長ｘ２", "Tastes like meat\r\nHeight x 2", "고기 맛이 나\r\n키 x 2", jaFont, enFont, koFont);
        uITextDic[sceneNm]["BoneItemName"] = UITextMst.CreateLangSet("骨", "Bone", "뼈", jaFont, enFont, koFont);
        uITextDic[sceneNm]["BoneItemDesc"] = UITextMst.CreateLangSet("変な味がする\r\n身長ｘ0.5", "It tastes weird\r\nHeight x 0.5", "맛이 이상해\r\n키 x 0.5", jaFont, enFont, koFont);
        uITextDic[sceneNm]["PooItemName"] = UITextMst.CreateLangSet("うんこ", "Poo", "똥", jaFont, enFont, koFont);
        uITextDic[sceneNm]["PooItemDesc"] = UITextMst.CreateLangSet("うわあああ！\r\n中毒 x 7秒", "UGH!!!!!!!!!!\r\nBecome poisoned x 7 sec", "으아악!\r\n중독 x 7초", jaFont, enFont, koFont);
        uITextDic[sceneNm]["SkipBtn"] = UITextMst.CreateLangSet("  スキップ  ", "     SKIP     ", " 건너뛰기 ", jaFont, enFont, koFont);
        uITextDic[sceneNm]["NextBtn"] = UITextMst.CreateLangSet("  次へ ->  ", "  NEXT ->  ", "    다음 ->    ", jaFont, enFont, koFont);
        uITextDic[sceneNm]["TourBtn"] = UITextMst.CreateLangSet("旅行 ->", "TOUR ->", "여행 ->", jaFont, enFont, koFont);
        
        sceneNm = "Prologue";
        uITextDic[sceneNm] = new Dictionary<string, Dictionary<Language, UITextMst>>();
        uITextDic[sceneNm]["PrologueText"] = UITextMst.CreateLangSet("プロローグ", "PROLOGUE", "  프롤로그  ", jaFont, enFont, koFont);
        uITextDic[sceneNm]["Page3ChatText"] = UITextMst.CreateLangSet("私の名前は\r\nひより。\r\nお菓子が食べたい", "I am Hiyori\r\nI'd like to eat sweets", "내 이름은 히요리\r\n과자가 먹고 싶어", jaFont, enFont, koFont);
        uITextDic[sceneNm]["Page4ChatText"] = UITextMst.CreateLangSet("コンビニ行く", "I'M GOING SHOPPING", "편의점 간다", jaFont, enFont, koFont);
        uITextDic[sceneNm]["SkipBtn"] = UITextMst.CreateLangSet("  スキップ  ", "     SKIP     ", " 건너뛰기 ", jaFont, enFont, koFont);
        uITextDic[sceneNm]["NextBtn"] = UITextMst.CreateLangSet("  次へ ->  ", "  NEXT ->  ", "    다음 ->    ", jaFont, enFont, koFont);
        uITextDic[sceneNm]["TutorialBtn"] = UITextMst.CreateLangSet("チュートリアル", "TUTORIAL", " 게임방법 ", jaFont, enFont, koFont);
        uITextDic[sceneNm]["StartBtn"] = UITextMst.CreateLangSet("スタート", "   START   ", "        시작        ", jaFont, enFont, koFont);

        sceneNm = "Tutorial";
        uITextDic[sceneNm] = new Dictionary<string, Dictionary<Language, UITextMst>>();
        uITextDic[sceneNm]["TutorialText"] = UITextMst.CreateLangSet("チュートリアル", "TUTORIAL", "  게임방법  ", jaFont, enFont, koFont);
        uITextDic[sceneNm]["HowTo"] = UITextMst.CreateLangSet("やって見よー", "Let's try", "해보자", jaFont, enFont, koFont);
        uITextDic[sceneNm]["TutorialMsg"] = UITextMst.CreateLangSet("コンビニに行って見ましょう", "Let's try to go to the convenience store", "편의점에 가보아요", jaFont, enFont, koFont);
        uITextDic[sceneNm]["TutorialSuccessMsg"] = UITextMst.CreateLangSet("成功", "Success", "성공", jaFont, enFont, koFont);
        uITextDic[sceneNm]["MoveDescText"] = UITextMst.CreateLangSet("移動　", " Move ", "   이동   ", jaFont, enFont, koFont);
        uITextDic[sceneNm]["JumpDescText"] = UITextMst.CreateLangSet("ジャンプ", " Jump ", "   점프   ", jaFont, enFont, koFont);
        uITextDic[sceneNm]["AttackDescText"] = UITextMst.CreateLangSet("攻撃", "Attack", "공격", jaFont, enFont, koFont);
        uITextDic[sceneNm]["BombDescText"] = UITextMst.CreateLangSet("爆弾", "Bomb", "폭탄", jaFont, enFont, koFont);
        uITextDic[sceneNm]["MCamChangeDescText"] = UITextMst.CreateLangSet("カメラ変更", "Camera change", "카메라 변경", jaFont, enFont, koFont);
        uITextDic[sceneNm]["MouseDescText"] = UITextMst.CreateLangSet("視点、ズーム", "View, Zoom", "시점, 줌", jaFont, enFont, koFont);
        uITextDic[sceneNm]["PauseDescText"] = UITextMst.CreateLangSet("一時停止", "Pause", "일시정지", jaFont, enFont, koFont);
        uITextDic[sceneNm]["TitleBtn"] = UITextMst.CreateLangSet("タイトルへ", "TO TITLE", "타이틀로", jaFont, enFont, koFont);
        uITextDic[sceneNm]["StartBtn"] = UITextMst.CreateLangSet("スタート", "   START   ", "        시작        ", jaFont, enFont, koFont);

        sceneNm = "Game";
        uITextDic[sceneNm] = new Dictionary<string, Dictionary<Language, UITextMst>>();
        uITextDic[sceneNm]["HeartCnt"] = UITextMst.CreateLangSet("", "", "", jaFont, enFont, koFont);
        uITextDic[sceneNm]["CoinCnt"] = UITextMst.CreateLangSet("", "", "", jaFont, enFont, koFont);
        uITextDic[sceneNm]["FPSCamNameText"] = UITextMst.CreateLangSet("ファースト", "FIRST", "1인칭", jaFont, enFont, koFont);
        uITextDic[sceneNm]["TPSCamNameText"] = UITextMst.CreateLangSet("サード", "THIRD", "3인칭", jaFont, enFont, koFont);
        uITextDic[sceneNm]["FreeCamNameText"] = UITextMst.CreateLangSet("フリー", "FREE", "자유", jaFont, enFont, koFont);
        uITextDic[sceneNm]["FollowCamNameText"] = UITextMst.CreateLangSet("フォロー", "FOLLOW", "추적", jaFont, enFont, koFont);
        uITextDic[sceneNm]["TopCamNameText"] = UITextMst.CreateLangSet("   トップ   ", "   TOP   ", "톱", jaFont, enFont, koFont);
        uITextDic[sceneNm]["ContinueText"] = UITextMst.CreateLangSet("コンティニュー？", "CONTINUE ?", "계속할래?", jaFont, enFont, koFont);
        uITextDic[sceneNm]["SuccessText"] = UITextMst.CreateLangSet("おめでとう", "CONGRATULATIONS", "축하해", jaFont, enFont, koFont);
        uITextDic[sceneNm]["CountText"] = UITextMst.CreateLangSet("10", "10", "10", jaFont, enFont, koFont);
        uITextDic[sceneNm]["ContinueBtn"] = UITextMst.CreateLangSet($"いいよ", $"CONTINUE", $"계속할래", jaFont, enFont, koFont);
        uITextDic[sceneNm]["ContinuePrice"] = UITextMst.CreateLangSet($"{defDataMst.ContinueMoneyCnt}", $"{defDataMst.ContinueMoneyCnt}", $"{defDataMst.ContinueMoneyCnt}", jaFont, enFont, koFont);
        uITextDic[sceneNm]["RestartBtn"] = UITextMst.CreateLangSet("リスタート", " RESTART ", "처음부터", jaFont, enFont, koFont);
        uITextDic[sceneNm]["TitleBtn"] = UITextMst.CreateLangSet("タイトルへ", "TO TITLE", "타이틀로", jaFont, enFont, koFont);
        uITextDic[sceneNm]["MyStageBtn"] = UITextMst.CreateLangSet("マイステージ", "My stage", "나의 스테이지", jaFont, enFont, koFont);
        uITextDic[sceneNm]["TourBtn"] = UITextMst.CreateLangSet("   旅行へ   ", "TO TOUR", "여행으로", jaFont, enFont, koFont);
        uITextDic[sceneNm]["NextBtn"] = UITextMst.CreateLangSet("  次へ ->  ", "  NEXT ->  ", "    다음 ->    ", jaFont, enFont, koFont);
        uITextDic[sceneNm]["NextText"] = UITextMst.CreateLangSet("次へ", "NEXT", "다음", jaFont, enFont, koFont);
        uITextDic[sceneNm]["ShopBtn"] = UITextMst.CreateLangSet("コンビニ ->", "Shop ->", "편의점 ->", jaFont, enFont, koFont);
        uITextDic[sceneNm]["PausedText"] = UITextMst.CreateLangSet("一時停止", "PAUSED", "일시정지됨", jaFont, enFont, koFont);
        uITextDic[sceneNm]["ResumeBtn"] = UITextMst.CreateLangSet("  つづける  ", "RESUME", "돌아가기", jaFont, enFont, koFont);

        sceneNm = "GameNext";
        uITextDic[sceneNm] = new Dictionary<string, Dictionary<Language, UITextMst>>();
        uITextDic[sceneNm]["StageTitle"] = UITextMst.CreateLangSet("", "", "", jaFont, enFont, koFont);
        uITextDic[sceneNm]["SkipBtn"] = UITextMst.CreateLangSet("  スキップ  ", "     SKIP     ", " 건너뛰기 ", jaFont, enFont, koFont);

        sceneNm = "Tour";
        uITextDic[sceneNm] = new Dictionary<string, Dictionary<Language, UITextMst>>();
        uITextDic[sceneNm]["Money"] = UITextMst.CreateLangSet("", "", "", jaFont, enFont, koFont);
        uITextDic[sceneNm]["ScoreLabel"] = UITextMst.CreateLangSet("点数", "SCORE", "점수", jaFont, enFont, koFont);
        uITextDic[sceneNm]["Score"] = UITextMst.CreateLangSet("", "", "", jaFont, enFont, koFont);
        uITextDic[sceneNm]["StageTitle"] = UITextMst.CreateLangSet("", "", "", jaFont, enFont, koFont);
        uITextDic[sceneNm]["TitleBtn"] = UITextMst.CreateLangSet("タイトル", "TITLE", "타이틀", jaFont, enFont, koFont);
        uITextDic[sceneNm]["StartBtn"] = UITextMst.CreateLangSet("スタート", "START", "        시작        ", jaFont, enFont, koFont);

        sceneNm = "MyStage";
        uITextDic[sceneNm] = new Dictionary<string, Dictionary<Language, UITextMst>>();
        uITextDic[sceneNm]["MyStageText"] = UITextMst.CreateLangSet("マイステージ", "MY STAGE", "나의 스테이지", jaFont, enFont, koFont);
        uITextDic[sceneNm]["Area1Text"] = UITextMst.CreateLangSet("プレイ", "Play", "플레이", jaFont, enFont, koFont);
        uITextDic[sceneNm]["Area2Text"] = UITextMst.CreateLangSet("反応ューブ数", "React cube count", "반응 큐브 갯수", jaFont, enFont, koFont);
        uITextDic[sceneNm]["Area3Text"] = UITextMst.CreateLangSet("敵の数", "Obstacle count", "적의 수", jaFont, enFont, koFont);
        uITextDic[sceneNm]["Area4Text"] = UITextMst.CreateLangSet("アイテム数", "Item count", "아이템 수", jaFont, enFont, koFont);
        uITextDic[sceneNm]["TitleBtn"] = UITextMst.CreateLangSet("タイトル", "TITLE", "타이틀", jaFont, enFont, koFont);
        uITextDic[sceneNm]["RandomBtn"] = UITextMst.CreateLangSet("ランダム", "RANDOM", "랜덤", jaFont, enFont, koFont);
        uITextDic[sceneNm]["PlayBtn"] = UITextMst.CreateLangSet("プレイ", "PLAY", "플레이", jaFont, enFont, koFont);
        uITextDic[sceneNm]["StageMsg"] = UITextMst.CreateLangSet("ステージサイズが小さいです", "Stage size is too small", "스테이지 크기가 너무 작습니다", jaFont, enFont, koFont);
        uITextDic[sceneNm]["BlockMsg"] = UITextMst.CreateLangSet("-1(無制限)の1x1キューブがありません", "One of 1x1 cube must be -1(Infinite)", "-1(무제한)인 1x1큐브가 없습니다", jaFont, enFont, koFont);
        uITextDic[sceneNm]["ObstMsg"] = UITextMst.CreateLangSet("敵の数がステージサイズより大きいです", "Obstacle's count is bigger than stage size", "적의 수가 스테이지 크기보다 큽니다", jaFont, enFont, koFont);
        uITextDic[sceneNm]["ItemMsg"] = UITextMst.CreateLangSet("アイテムの数がステージサイズより大きいです", "Item's count is bigger than stage size", "아이템의 수가 스테이지 크기보다 큽니다", jaFont, enFont, koFont);
        uITextDic[sceneNm]["OptionTitle"] = UITextMst.CreateLangSet("", "", "", jaFont, enFont, koFont);
        uITextDic[sceneNm]["OptionMin"] = UITextMst.CreateLangSet("", "", "", jaFont, enFont, koFont);
        uITextDic[sceneNm]["OptionMax"] = UITextMst.CreateLangSet("", "", "", jaFont, enFont, koFont);
        uITextDic[sceneNm]["OptionCurr"] = UITextMst.CreateLangSet("", "", "", jaFont, enFont, koFont);

        sceneNm = "Shop";
        uITextDic[sceneNm] = new Dictionary<string, Dictionary<Language, UITextMst>>();
        uITextDic[sceneNm]["TitleText"] = UITextMst.CreateLangSet("コンビニ", "Convenience Store", "편의점", jaFont, enFont, koFont);
        uITextDic[sceneNm]["CoinCnt"] = UITextMst.CreateLangSet("", "", "", jaFont, enFont, koFont);
        uITextDic[sceneNm]["SweetsTitle"] = UITextMst.CreateLangSet("お菓子", "Sweets", "과자", jaFont, enFont, koFont);
        uITextDic[sceneNm]["AbilitiesTitle"] = UITextMst.CreateLangSet("能力", "Ability", "능력", jaFont, enFont, koFont);
        uITextDic[sceneNm]["Name"] = UITextMst.CreateLangSet("", "", "", jaFont, enFont, koFont);
        uITextDic[sceneNm]["Desc"] = UITextMst.CreateLangSet("", "", "", jaFont, enFont, koFont);
        uITextDic[sceneNm]["Status"] = UITextMst.CreateLangSet("", "", "", jaFont, enFont, koFont);
        uITextDic[sceneNm]["Price"] = UITextMst.CreateLangSet("", "", "", jaFont, enFont, koFont);
        uITextDic[sceneNm]["BuyBtnText"] = UITextMst.CreateLangSet("買う", "Buy", "살래", jaFont, enFont, koFont);
        uITextDic[sceneNm]["RefundBtnText"] = UITextMst.CreateLangSet("買わない", "Refund", "안살꺼야", jaFont, enFont, koFont);
        uITextDic[sceneNm]["EpilogueBtn"] = UITextMst.CreateLangSet("エピローグ ->", "EPILOGUE ->", "에필로그 ->", jaFont, enFont, koFont);
        uITextDic[sceneNm]["TourBtn"] = UITextMst.CreateLangSet("     旅行 ->     ", "   TOUR ->   ", "     여행 ->     ", jaFont, enFont, koFont);
        uITextDic[sceneNm]["MyStageBtn"] = UITextMst.CreateLangSet("マイステージ ->", "MY STAGE ->", "나의 스테이지 ->", jaFont, enFont, koFont);

        sceneNm = "Epilogue";
        uITextDic[sceneNm] = new Dictionary<string, Dictionary<Language, UITextMst>>();
        uITextDic[sceneNm]["EpilogueText"] = UITextMst.CreateLangSet("エピローグ", "EPILOGUE", "  에필로그  ", jaFont, enFont, koFont);
        uITextDic[sceneNm]["Page1ChatText"] = UITextMst.CreateLangSet("お菓子\r\n美味しい", "YUMMY", "과자\r\n맛있어", jaFont, enFont, koFont);
        uITextDic[sceneNm]["Page1SweetList"] = UITextMst.CreateLangSet("", "", "", jaFont, enFont, koFont);
        uITextDic[sceneNm]["Page2ChatText"] = UITextMst.CreateLangSet("こんど\r\n一緒に行こう", "Let's come together", "다음에 같이 오자", jaFont, enFont, koFont);
        uITextDic[sceneNm]["SkipBtn"] = UITextMst.CreateLangSet("  スキップ  ", "     SKIP     ", " 건너뛰기 ", jaFont, enFont, koFont);
        uITextDic[sceneNm]["NextBtn"] = UITextMst.CreateLangSet("  次へ ->  ", "  NEXT ->  ", "    다음 ->    ", jaFont, enFont, koFont);
        uITextDic[sceneNm]["ToCreditsBtn"] = UITextMst.CreateLangSet("スタッフロール", "CREDITS", "크레딧", jaFont, enFont, koFont);

        sceneNm = "Credits";
        uITextDic[sceneNm] = new Dictionary<string, Dictionary<Language, UITextMst>>();
        uITextDic[sceneNm]["CharacterCreditsTitle"] = UITextMst.CreateLangSet("キャラクター", "Character", "캐릭터", jaFont, enFont, koFont);
        uITextDic[sceneNm]["CharacterCreditsRole1"] = UITextMst.CreateLangSet("Course Library", "Course Library", "Course Library", jaFont, enFont, koFont);
        uITextDic[sceneNm]["CharacterCreditsName1"] = UITextMst.CreateLangSet("Unity Learn", "Unity Learn", "Unity Learn", jaFont, enFont, koFont);
        uITextDic[sceneNm]["CharacterCreditsRole2"] = UITextMst.CreateLangSet("Free Low Polygon_Animal", "Free Low Polygon_Animal", "Free Low Polygon_Animal", jaFont, enFont, koFont);
        uITextDic[sceneNm]["CharacterCreditsName2"] = UITextMst.CreateLangSet("GLOOMY STUDIO", "GLOOMY STUDIO", "GLOOMY STUDIO", jaFont, enFont, koFont);
        uITextDic[sceneNm]["AnimationCreditsTitle"] = UITextMst.CreateLangSet("アニメーション", "Animation", "애니메이션", jaFont, enFont, koFont);
        uITextDic[sceneNm]["AnimationCreditsRole1"] = UITextMst.CreateLangSet("Course Library", "Course Library", "Course Library", jaFont, enFont, koFont);
        uITextDic[sceneNm]["AnimationCreditsName1"] = UITextMst.CreateLangSet("Unity Learn", "Unity Learn", "Unity Learn", jaFont, enFont, koFont);
        uITextDic[sceneNm]["AnimationCreditsRole2"] = UITextMst.CreateLangSet("Animations", "Animations", "Animations", jaFont, enFont, koFont);
        uITextDic[sceneNm]["AnimationCreditsName2"] = UITextMst.CreateLangSet("Mixamo", "Mixamo", "Mixamo", jaFont, enFont, koFont);
        uITextDic[sceneNm]["ParticleCreditsTitle"] = UITextMst.CreateLangSet("パーティクル", "Particle", "파티클", jaFont, enFont, koFont);
        uITextDic[sceneNm]["ParticleCreditsRole1"] = UITextMst.CreateLangSet("Course Library", "Course Library", "Course Library", jaFont, enFont, koFont);
        uITextDic[sceneNm]["ParticleCreditsName1"] = UITextMst.CreateLangSet("Unity Learn", "Unity Learn", "Unity Learn", jaFont, enFont, koFont);
        uITextDic[sceneNm]["ParticleCreditsRole2"] = UITextMst.CreateLangSet("Polygonal's Low-Poly Particle Pack", "Polygonal's Low-Poly Particle Pack", "Polygonal's Low-Poly Particle Pack", jaFont, enFont, koFont);
        uITextDic[sceneNm]["ParticleCreditsName2"] = UITextMst.CreateLangSet("Polygonal Stuff", "Polygonal Stuff", "Polygonal Stuff", jaFont, enFont, koFont);
        uITextDic[sceneNm]["SoundCredits1Title"] = UITextMst.CreateLangSet("音響", "Sound", "사운드", jaFont, enFont, koFont);
        uITextDic[sceneNm]["SoundCredits1Role1"] = UITextMst.CreateLangSet("Course Library", "Course Library", "Course Library", jaFont, enFont, koFont);
        uITextDic[sceneNm]["SoundCredits1Name1"] = UITextMst.CreateLangSet("Unity Learn", "Unity Learn", "Unity Learn", jaFont, enFont, koFont);
        uITextDic[sceneNm]["SoundCredits1Role2"] = UITextMst.CreateLangSet("8-Bit Sfx", "8-Bit Sfx", "8-Bit Sfx", jaFont, enFont, koFont);
        uITextDic[sceneNm]["SoundCredits1Name2"] = UITextMst.CreateLangSet("Little Robot Sound Factory", "Little Robot Sound Factory", "Little Robot Sound Factory", jaFont, enFont, koFont);
        uITextDic[sceneNm]["SoundCredits1Role3"] = UITextMst.CreateLangSet("FREE Casual Game SFX Pack", "FREE Casual Game SFX Pack", "FREE Casual Game SFX Pack", jaFont, enFont, koFont);
        uITextDic[sceneNm]["SoundCredits1Name3"] = UITextMst.CreateLangSet("Dustyroom", "Dustyroom", "Dustyroom", jaFont, enFont, koFont);
        uITextDic[sceneNm]["SoundCredits1Role4"] = UITextMst.CreateLangSet("Free Footsteps System", "Free Footsteps System", "Free Footsteps System", jaFont, enFont, koFont);
        uITextDic[sceneNm]["SoundCredits1Name4"] = UITextMst.CreateLangSet("Pavel Cristian", "Pavel Cristian", "Pavel Cristian", jaFont, enFont, koFont);
        uITextDic[sceneNm]["SoundCredits1Role5"] = UITextMst.CreateLangSet("Monster SFX - 111518", "Monster SFX - 111518", "Monster SFX - 111518", jaFont, enFont, koFont);
        uITextDic[sceneNm]["SoundCredits1Name5"] = UITextMst.CreateLangSet("The Octoverse", "The Octoverse", "The Octoverse", jaFont, enFont, koFont);
        uITextDic[sceneNm]["SoundCredits1Role6"] = UITextMst.CreateLangSet("Free Music Tracks For Games", "Free Music Tracks For Games", "Free Music Tracks For Games", jaFont, enFont, koFont);
        uITextDic[sceneNm]["SoundCredits1Name6"] = UITextMst.CreateLangSet("Rizwan Ashraf", "Rizwan Ashraf", "Rizwan Ashraf", jaFont, enFont, koFont);
        uITextDic[sceneNm]["SoundCredits2Role1"] = UITextMst.CreateLangSet("Post Apocalypse Guns Demo", "Post Apocalypse Guns Demo", "Post Apocalypse Guns Demo", jaFont, enFont, koFont);
        uITextDic[sceneNm]["SoundCredits2Name1"] = UITextMst.CreateLangSet("Sound Earth Game Audio", "Sound Earth Game Audio", "Sound Earth Game Audio", jaFont, enFont, koFont);
        uITextDic[sceneNm]["SoundCredits2Role2"] = UITextMst.CreateLangSet("Shooting Sound", "Shooting Sound", "Shooting Sound", jaFont, enFont, koFont);
        uITextDic[sceneNm]["SoundCredits2Name2"] = UITextMst.CreateLangSet("B.G.M", "B.G.M", "B.G.M", jaFont, enFont, koFont);
        uITextDic[sceneNm]["SoundCredits2Role3"] = UITextMst.CreateLangSet("Mai Free Voices", "Mai Free Voices", "Mai Free Voices", jaFont, enFont, koFont);
        uITextDic[sceneNm]["SoundCredits2Name3"] = UITextMst.CreateLangSet("nagisa.f (nagisa.f creative)", "nagisa.f (nagisa.f creative)", "nagisa.f (nagisa.f creative)", jaFont, enFont, koFont);
        uITextDic[sceneNm]["FontCreditsTitle"] = UITextMst.CreateLangSet("フォント", "Font", "폰트", jaFont, enFont, koFont);
        uITextDic[sceneNm]["FontCreditsRole1"] = UITextMst.CreateLangSet("HachiMaruPop-Regular", "HachiMaruPop-Regular", "HachiMaruPop-Regular", jaFont, enFont, koFont);
        uITextDic[sceneNm]["FontCreditsName1"] = UITextMst.CreateLangSet("Nonty", "Nonty", "Nonty", jaFont, enFont, koFont);
        uITextDic[sceneNm]["FontCreditsRole2"] = UITextMst.CreateLangSet("HiMelody-Regular", "HiMelody-Regular", "HiMelody-Regular", jaFont, enFont, koFont);
        uITextDic[sceneNm]["FontCreditsName2"] = UITextMst.CreateLangSet("YoonDesign Inc", "YoonDesign Inc", "YoonDesign Inc", jaFont, enFont, koFont);
        uITextDic[sceneNm]["FontCreditsRole3"] = UITextMst.CreateLangSet("NotoSansJP-Regular", "NotoSansJP-Regular", "NotoSansJP-Regular", jaFont, enFont, koFont);
        uITextDic[sceneNm]["FontCreditsName3"] = UITextMst.CreateLangSet("Google", "Google", "Google", jaFont, enFont, koFont);
        uITextDic[sceneNm]["FontCreditsRole4"] = UITextMst.CreateLangSet("NotoSansKR-Regular", "NotoSansKR-Regular", "NotoSansKR-Regular", jaFont, enFont, koFont);
        uITextDic[sceneNm]["FontCreditsName4"] = UITextMst.CreateLangSet("Google", "Google", "Google", jaFont, enFont, koFont);
        uITextDic[sceneNm]["EndingCreditsTitle"] = UITextMst.CreateLangSet("エンディング", "Ending", "엔딩", jaFont, enFont, koFont);
        uITextDic[sceneNm]["EndingCreditsDesc1"] = UITextMst.CreateLangSet("「ふしぎの海のナディア」", "「Nadia: The Secret of Blue Water」", "「신비한 바다의 나디아」", jaFont, enFont, koFont);
        uITextDic[sceneNm]["EndingCreditsDesc2"] = UITextMst.CreateLangSet("のエンディングを真似して作りました。", "Follow Nadia's ending", "의 엔딩을 따라 만들었어요.", jaFont, enFont, koFont);
        uITextDic[sceneNm]["EndingCreditsDesc3"] = UITextMst.CreateLangSet("ナディア好き！", "I love Nadia!", "나디아 좋아!", jaFont, enFont, koFont);
        uITextDic[sceneNm]["CreatedByCreditsRole"] = UITextMst.CreateLangSet("制作", "Created by", "제작", jaFont, enFont, koFont);
        uITextDic[sceneNm]["CreatedByCreditsName"] = UITextMst.CreateLangSet("パク　チャングジュ", "CHANGJOO PARK", "박창주", jaFont, enFont, koFont);
        uITextDic[sceneNm]["GameOverText"] = UITextMst.CreateLangSet("ゲームオーバー", "GAME OVER", "게임 오버", jaFont, enFont, koFont);
        uITextDic[sceneNm]["ThankyouText"] = UITextMst.CreateLangSet("サンキューフォープレイング", "Thank you for playing", "고마워", jaFont, enFont, koFont);
        uITextDic[sceneNm]["SkipBtn"] = UITextMst.CreateLangSet("  スキップ  ", "     SKIP     ", " 건너뛰기 ", jaFont, enFont, koFont);
        uITextDic[sceneNm]["TourBtn"] = UITextMst.CreateLangSet("旅行 ->", "TOUR ->", "여행 ->", jaFont, enFont, koFont);
        uITextDic[sceneNm]["TitleBtn"] = UITextMst.CreateLangSet("タイトルへ", "TO TITLE", " 처음으로 ", jaFont, enFont, koFont);

        sceneNm = "Test";
        uITextDic[sceneNm] = new Dictionary<string, Dictionary<Language, UITextMst>>();
        uITextDic[sceneNm]["HeartCnt"] = UITextMst.CreateLangSet("", "", "", jaFont, enFont, koFont);
        uITextDic[sceneNm]["CoinCnt"] = UITextMst.CreateLangSet("", "", "", jaFont, enFont, koFont);
        uITextDic[sceneNm]["TempCoinCnt"] = UITextMst.CreateLangSet("", "", "", jaFont, enFont, koFont);
        uITextDic[sceneNm]["ActionCamNameText"] = UITextMst.CreateLangSet("アクション", "ACTION", "액션", jaFont, enFont, koFont);
        uITextDic[sceneNm]["TopCamNameText"] = UITextMst.CreateLangSet("   トップ   ", "   TOP   ", "톱", jaFont, enFont, koFont);
        uITextDic[sceneNm]["FollowCamNameText"] = UITextMst.CreateLangSet("フォロー", "FOLLOW", "추적", jaFont, enFont, koFont);
        uITextDic[sceneNm]["ContinueText"] = UITextMst.CreateLangSet("コンティニュー？", "CONTINUE ?", "계속할래?", jaFont, enFont, koFont);
        uITextDic[sceneNm]["SuccessText"] = UITextMst.CreateLangSet("おめでとう", "CONGRATULATIONS", "축하해", jaFont, enFont, koFont);
        uITextDic[sceneNm]["CountText"] = UITextMst.CreateLangSet("10", "10", "10", jaFont, enFont, koFont);
        uITextDic[sceneNm]["ContinueBtn"] = UITextMst.CreateLangSet($"いいよ", $"CONTINUE", $"계속할래", jaFont, enFont, koFont);
        uITextDic[sceneNm]["ContinuePrice"] = UITextMst.CreateLangSet($"{defDataMst.ContinueMoneyCnt}", $"{defDataMst.ContinueMoneyCnt}", $"{defDataMst.ContinueMoneyCnt}", jaFont, enFont, koFont);
        uITextDic[sceneNm]["RestartBtn"] = UITextMst.CreateLangSet("リスタート", " RESTART ", "처음부터", jaFont, enFont, koFont);
        uITextDic[sceneNm]["TitleBtn"] = UITextMst.CreateLangSet("タイトルへ", "TO TITLE", "타이틀로", jaFont, enFont, koFont);
        uITextDic[sceneNm]["RsltRestartBtn"] = UITextMst.CreateLangSet("リスタート", " RESTART ", "처음부터", jaFont, enFont, koFont);

        return uITextDic;
    }
}