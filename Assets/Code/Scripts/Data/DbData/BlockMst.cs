using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockMst
{
    public string Name { get; private set; }
    public int MaxCnt { get; set; }

    public BlockMst(string name, int maxCnt)
    {
        this.Name = name;
        this.MaxCnt = maxCnt;
    }

    public static Dictionary<string, Dictionary<string, int>> GetDic()
    {
        Dictionary<string, Dictionary<string, int>> dic = new Dictionary<string, Dictionary<string, int>>()
        {
            {
                "Title", new Dictionary<string, int>()
                {
                    { "JumpBlock", -1 },
                    { "FallBlock", -1 },
                    { "TrampolineBlock", -1 },
                    { "FireBlock", -1 },
                    { "IceBlock", -1 },

                    { "CowBlock", 0 },
                    { "FoxBlock", 0 },
                    { "ChickBlock", 0 },
                    { "KiteBlock", 0 },
                    { "EnemyGroundBlock", -1 },
                    { "EnemySkyBlock", -1 },

                    { "ZeroGravityBlock", 2 },
                    { "HammerBlock", 1 },

                    { "DragonBlock", 1 },

                    { "VolcanoBlock", 1 }
                }
            },
            {
                "Tutorial", new Dictionary<string, int>()
                {
                    { "JumpBlock", 0 },
                    { "FallBlock", 0 },
                    { "TrampolineBlock", 0 },
                    { "FireBlock", 0 },
                    { "IceBlock", 0 },

                    { "CowBlock", 0 },
                    { "FoxBlock", 0 },
                    { "ChickBlock", 0 },
                    { "KiteBlock", 0 },
                    { "EnemyGroundBlock", 0 },
                    { "EnemySkyBlock", 0 },

                    { "ZeroGravityBlock", 0 },
                    { "HammerBlock", 0 },

                    { "DragonBlock", 0 },

                    { "VolcanoBlock", 0 }
                }
            },
            {
                "Arcade1", new Dictionary<string, int>()
                {
                    { "JumpBlock", 0 },
                    { "FallBlock", 0 },
                    { "TrampolineBlock", 0 },
                    { "FireBlock", 0 },
                    { "IceBlock", 0 },

                    { "CowBlock", -1 },
                    { "FoxBlock", 0 },
                    { "ChickBlock", 0 },
                    { "KiteBlock", 0 },
                    { "EnemyGroundBlock", 0 },
                    { "EnemySkyBlock", 0 },

                    { "ZeroGravityBlock", 0 },
                    { "HammerBlock", 0 },

                    { "DragonBlock", 0 },

                    { "VolcanoBlock", 0 }
                }
            },
            {
                "Arcade2", new Dictionary<string, int>()
                {
                    { "JumpBlock", -1 },
                    { "FallBlock", 0 },
                    { "TrampolineBlock", 0 },
                    { "FireBlock", 0 },
                    { "IceBlock", 0 },

                    { "CowBlock", 0 },
                    { "FoxBlock", -1 },
                    { "ChickBlock", 0 },
                    { "KiteBlock", 0 },
                    { "EnemyGroundBlock", 0 },
                    { "EnemySkyBlock", 0 },

                    { "ZeroGravityBlock", 0 },
                    { "HammerBlock", 0 },

                    { "DragonBlock", 0 },

                    { "VolcanoBlock", 0 }
                }
            },
            {
                "Arcade3", new Dictionary<string, int>()
                {
                    { "JumpBlock", 0 },
                    { "FallBlock", -1 },
                    { "TrampolineBlock", 0 },
                    { "FireBlock", 0 },
                    { "IceBlock", 0 },

                    { "CowBlock", 0 },
                    { "FoxBlock", 0 },
                    { "ChickBlock", 0 },
                    { "KiteBlock", -1 },
                    { "EnemyGroundBlock", 0 },
                    { "EnemySkyBlock", 0 },

                    { "ZeroGravityBlock", 0 },
                    { "HammerBlock", 0 },

                    { "DragonBlock", 0 },

                    { "VolcanoBlock", 0 }
                }
            },
            {
                "Arcade4", new Dictionary<string, int>()
                {
                    { "JumpBlock", 0 },
                    { "FallBlock", 0 },
                    { "TrampolineBlock", -1 },
                    { "FireBlock", 0 },
                    { "IceBlock", 0 },

                    { "CowBlock", 0 },
                    { "FoxBlock", 0 },
                    { "ChickBlock", 0 },
                    { "KiteBlock", 0 },
                    { "EnemyGroundBlock", 0 },
                    { "EnemySkyBlock", 0 },

                    { "ZeroGravityBlock", 0 },
                    { "HammerBlock", 0 },

                    { "DragonBlock", 0 },

                    { "VolcanoBlock", 0 }
                }
            },
            {
                "Arcade5", new Dictionary<string, int>()
                {
                    { "JumpBlock", 0 },
                    { "FallBlock", 0 },
                    { "TrampolineBlock", -1 },
                    { "FireBlock", -1 },
                    { "IceBlock", -1 },

                    { "CowBlock", 0 },
                    { "FoxBlock", 0 },
                    { "ChickBlock", -1 },
                    { "KiteBlock", 0 },
                    { "EnemyGroundBlock", 0 },
                    { "EnemySkyBlock", 0 },

                    { "ZeroGravityBlock", 0 },
                    { "HammerBlock", 0 },

                    { "DragonBlock", 0 },

                    { "VolcanoBlock", 0 }
                }
            },
            {
                "Arcade6", new Dictionary<string, int>()
                {
                    { "JumpBlock", 0 },
                    { "FallBlock", 0 },
                    { "TrampolineBlock", -1 },
                    { "FireBlock", -1 },
                    { "IceBlock", -1 },

                    { "CowBlock", -1 },
                    { "FoxBlock", -1 },
                    { "ChickBlock", -1 },
                    { "KiteBlock", -1 },
                    { "EnemyGroundBlock", 0 },
                    { "EnemySkyBlock", 0 },

                    { "ZeroGravityBlock", 0 },
                    { "HammerBlock", 1 },

                    { "DragonBlock", 0 },

                    { "VolcanoBlock", 0 }
                }
            },
            {
                "Arcade7", new Dictionary<string, int>()
                {
                    { "JumpBlock", 0 },
                    { "FallBlock", 0 },
                    { "TrampolineBlock", 0 },
                    { "FireBlock", 0 },
                    { "IceBlock", 0 },

                    { "CowBlock", -1 },
                    { "FoxBlock", -1 },
                    { "ChickBlock", -1 },
                    { "KiteBlock", -1 },
                    { "EnemyGroundBlock", 0 },
                    { "EnemySkyBlock", 0 },

                    { "ZeroGravityBlock", 2 },
                    { "HammerBlock", 0 },

                    { "DragonBlock", 0 },

                    { "VolcanoBlock", 0 }
                }
            },
            {
                "Arcade8", new Dictionary<string, int>()
                {
                    { "JumpBlock", -1 },
                    { "FallBlock", -1 },
                    { "TrampolineBlock", -1 },
                    { "FireBlock", -1 },
                    { "IceBlock", -1 },

                    { "CowBlock", -1 },
                    { "FoxBlock", -1 },
                    { "ChickBlock", -1 },
                    { "KiteBlock", -1 },
                    { "EnemyGroundBlock", 0 },
                    { "EnemySkyBlock", 0 },

                    { "ZeroGravityBlock", 2 },
                    { "HammerBlock", 1 },

                    { "DragonBlock", 1 },

                    { "VolcanoBlock", 1 }
                }
            },
            {
                "MyStage", new Dictionary<string, int>()
                {
                    { "JumpBlock", 0 },
                    { "FallBlock", 0 },
                    { "TrampolineBlock", 0 },
                    { "FireBlock", 0 },
                    { "IceBlock", 0 },

                    { "CowBlock", -1 },
                    { "FoxBlock", 0 },
                    { "ChickBlock", 0 },
                    { "KiteBlock", 0 },
                    { "EnemyGroundBlock", 0 },
                    { "EnemySkyBlock", 0 },

                    { "ZeroGravityBlock", 0 },
                    { "HammerBlock", 0 },

                    { "DragonBlock", 0 },

                    { "VolcanoBlock", 0 }
                }
            },
            {
                "Mission1", new Dictionary<string, int>()
                {
                    { "JumpBlock", -1 },
                    { "FallBlock", -1 },
                    { "TrampolineBlock", -1 },
                    { "FireBlock", 0 },
                    { "IceBlock", 0 },

                    { "CowBlock", 0 },
                    { "FoxBlock", 0 },
                    { "ChickBlock", 0 },
                    { "KiteBlock", 0 },
                    { "EnemyGroundBlock", 0 },
                    { "EnemySkyBlock", 0 },

                    { "ZeroGravityBlock", 0 },
                    { "HammerBlock", 0 },

                    { "DragonBlock", 0 },

                    { "VolcanoBlock", 0 }
                }
            },
            {
                "Mission2", new Dictionary<string, int>()
                {
                    { "JumpBlock", -1 },
                    { "FallBlock", 0 },
                    { "TrampolineBlock", 0 },
                    { "FireBlock", 0 },
                    { "IceBlock", 0 },

                    { "CowBlock", 0 },
                    { "FoxBlock", 0 },
                    { "ChickBlock", 0 },
                    { "KiteBlock", 0 },
                    { "EnemyGroundBlock", 0 },
                    { "EnemySkyBlock", 0 },

                    { "ZeroGravityBlock", 0 },
                    { "HammerBlock", 0 },

                    { "DragonBlock", 0 },

                    { "VolcanoBlock", 0 }
                }
            },
            {
                "Mission3", new Dictionary<string, int>()
                {
                    { "JumpBlock", 0 },
                    { "FallBlock", 0 },
                    { "TrampolineBlock", 0 },
                    { "FireBlock", 0 },
                    { "IceBlock", 0 },

                    { "CowBlock", -1 },
                    { "FoxBlock", -1 },
                    { "ChickBlock", -1 },
                    { "KiteBlock", -1 },
                    { "EnemyGroundBlock", 0 },
                    { "EnemySkyBlock", 0 },

                    { "ZeroGravityBlock", 0 },
                    { "HammerBlock", 0 },

                    { "DragonBlock", 0 },

                    { "VolcanoBlock", 0 }
                }
            },
            {
                "Mission4", new Dictionary<string, int>()
                {
                    { "JumpBlock", 0 },
                    { "FallBlock", 0 },
                    { "TrampolineBlock", 0 },
                    { "FireBlock", -1 },
                    { "IceBlock", 0 },

                    { "CowBlock", 0 },
                    { "FoxBlock", 0 },
                    { "ChickBlock", 0 },
                    { "KiteBlock", 0 },
                    { "EnemyGroundBlock", 0 },
                    { "EnemySkyBlock", -1 },

                    { "ZeroGravityBlock", 0 },
                    { "HammerBlock", 0 },

                    { "DragonBlock", 0 },

                    { "VolcanoBlock", -1 }
                }
            },
            {
                "Mission5", new Dictionary<string, int>()
                {
                    { "JumpBlock", 0 },
                    { "FallBlock", 0 },
                    { "TrampolineBlock", 0 },
                    { "FireBlock", 0 },
                    { "IceBlock", 0 },

                    { "CowBlock", -1 },
                    { "FoxBlock", 0 },
                    { "ChickBlock", 0 },
                    { "KiteBlock", 0 },
                    { "EnemyGroundBlock", 0 },
                    { "EnemySkyBlock", 0 },

                    { "ZeroGravityBlock", 0 },
                    { "HammerBlock", 0 },

                    { "DragonBlock", 0 },

                    { "VolcanoBlock", 0 }
                }
            },
            {
                "Mission6", new Dictionary<string, int>()
                {
                    { "JumpBlock", -1 },
                    { "FallBlock", -1 },
                    { "TrampolineBlock", -1 },
                    { "FireBlock", -1 },
                    { "IceBlock", -1 },

                    { "CowBlock", 0 },
                    { "FoxBlock", 0 },
                    { "ChickBlock", 0 },
                    { "KiteBlock", 0 },
                    { "EnemyGroundBlock", -1 },
                    { "EnemySkyBlock", -1 },

                    { "ZeroGravityBlock", 0 },
                    { "HammerBlock", 5 },

                    { "DragonBlock", 0 },

                    { "VolcanoBlock", 0 }
                }
            },
            {
                "Mission7", new Dictionary<string, int>()
                {
                    { "JumpBlock", -1 },
                    { "FallBlock", -1 },
                    { "TrampolineBlock", -1 },
                    { "FireBlock", -1 },
                    { "IceBlock", -1 },

                    { "CowBlock", 0 },
                    { "FoxBlock", 0 },
                    { "ChickBlock", 0 },
                    { "KiteBlock", 0 },
                    { "EnemyGroundBlock", -1 },
                    { "EnemySkyBlock", -1 },

                    { "ZeroGravityBlock", -1 },
                    { "HammerBlock", 3 },

                    { "DragonBlock", 1 },

                    { "VolcanoBlock", -1 }
                }
            }
        };

        return dic;
    }
}
