using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemMst
{
    public string Name { get; private set; }
    public ItemType ItemType { get; private set; }
    public float PointCnt { get; set; }

    public ItemMst(string name, ItemType itemType, float pointCnt)
    {
        this.Name = name;
        this.ItemType = itemType;
        this.PointCnt = pointCnt;
    }

    public static Dictionary<string, ItemMst> GetDic()
    {
        Dictionary<string, ItemMst> dic = new Dictionary<string, ItemMst>()
        {
            { "Apple", new ItemMst("Apple", ItemType.Life, 1) },
            { "Banana", new ItemMst("Banana", ItemType.Score, 1000) },
            { "Bone", new ItemMst("Bone", ItemType.Power, 0.5f) },
            { "Coin", new ItemMst("Coin", ItemType.Money, 1) },
            { "CoinBig", new ItemMst("CoinBig", ItemType.Money, 30) },
            { "Jewel", new ItemMst("Jewel", ItemType.Money, 30) },
            { "Pizza", new ItemMst("Pizza", ItemType.Score, 500) },
            { "Poo", new ItemMst("Poo", ItemType.Poison, 7) },
            { "Steak", new ItemMst("Steak", ItemType.Power, 2) },
            { "SkillCube", new ItemMst("SkillCube", ItemType.Skill, 0) },
            { "Bomb", new ItemMst("Bomb", ItemType.Bomb, 1) }
        };

        return dic;
    }

    public static Dictionary<string, Dictionary<string, int>> GetStageDic()
    {
        Dictionary<string, Dictionary<string, int>> dic = new Dictionary<string, Dictionary<string, int>>()
        {
            {
                "Tutorial", new Dictionary<string, int>()
                {
                    { "Apple", 0 },
                    { "Banana", 0 },
                    { "Bone", 0 },
                    { "Coin", 0 },
                    { "Pizza", 0 },
                    { "Poo", 0 },
                    { "Steak", 0 }
                }
            },{
                "Arcade1", new Dictionary<string, int>()
                {
                    { "Apple", 1 },
                    { "Banana", 1 },
                    { "Bone", 0 },
                    { "Coin", 1 },
                    { "Pizza", 1 },
                    { "Poo", 1 },
                    { "Steak", 0 }
                }
            },
            {
                "Arcade2", new Dictionary<string, int>()
                {
                    { "Apple", 1 },
                    { "Banana", 2 },
                    { "Bone", 0 },
                    { "Coin", 2 },
                    { "Pizza", 2 },
                    { "Poo", 2 },
                    { "Steak", 0 }
                }
            },
            {
                "Arcade3", new Dictionary<string, int>()
                {
                    { "Apple", 1 },
                    { "Banana", 3 },
                    { "Bone", 0 },
                    { "Coin", 3 },
                    { "Pizza", 3 },
                    { "Poo", 3 },
                    { "Steak", 0 }
                }
            },
            {
                "Arcade4", new Dictionary<string, int>()
                {
                    { "Apple", 1 },
                    { "Banana", 3 },
                    { "Bone", 0 },
                    { "Coin", 3 },
                    { "Pizza", 3 },
                    { "Poo", 3 },
                    { "Steak", 0 }
                }
            },
            {
                "Arcade5", new Dictionary<string, int>()
                {
                    { "Apple", 1 },
                    { "Banana", 4 },
                    { "Bone", 1 },
                    { "Coin", 4 },
                    { "Pizza", 4 },
                    { "Poo", 4 },
                    { "Steak", 1 }
                }
            },
            {
                "Arcade6", new Dictionary<string, int>()
                {
                    { "Apple", 1 },
                    { "Banana", 5 },
                    { "Bone", 1 },
                    { "Coin", 5 },
                    { "Pizza", 5 },
                    { "Poo", 5 },
                    { "Steak", 1 }
                }
            },
            {
                "Arcade7", new Dictionary<string, int>()
                {
                    { "Apple", 1 },
                    { "Banana", 6 },
                    { "Bone", 1 },
                    { "Coin", 6 },
                    { "Pizza", 6 },
                    { "Poo", 6 },
                    { "Steak", 1 }
                }
            },
            {
                "Arcade8", new Dictionary<string, int>()
                {
                    { "Apple", 1 },
                    { "Banana", 7 },
                    { "Bone", 1 },
                    { "Coin", 7 },
                    { "Pizza", 7 },
                    { "Poo", 7 },
                    { "Steak", 1 }
                }
            },
            {
                "MyStage", new Dictionary<string, int>()
                {
                    { "Apple", 0 },
                    { "Banana", 0 },
                    { "Bone", 0 },
                    { "Coin", 0 },
                    { "Pizza", 0 },
                    { "Poo", 0 },
                    { "Steak", 0 }
                }
            },
            {
                "Mission1", new Dictionary<string, int>()
                {
                    { "Apple", 0 },
                    { "Banana", 23 },
                    { "Bone", 0 },
                    { "Coin", 0 },
                    { "Pizza", 24 },
                    { "Poo", 0 },
                    { "Steak", 0 }
                }
            },
            {
                "Mission2", new Dictionary<string, int>()
                {
                    { "Apple", 0 },
                    { "Banana", 0 },
                    { "Bone", 0 },
                    { "Coin", 0 },
                    { "Pizza", 13 },
                    { "Poo", 0 },
                    { "Steak", 0 }
                }
            },
            {
                "Mission3", new Dictionary<string, int>()
                {
                    { "Apple", 5 },
                    { "Banana", 5 },
                    { "Bone", 1 },
                    { "Coin", 5 },
                    { "Pizza", 5 },
                    { "Poo", 5 },
                    { "Steak", 1 }
                }
            },
            {
                "Mission4", new Dictionary<string, int>()
                {
                    { "Apple", 1 },
                    { "Banana", 5 },
                    { "Bone", 1 },
                    { "Coin", 5 },
                    { "Pizza", 5 },
                    { "Poo", 1 },
                    { "Steak", 1 }
                }
            },
            {
                "Mission5", new Dictionary<string, int>()
                {
                    { "Apple", 0 },
                    { "Banana", 0 },
                    { "Bone", 1 },
                    { "Coin", 0 },
                    { "Pizza", 0 },
                    { "Poo", 50 },
                    { "Steak", 1 }
                }
            },
            {
                "Mission6", new Dictionary<string, int>()
                {
                    { "Apple", 0 },
                    { "Banana", 0 },
                    { "Bone", 0 },
                    { "Coin", 98 },
                    { "Pizza", 0 },
                    { "Poo", 0 },
                    { "Steak", 0 }
                }
            },
            {
                "Mission7", new Dictionary<string, int>()
                {
                    { "Apple", 10 },
                    { "Banana", 20 },
                    { "Bone", 5 },
                    { "Coin", 20 },
                    { "Pizza", 20 },
                    { "Poo", 20 },
                    { "Steak", 5 }
                }
            }
        };

        return dic;
    }
}