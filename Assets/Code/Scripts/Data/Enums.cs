using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Layer
{
    Player = 6,
    Skill = 7,
    Enemy = 8,
    Dragon = 9,
    Obst = 10,
    Floor = 11,
    Ground = 12,
    ActiveGround = 13,
    Item = 14
}

public enum FourTime
{
    Morning,
    Afternoon,
    Evening,
    Night
}

public enum CamType
{
    FirstPerson,
    FreeLook,
    ThirdPerson,
    TopFollow,
    Top
}

public enum IslandCamType
{
    TitleCam,
    TitleZoomCam,
    ArcadeCam,
    TopCam,
    SideCam
}

public enum Reverse
{
    Normal,
    Reverse
}

public enum GameMode
{
    Arcade,
    Tour,
    MyStage
}

public enum GameProgress
{
    Ready,
    Play,
    Clear,
    Fail
}

public enum Language
{
    JA,
    EN,
    KO
}

public enum Ability
{
    None,
    Life,
    Jump,
    Speed,
    Push,
    Bomb,
    MoveAir,
    MyStage
}

public enum AbnormalState
{
    None,
    Burn,
    Frozen
}

public enum ItemType
{
    Life,
    Score,
    Money,
    Power,
    Poison,
    Skill,
    Bomb
}

public enum SkillType
{
    SkillA,
    SkillB,
    SkillC,
    SkillD
}