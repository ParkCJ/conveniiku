using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class PlayerData
{
    // Player
    public int Life;
    public float SpeedGr;
    public float SpeedAir;
    public float JumpForce;
    public float JumpInterval;
    public float PushForce;
    public float PushInterval;
    public float AttackInterval;
    public float BombInterval;
    public int Bomb;
    public float PowerUp;
    public bool MoveAir;

    // Money, Score, Block
    public int Money;
    public int Score;
    public int Block;

    // Sweet
    public Dictionary<SweetMst, int> PlayerSweets;

    // Travel
    public int LastStageIdx = 100;
    public FourTime LastFourTime;
    public IslandCamType LastIslandCamType;

    // Arcade
    public CamType camType = CamType.TopFollow;

    // MyStage
    public bool MyStageUnlocked;
    public PlayerMyStage PlayerMyStage;

    // Mode
    public bool BattleMode;
    public SkillType CurrSkillType;

    // Config
    public Language Lang;
    public Reverse ActionCamHori;
    public Reverse ActionCamVert;
    public float ActionCamShakeRate;

    public PlayerData()
    {
        Life = 0;
        SpeedGr = 0;
        SpeedAir = 0.1f;
        JumpForce = 0;
        JumpInterval = 1;
        PushForce = 1f;
        PushInterval = 1;
        AttackInterval = 1;
        BombInterval = 1;
        Bomb = 0;
        PowerUp = 1.0f;
        MoveAir = false;

        // Money, Score
        Money = 10;
        Score = 0;
        Block = 0;

        // Sweet
        PlayerSweets = new Dictionary<SweetMst, int>();

        // Travel
        LastStageIdx = 100;
        LastFourTime = FourTime.Morning;
        LastIslandCamType = IslandCamType.TitleCam;

        // MyStage
        MyStageUnlocked = false;
        PlayerMyStage = new PlayerMyStage();

        // Mode
        BattleMode = true;
        CurrSkillType = SkillType.SkillA;

        // Config
        Lang = Language.EN;
        ActionCamShakeRate = 10;
    }

    public PlayerData(DefDataMst defDataMst) : this()
    {
        Life = defDataMst.DefLifeCnt;
        SpeedGr = defDataMst.DefSpeedGr;
        JumpForce = defDataMst.DefJumpForce;
        Bomb = defDataMst.DefBombCnt;
    }

    // Sweet
    public void BuySweet(SweetMst sweetMst, int count = 1)
    {
        if (PlayerSweets.ContainsKey(sweetMst))
            PlayerSweets[sweetMst] += count;
        else
            PlayerSweets[sweetMst] = 1;

        switch (sweetMst.Ability)
        {
            case Ability.Life:
                Life += (int)sweetMst.AbilityCnt;
                break;
            case Ability.Jump:
                JumpForce += sweetMst.AbilityCnt;
                break;
            case Ability.Speed:
                SpeedGr += sweetMst.AbilityCnt;
                break;
            case Ability.Push:
                PushForce += sweetMst.AbilityCnt;
                break;
            case Ability.Bomb:
                Bomb += (int)sweetMst.AbilityCnt;
                break;
            case Ability.MyStage:
                MyStageUnlocked = true;
                break;
            case Ability.MoveAir:
                MoveAir = true;
                break;
        }

        Money -= sweetMst.CoinCnt;
    }

    public void RefundSweet(SweetMst sweetMst, int count = 1)
    {
        if (PlayerSweets.ContainsKey(sweetMst))
        {
            PlayerSweets[sweetMst] -= count;
            if (PlayerSweets[sweetMst] == 0)
                PlayerSweets.Remove(sweetMst);
        }

        switch (sweetMst.Ability)
        {
            case Ability.Life:
                Life -= (int)sweetMst.AbilityCnt;
                Life = Mathf.Max(Life, GameManager.Instance.dbHandler.DefDataMst.DefLifeCnt);
                break;
            case Ability.Jump:
                JumpForce -= sweetMst.AbilityCnt;
                JumpForce = Mathf.Max(JumpForce, GameManager.Instance.dbHandler.DefDataMst.DefJumpForce);
                break;
            case Ability.Speed:
                SpeedGr -= sweetMst.AbilityCnt;
                SpeedGr = Mathf.Max(SpeedGr, GameManager.Instance.dbHandler.DefDataMst.DefSpeedGr);
                break;
            case Ability.Push:
                PushForce -= sweetMst.AbilityCnt;
                break;
            case Ability.Bomb:
                Bomb -= (int)sweetMst.AbilityCnt;
                Bomb = Mathf.Max(Bomb, GameManager.Instance.dbHandler.DefDataMst.DefBombCnt);
                break;
            case Ability.MyStage:
                MyStageUnlocked = false;
                break;
            case Ability.MoveAir:
                MoveAir = false;
                break;
        }

        Money += sweetMst.CoinCnt;
    }

    public void ConsumeSweets()
    {
        PlayerSweets.Clear();
    }
}
