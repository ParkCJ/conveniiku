using System;

[Serializable]
public class PlayerMyStage
{
    // Play
    public int Life;
    public int XSize;
    public int ZSize;
    public float ObstBlockRate;
    public FourTime fourTime;

    // Block
    public int JumpBlock;
    public int FallBlock;
    public int TrampolineBlock;
    public int FireBlock;
    public int IceBlock;
    public int EnemyGroundBlock;
    public int EnemySkyBlock;
    public int ZeroGravityBlock;
    public int HammerBlock;
    public int DragonBlock;
    public int VolcanoBlock;

    // Obstacle
    public int CowObst;
    public int FoxObst;
    public int ChickObst;
    public int KiteObst;
    public int DragonObst;
    public int HammerObst;

    // Item
    public int CoinItem;
    public int SteakItem;
    public int BoneItem;
    public int AppleItem;
    public int BananaItem;
    public int PizzaItem;
    public int PooItem;

    public PlayerMyStage()
    {
        Life = 1;
        XSize = 1;
        ZSize = 1;
    }
}