using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using UnityEngine.SceneManagement;
using Cinemachine;

/* TODO
 * 1. 공격 Sphear 생성
 * 2. 게임 clear 후 못움직이게 (script enable false)
 * 3. Third person camera 수정 (1인칭과 같은 조작에 3인칭 시점으로. 2배 scale에도 잘 보이게)
 */
public class StoreGameCtrl : MonoBehaviour
{
    [Header("Prefab (Environment)")]
    [SerializeField] Material[] skyboxMorningMats;
    [SerializeField] Material[] skyboxAfternoonMats;
    [SerializeField] Material[] skyboxEveningMats;
    [SerializeField] Material[] skyboxNightMats;
    [SerializeField] GameObject cloudAnchorPrefab;
    [SerializeField] ParticleSystem balloonParticlePrefab;
    [SerializeField] GameObject nightDomePrefab;
    [SerializeField] AudioSource[] soundscapePrefabs;
    GameObject nightDome;
    GameObject cloudContainer;
    float windStrength;

    [Header("Prefab (Stage)")]
    [SerializeField] GameObject startBlockPrefab;
    [SerializeField] GameObject nextBlockPrefab;
    [SerializeField] GameObject goalBlockPrefab;
    [SerializeField] GameObject normalBlockPrefab;
    [SerializeField] GameObject[] block1x1s;
    [SerializeField] GameObject[] block2x2s;
    [SerializeField] GameObject[] block3x3s;
    [SerializeField] GameObject[] block4x4s;
    [SerializeField] GameObject[] block5x5s;
    [SerializeField] GameObject[] obstPrefabs;
    [SerializeField] GameObject[] itemPrefabs;
    [SerializeField] GameObject playerPrefab;
    [SerializeField] GameObject floor;
    [SerializeField] GameObject destroySensorPrefab;
    GameObject blockContainer;
    Vector3 stageCenter;
    List<Block> obstBlockList = new List<Block>();
    Dictionary<int, Dictionary<int, bool?>> blockBook = new Dictionary<int, Dictionary<int, bool?>>();
    Dictionary<int, Dictionary<int, bool>> obstBook = new Dictionary<int, Dictionary<int, bool>>();
    Dictionary<int, Dictionary<int, bool>> itemBook = new Dictionary<int, Dictionary<int, bool>>();
    Dictionary<string, int> stageItems = new Dictionary<string, int>();

    [Header("Camera")]
    public CinemachineVirtualCamera firstPersonCam;
    public CinemachineVirtualCamera thirdPersonCam;
    public CinemachineFreeLook freeLookCam;
    public CinemachineVirtualCamera topFollowCam;
    public CinemachineVirtualCamera topCam;

    [SerializeField] Camera secondCam;
    [SerializeField] CinemachineVirtualCamera secondActionCam;
    [SerializeField] CinemachineVirtualCamera secondFollowCam;
    [SerializeField] CinemachineVirtualCamera secondTopCam;
    public RenderTexture SecondCamRndTexture { get; private set; }

    [Header("Audio")]
    [SerializeField] AudioClip gameClearSound;
    [SerializeField] AudioClip gameOverSound;
    [SerializeField] AudioClip lifeDownSound;

    public GameProgress gameProgress;
    public GameObject player;
    GameManager gm;

    void Awake()
    {
        gm = GameManager.Instance;
        gm.gameCtrl = this;

        CreateEnvironment();
        CreateStage();
        CreateSecondCam();
        CreatePlayer();

        gameProgress = GameProgress.Play;
    }

    void OnDestroy()
    {
        if (SecondCamRndTexture != null)
            SecondCamRndTexture.Release();
    }

    // Environment
    void CreateEnvironment()
    {
        // stage center
        stageCenter = new Vector3((gm.gdStageMst.XSize - 0.5f) * 0.5f, 0.5f * 0.5f, (gm.gdStageMst.ZSize - 0.5f) * 0.5f);

        // Calculate topCam position
        float x = (gm.gdStageMst.XSize * 0.5f) - 0.5f;

        int max = Mathf.Max(gm.gdStageMst.XSize, gm.gdStageMst.ZSize);
        float y = max >= 14 ? max * 0.7f : (max > 10 ? max * 0.85f : (max > 4 ? max : (max * 2)));

        float zRate = y < max && gm.gdStageMst.ZSize >= gm.gdStageMst.XSize ? y : gm.gdStageMst.ZSize;
        float z = -y + ((zRate * 0.5f) + 0.5f);

        topCam.transform.position = new Vector3(x, y, z);
        topCam.transform.rotation = Quaternion.Euler(new Vector3(45, 0, 0));
        secondTopCam.transform.position = new Vector3(x, y, z);
        secondTopCam.transform.rotation = Quaternion.Euler(new Vector3(45, 0, 0));

        // direct light
        FourTime fourTime = gm.gameMode == GameMode.Tour ? gm.gdPlayerData.LastFourTime : gm.gdStageMst.FourTime;

        Light dirLight = GameObject.Find("Directional Light").GetComponent<Light>();
        dirLight.gameObject.transform.rotation = Quaternion.Euler(new Vector3(TimeHandler.GetSunXRot(fourTime), -30, 0));

        // soundscape
        if (SceneManager.GetActiveScene().name == "Game")
        {
            foreach (AudioSource soundscape in soundscapePrefabs)
                Instantiate(soundscape, stageCenter, soundscape.transform.rotation);
        }

        // postprocessing & Fourtime
        Volume ppVol = GameObject.Find("Global Volume").GetComponent<Volume>();
        Bloom ppBloom;
        ppVol.profile.TryGet(out ppBloom);

        // Wind
        windStrength = Random.Range(-gm.gdStageMst.WindStrength, gm.gdStageMst.WindStrength);

        // FourTime
        switch (fourTime)
        {
            case FourTime.Morning:
                RenderSettings.skybox = skyboxMorningMats[Random.Range(0, skyboxMorningMats.Length)];
                dirLight.color = new Color32(255, 244, 214, 255);
                dirLight.intensity = 1f;
                Destroy(nightDome);

                ppBloom.intensity.value = 0.25f;
                break;
            case FourTime.Afternoon:
                RenderSettings.skybox = skyboxAfternoonMats[Random.Range(0, skyboxAfternoonMats.Length)];
                RenderSettings.ambientIntensity = 1.05f;
                dirLight.color = new Color32(255, 244, 214, 255);
                dirLight.intensity = 1f;
                Destroy(nightDome);

                ppBloom.intensity.value = 0.5f;
                break;
            case FourTime.Evening:
                RenderSettings.skybox = skyboxEveningMats[Random.Range(0, skyboxEveningMats.Length)];
                RenderSettings.ambientMode = AmbientMode.Flat;
                RenderSettings.ambientLight = new Color32(143, 102, 90, 255);
                dirLight.color = new Color32(255, 244, 214, 255);
                dirLight.intensity = 1f;
                Destroy(nightDome);

                ppBloom.intensity.value = 0.75f;
                break;
            case FourTime.Night:
                dirLight.color = new Color32(244, 175, 3, 255);
                dirLight.intensity = 0.175f;
                if (nightDome == null)
                {
                    nightDome = Instantiate(nightDomePrefab);
                    nightDome.GetComponent<NightDome>().SetSpeed(windStrength);
                }

                ppBloom.intensity.value = 1f;
                break;
        }

        // Cloud
        cloudContainer = new GameObject("Cloud container");
        for (int i = 0; i < gm.gdStageMst.CloudCnt; i++) // cloud
        {
            CloudAnchor cloudAnchor = Instantiate(cloudAnchorPrefab, stageCenter, cloudAnchorPrefab.transform.rotation, cloudContainer.transform).GetComponent<CloudAnchor>();
            cloudAnchor.windStrength = windStrength;
        }

        // Effect
        if (gm.gdStageMst.Effect)
            Instantiate(balloonParticlePrefab);
    }

    // Stage
    #region - CreateStage
    void CreateStage()
    {
        // Block, Obst, Item book
        for (int xPos = 0; xPos < gm.gdStageMst.XSize; xPos++)
        {
            blockBook[xPos] = new Dictionary<int, bool?>();
            obstBook[xPos] = new Dictionary<int, bool>();
            itemBook[xPos] = new Dictionary<int, bool>();
            for (int zPos = 0; zPos < gm.gdStageMst.ZSize; zPos++)
            {
                blockBook[xPos][zPos] = null;
                obstBook[xPos][zPos] = false;
                itemBook[xPos][zPos] = false;
            }
        }

        // Block container
        blockContainer = new GameObject("Block container");

        // Floor
        floor.transform.position = new Vector3((gm.gdStageMst.XSize - 1) / 2.0f, -1f, (gm.gdStageMst.ZSize - 1) / 2.0f);
        floor.transform.localScale = new Vector3(gm.gdStageMst.XSize, 1, gm.gdStageMst.ZSize);

        // Destroy sensor
        Instantiate(destroySensorPrefab, new Vector3(stageCenter.x, stageCenter.y - 30, stageCenter.z), destroySensorPrefab.transform.rotation);

        // StartBlock
        Instantiate(startBlockPrefab, new Vector3(0, 0, 0), startBlockPrefab.transform.rotation, blockContainer.transform);
        blockBook[0][0] = true;

        // GoalBlock
        if (gm.gameMode == GameMode.Arcade && gm.gdStageMst.Name != "Tutorial")
            Instantiate(nextBlockPrefab, new Vector3(gm.gdStageMst.XSize - 1, 0, gm.gdStageMst.ZSize - 1), nextBlockPrefab.transform.rotation, blockContainer.transform);
        else
            Instantiate(goalBlockPrefab, new Vector3(gm.gdStageMst.XSize - 1, 0, gm.gdStageMst.ZSize - 1), goalBlockPrefab.transform.rotation, blockContainer.transform);

        blockBook[gm.gdStageMst.XSize - 1][gm.gdStageMst.ZSize - 1] = true;

        // Create game blocks
        for (int xPos = 0; xPos < gm.gdStageMst.XSize; xPos++)
        {
            for (int zPos = 0; zPos < gm.gdStageMst.ZSize; zPos++)
            {
                // Check if position is StartBlock or GoalBlock position
                if ((xPos == 0 && zPos == 0) ||
                    (xPos == gm.gdStageMst.XSize - 1 && zPos == gm.gdStageMst.ZSize - 1))
                    continue;

                // Block
                if (blockBook[xPos][zPos] == null)
                {
                    int xSpaceLeft = gm.gdStageMst.XSize - xPos;
                    int zSpaceLeft = gm.gdStageMst.ZSize - zPos;
                    int minSpaceLeft = Mathf.Min(xSpaceLeft, zSpaceLeft);

                    CreateBlock(xPos, zPos, minSpaceLeft);
                }
            }
        }

        // Create Obstacles
        CreateObstItems(gm.dbHandler.ObstMstDic, obstPrefabs, obstBook);

        // Create Items
        CreateObstItems(gm.dbHandler.StageItemMstDic, itemPrefabs, itemBook);

        // Clear obstBlockList
        obstBlockList.Clear();
        blockBook = null;
        obstBook = null;
        itemBook = null;
    }

    void CreateBlock(int xPos, int zPos, int maxSpace)
    {
        int rndRangeMax = gm.gdStageMst.XSize * gm.gdStageMst.ZSize;
        float obstRangeMax = gm.gdStageMst.XSize * gm.gdStageMst.ZSize * gm.gdStageMst.ObstBlockRate;

        if (Random.Range(1, rndRangeMax + 1) <= obstRangeMax)
        {
            CreateObstBlock(xPos, zPos, maxSpace);
        }
        else
        {
            CreateNormalBlock(xPos, zPos);
        }
    }

    void CreateNormalBlock(int xPos, int zPos)
    {
        Instantiate(normalBlockPrefab, new Vector3(xPos, 0, zPos), normalBlockPrefab.transform.rotation, blockContainer.transform);
        blockBook[xPos][zPos] = true;
    }

    void CreateObstBlock(int xPos, int zPos, int maxSpace)
    {
        if (maxSpace > 5)
            maxSpace = 5;

        int obstSize = GetRndObstSize(maxSpace);

        switch (obstSize)
        {
            case 1:
                InstantiateObstBlock(xPos, zPos, obstSize, block1x1s);
                break;
            case 2:
                InstantiateObstBlock(xPos, zPos, obstSize, block2x2s);
                break;
            case 3:
                InstantiateObstBlock(xPos, zPos, obstSize, block3x3s);
                break;
            case 4:
                InstantiateObstBlock(xPos, zPos, obstSize, block4x4s);
                break;
            case 5:
                InstantiateObstBlock(xPos, zPos, obstSize, block5x5s);
                break;
        }
    }

    int GetRndObstSize(int maxSpace)
    {
        int maxPos = block1x1s.Length
            + (maxSpace >= 2 ? block2x2s.Length : 0)
            + (maxSpace >= 3 ? block3x3s.Length : 0)
            + (maxSpace >= 4 ? block4x4s.Length : 0)
            + (maxSpace >= 5 ? block5x5s.Length : 0);

        int pos = Random.Range(0, maxPos);
        if (pos < block1x1s.Length)
            return 1;
        else if (pos < block1x1s.Length + block2x2s.Length)
            return 2;
        else if (pos < block1x1s.Length + block2x2s.Length + block3x3s.Length)
            return 3;
        else if (pos < block1x1s.Length + block2x2s.Length + block3x3s.Length + block4x4s.Length)
            return 4;
        else
            return 5;
    }

    void InstantiateObstBlock(int xPos, int zPos, int obstSize, GameObject[] blocks)
    {
        if (blocks.Length == 0 || HasBlock(xPos, zPos, obstSize))
            CreateObstBlock(xPos, zPos, obstSize - 1);
        else
        {
            int idx = Random.Range(0, blocks.Length);
            string bNm = blocks[idx].name;

            int maxCnt = gm.dbHandler.BlockMstDic[gm.gdStageMst.Name][bNm];
            if (maxCnt == -1 || obstBlockList.Count(x => x.name.StartsWith(bNm)) < maxCnt)
            {
                // Because block's pivot point is in center, position offset is required. (ex: 2x2 -> xPos: xPos + 0.5, zPos: zPos + 0.5)
                float posOffset = (obstSize - 1) / 2.0f;

                GameObject obj = Instantiate(blocks[idx].gameObject, new Vector3(xPos + posOffset, 0, zPos + posOffset), blocks[idx].transform.rotation, blockContainer.transform);
                obstBlockList.Add(obj.GetComponent<Block>());

                for (int x = xPos; x < xPos + obstSize; x++)
                    for (int z = zPos; z < zPos + obstSize; z++)
                        blockBook[x][z] = false;
            }
            else
                CreateObstBlock(xPos, zPos, obstSize);
        }
    }

    bool HasBlock(int xPos, int zPos, int blockSize)
    {
        for (int x = xPos; x < xPos + blockSize; x++)
        {
            for (int z = zPos; z < zPos + blockSize; z++)
            {
                if (blockBook[x][z] != null)
                    return true;
            }
        }

        return false;
    }

    void CreateObstItems(Dictionary<string, Dictionary<string, int>> mstDic, GameObject[] prefabs, Dictionary<int, Dictionary<int, bool>> book)
    {
        Dictionary<string, int> dic = mstDic[gm.gdStageMst.Name];
        foreach (string key in dic.Keys)
        {
            int totCnt = dic[key];
            if (totCnt == 0)
                continue;

            int idx = -1;
            for (int i = 0; i < prefabs.Length; i++)
            {
                if (prefabs[i].name == key)
                {
                    idx = i;
                    break;
                }

            }

            for (int cnt = 0; cnt < totCnt; cnt++)
                CreateObstItem(idx, prefabs, book);
        }
    }

    void CreateObstItem(int idx, GameObject[] prefabs, Dictionary<int, Dictionary<int, bool>> book)
    {
        int xPos = Random.Range(0, gm.gdStageMst.XSize);
        int zPos = Random.Range(0, gm.gdStageMst.ZSize);

        if ((xPos == 0 && zPos == 0) ||
            (xPos == gm.gdStageMst.XSize - 1 && zPos == gm.gdStageMst.ZSize - 1))
            CreateObstItem(idx, prefabs, book);
        else if (book[xPos][zPos])
            CreateObstItem(idx, prefabs, book);
        else
        {
            Instantiate(prefabs[idx].gameObject, new Vector3(xPos, prefabs[idx].transform.position.y, zPos), Quaternion.identity);
            book[xPos][zPos] = true;
        }
    }
    #endregion

    // Process
    public void Pause()
    {
        if (Mathf.Approximately(Time.timeScale, 0))
        {
            Time.timeScale = 1;
            AudioListener.pause = false;
        }
        else
        {
            Time.timeScale = 0;
            AudioListener.pause = true;
        }
    }

    public void GameClear()
    {
        gameProgress = GameProgress.Clear;

        // Stop all block's activation
        foreach (Block block in GameObject.FindObjectsOfType<Block>())
            block.StopActivationNow();

        // Destroy all obstacles
        foreach (Destroyable destroyable in GameObject.FindObjectsOfType<Destroyable>())
            destroyable.PassAway();

        // Change block to score
        if (gm.gdPlayerData.Block > 0)
        {
            AddScore(gm.gdPlayerData.Block * 100);
            AddBlock(-gm.gdPlayerData.Block);
        }

        AudioSource.PlayClipAtPoint(gameClearSound, Camera.main.transform.position, 0.02f);
    }

    public void Continue()
    {
        gameProgress = GameProgress.Play;

        int currBomb = gm.gdPlayerData.Bomb;
        gm.InitPlayerData(true);
        gm.gdPlayerData.Bomb = Mathf.Max(currBomb, gm.gdPlayerData.Bomb);

        CreatePlayer();
    }

    void Update()
    {
        RenderSettings.skybox.SetFloat("_Rotation", Time.time * windStrength);
    }

    // Player
    void CreatePlayer()
    {
        // Create player
        player = Instantiate(playerPrefab, new Vector3(0, 1, 0), playerPrefab.transform.rotation);

        // Set ability
        SetPlayerAbility();

        // Set camera
        SetCam();
    }

    void SetPlayerAbility()
    {
        player.transform.localScale = Vector3.one * gm.gdPlayerData.PowerUp;
        player.transform.position = new Vector3(player.transform.position.x, (player.transform.localScale.y / 2.0f + 0.5f), player.transform.position.z);
        gm.gdPlayerData.SpeedGr = gm.PlayerData.SpeedGr * gm.gdPlayerData.PowerUp;
        gm.gdPlayerData.PushForce = gm.PlayerData.PushForce * gm.gdPlayerData.PowerUp;
    }

    public void KillPlayer()
    {
        AudioSource.PlayClipAtPoint(lifeDownSound, player.transform.position);

        if (gameProgress == GameProgress.Clear)
        {
            // 게임 클리어 후 밑으로 떨어져서 플레이어가 죽을 때
            Destroy(player);
            CreatePlayer();
        }
        else
        {
            Destroy(player);

            // Player data
            gm.gdPlayerData.Life -= 1;
            gm.InitPlayerData();

            if (gm.gdPlayerData.Life == 0)
            {
                gameProgress = GameProgress.Fail;
                AudioSource.PlayClipAtPoint(gameOverSound, Camera.main.transform.position, 0.05f);
            }
            else
                CreatePlayer();
        }
    }

    // Item
    public void AddLife(int lifeCnt)
    {
        gm.gdPlayerData.Life = Mathf.Clamp(0, gm.gdPlayerData.Life + lifeCnt, gm.dbHandler.DefDataMst.DefMaxLifeCnt);
    }

    public void AddScore(int score)
    {
        gm.gdPlayerData.Score = Mathf.Clamp(0, gm.gdPlayerData.Score + score, gm.dbHandler.DefDataMst.DefMaxScore);
    }

    public void AddBlock(int block)
    {
        gm.gdPlayerData.Block = Mathf.Clamp(0, gm.gdPlayerData.Block + block, gm.dbHandler.DefDataMst.DefMaxBlock);
    }

    public void AddMoney(int money)
    {
        gm.gdPlayerData.Money = Mathf.Clamp(0, gm.gdPlayerData.Money + money, gm.dbHandler.DefDataMst.DefMaxMoney);
    }

    public void AddPower(float power)
    {
        gm.gdPlayerData.PowerUp = Mathf.Clamp(gm.gdPlayerData.PowerUp * power, 0.5f, 2);
        SetPlayerAbility();
    }

    public void ChangeSkill(int skillIdx)
    {
        gm.gdPlayerData.CurrSkillType = (SkillType)skillIdx;
    }

    public void AddBomb(int bombCnt)
    {
        gm.gdPlayerData.Bomb = Mathf.Clamp(0, gm.gdPlayerData.Bomb + bombCnt, gm.dbHandler.DefDataMst.DefMaxBomb);
    }

    public void SetCam()
    {
        //if (CinemachineCore.Instance.GetActiveBrain(0).ActiveVirtualCamera != null)
        //    CinemachineCore.Instance.GetActiveBrain(0).ActiveVirtualCamera.Priority = 0;

        switch (gm.gdPlayerData.camType)
        {
            case CamType.FirstPerson:
                firstPersonCam.Priority = 1;
                thirdPersonCam.Priority = 0;
                freeLookCam.Priority = 0;
                topFollowCam.Priority = 0;
                topCam.Priority = 0;
                firstPersonCam.Follow = player.transform.Find("CamFollowTarget");

                Cursor.lockState = CursorLockMode.Locked;

                secondActionCam.Priority = 0;
                secondFollowCam.Follow = player.transform;
                secondFollowCam.LookAt = player.transform;
                secondFollowCam.Priority = 1;
                secondTopCam.Priority = 0;
                break;
            case CamType.FreeLook:
                firstPersonCam.Priority = 0;
                thirdPersonCam.Priority = 0;
                freeLookCam.Priority = 1;
                topFollowCam.Priority = 0;
                topCam.Priority = 0;
                freeLookCam.Follow = player.transform;
                freeLookCam.LookAt = player.transform;
                freeLookCam.m_XAxis.m_InvertInput = gm.gdPlayerData.ActionCamHori == Reverse.Normal;
                freeLookCam.m_YAxis.m_InvertInput = gm.gdPlayerData.ActionCamVert == Reverse.Normal;

                Cursor.lockState = CursorLockMode.Locked;

                secondActionCam.Priority = 0;
                secondFollowCam.Priority = 0;
                secondTopCam.Priority = 1;
                break;
            case CamType.ThirdPerson:
                firstPersonCam.Priority = 0;
                thirdPersonCam.Priority = 1;
                freeLookCam.Priority = 0;
                topFollowCam.Priority = 0;
                topCam.Priority = 0;
                thirdPersonCam.Follow = player.transform.Find("CamFollowTarget");
                thirdPersonCam.LookAt = player.transform.Find("TpsCamLookTarget");

                Cursor.lockState = CursorLockMode.None;

                secondActionCam.Priority = 0;
                secondFollowCam.Priority = 0;
                secondTopCam.Priority = 1;
                break;
            case CamType.TopFollow:
                firstPersonCam.Priority = 0;
                thirdPersonCam.Priority = 0;
                freeLookCam.Priority = 0;
                topFollowCam.Priority = 1;
                topCam.Priority = 0;
                topFollowCam.Follow = player.transform;

                Cursor.lockState = CursorLockMode.None;

                secondActionCam.Priority = 0;
                secondFollowCam.Priority = 0;
                secondTopCam.Priority = 1;
                break;
            case CamType.Top:
                firstPersonCam.Priority = 0;
                thirdPersonCam.Priority = 0;
                freeLookCam.Priority = 0;
                topFollowCam.Priority = 0;
                topCam.Priority = 1;

                Cursor.lockState = CursorLockMode.None;

                secondActionCam.Follow = player.transform;
                secondActionCam.LookAt = player.transform;
                secondActionCam.Priority = 1;
                secondFollowCam.Priority = 0;
                secondTopCam.Priority = 0;
                break;
        }
    }

    void CreateSecondCam()
    {
        SecondCamRndTexture = new RenderTexture(960, 690, 24);
        SecondCamRndTexture.name = System.Guid.NewGuid().ToString();
        SecondCamRndTexture.Create();
        secondCam.targetTexture = SecondCamRndTexture;
    }
}