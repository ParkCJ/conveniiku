using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyable : MonoBehaviour
{
    const float shrinkRotationSpeed = 100.0f;
    const float shrinkSpeed = 0.99f;
    const float destroyRate = 0.3f;

    [Header("Config")]
    [SerializeField] int lifeSpan;
    [SerializeField] int score;

    [Header("Effect")]
    [SerializeField] ParticleSystem passAwayParticle;
    [SerializeField] ParticleSystem destroyParticle;
    [SerializeField] AudioClip passAwaySound;
    [SerializeField] AudioClip destroySound;

    [Header("UI")]
    [SerializeField] protected GameObject pointUIPrefab;

    GameManager gm;
    Rigidbody objectRb;

    float currScaleX;

    void Awake()
    {
        gm = GameManager.Instance;
        objectRb = GetComponent<Rigidbody>();

        currScaleX = transform.localScale.x;
    }

    // Start is called before the first frame update
    void Start()
    {
        lifeSpan = gm.gdPlayerData.BattleMode && GetComponent<Enemy>() != null ? int.MaxValue : lifeSpan;

        if (lifeSpan > 0)
            StartCoroutine(CheckLifeSpan());
    }

    IEnumerator CheckLifeSpan()
    {
        while (lifeSpan > 0)
        {
            yield return new WaitForSeconds(1);
            lifeSpan--;
        }

        StartCoroutine(StartPassAway());
    }

    IEnumerator StartPassAway()
    {
        if (objectRb != null)
        {
            objectRb.velocity = Vector3.zero;
            objectRb.angularVelocity = Vector3.zero;
        }

        // destroy all scripts
        foreach (MonoBehaviour script in GetComponents<MonoBehaviour>())
        {
            if (script != this)
                Destroy(script);
        }

        Instantiate(passAwayParticle, transform.position, passAwayParticle.transform.rotation);

        float scaleToDestroy = currScaleX * destroyRate;

        while (transform.localScale.x > scaleToDestroy)
        {
            transform.Rotate(Vector3.up, shrinkRotationSpeed * Time.deltaTime);
            transform.localScale *= shrinkSpeed;

            yield return new WaitForFixedUpdate();
        }

        AudioSource.PlayClipAtPoint(passAwaySound, transform.position, 0.1f);
        Destroy(gameObject);
    }

    public void PassAway()
    {
        lifeSpan = 0;
    }

    public void DestroyNow()
    {
        Instantiate(destroyParticle, transform.position, destroyParticle.transform.rotation);
        AudioSource.PlayClipAtPoint(destroySound, transform.position);

        if (gm.gdPlayerData.BattleMode && pointUIPrefab != null)
        {
            GameObject obj = Instantiate(pointUIPrefab, transform.position, Quaternion.identity);
            obj.GetComponent<PointUI>().SetText(score);
            obj.SetActive(true);

            gm.gameCtrl.AddScore((int)score);
        }

        Destroy(gameObject);
    }
}