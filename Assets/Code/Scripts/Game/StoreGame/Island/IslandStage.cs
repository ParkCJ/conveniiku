using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IslandStage : MonoBehaviour
{
    [SerializeField] Island island;
    [SerializeField] int stageIdx;

    void OnMouseDown()
    {
        island.SelectStage(stageIdx);
    }
}
