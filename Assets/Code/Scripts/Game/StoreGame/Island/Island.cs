using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using Cinemachine;

public class Island : MonoBehaviour
{
    const float playerMoveSpeed = 1f;

    [Header("Island")]
    [SerializeField] GameObject player;
    [SerializeField] GameObject groundAnimals;
    [SerializeField] GameObject skyAnimals;

    [Header("Prefab (Environment)")]
    [SerializeField] Material[] skyboxMorningMats;
    [SerializeField] Material[] skyboxAfternoonMats;
    [SerializeField] Material[] skyboxEveningMats;
    [SerializeField] GameObject nightDomePrefab;
    [SerializeField] GameObject cloudAnchorPrefab;
    [SerializeField] AudioSource[] soundscapePrefabs;
    FourTime fourTime = FourTime.Morning;
    GameObject nightDome;
    GameObject cloudContainer;
    float windStrength;

    [Header("Stage")]
    [SerializeField] CinemachinePathBase stageDollyTrack;
    Dictionary<int, int> stageWPDic = new Dictionary<int, int>();
    public float currWP = 0;
    public int currStageIdx;

    [Header("Shop")]
    [SerializeField] MeshRenderer shopNeonRndr;
    [SerializeField] Material neonDayBlue;
    [SerializeField] Material neonDayRed;
    [SerializeField] Material neonNightBlue;
    [SerializeField] Material neonNightRed;

    [Header("Cam")]
    [SerializeField] GameObject titleCamAnchor;
    [SerializeField] CinemachineVirtualCamera titleCam;
    [SerializeField] CinemachineVirtualCamera titleZoomCam;
    [SerializeField] CinemachineVirtualCamera arcadeCam;
    [SerializeField] CinemachineVirtualCamera topCam;
    [SerializeField] CinemachineVirtualCamera sideCam;

    GameManager gm;

    void Awake()
    {
        gm = GameManager.Instance;

        InitIsland();
    }

    void InitIsland()
    {
        // Animals
        foreach (Transform animal in groundAnimals.transform)
            StartCoroutine(IdleGroundAnimal(animal));

        foreach (Transform anchor in skyAnimals.transform)
            StartCoroutine(RotateSkyAnimal(anchor));

        // Enviroment
        CreateEnvironment(fourTime);

        // soundscape
        foreach (AudioSource soundscape in soundscapePrefabs)
            Instantiate(soundscape, Vector3.zero, soundscape.transform.rotation);

        // Waypoint
        stageWPDic.Add(100, 0);
        stageWPDic.Add(101, 2);
        stageWPDic.Add(102, 4);
        stageWPDic.Add(103, 6);
        stageWPDic.Add(104, 8);
        stageWPDic.Add(105, 10);
        stageWPDic.Add(106, 12);
        stageWPDic.Add(107, 14);
        stageWPDic.Add(108, 15);
        stageWPDic.Add(500, 16);
        stageWPDic.Add(501, 17);
        stageWPDic.Add(502, 18);
        stageWPDic.Add(503, 19);
        stageWPDic.Add(504, 20);
        stageWPDic.Add(505, 21);
        stageWPDic.Add(506, 22);
    }

    // Animals
    IEnumerator IdleGroundAnimal(Transform animal)
    {
        bool eatFlag = false;

        Animator anim = animal.GetComponentInChildren<Animator>();
        anim.SetFloat("Speed_f", 0);

        while (true)
        {
            anim.SetBool("Eat_b", eatFlag);

            yield return new WaitForSeconds(Random.Range(3, 10.0f));
            eatFlag = !eatFlag;
        }
    }

    IEnumerator RotateSkyAnimal(Transform anchor)
    {
        float speed = Random.Range(-35, 35);
        while (true)
        {
            anchor.Rotate(Vector3.up * speed * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }
    }

    // Environment
    public void CreateEnvironment(FourTime fourTime)
    {
        // direct light
        Light dirLight = GameObject.Find("Directional Light").GetComponent<Light>();
        dirLight.gameObject.transform.rotation = Quaternion.Euler(new Vector3(TimeHandler.GetSunXRot(fourTime), -30, 0));

        // postprocessing & Fourtime
        Volume ppVol = GameObject.Find("Global Volume").GetComponent<Volume>();
        Bloom ppBloom;
        ppVol.profile.TryGet(out ppBloom);

        Material[] shopNeonMats;
        switch (fourTime)
        {
            case FourTime.Morning:
                RenderSettings.skybox = skyboxMorningMats[Random.Range(0, skyboxMorningMats.Length)];
                dirLight.color = new Color32(255, 244, 214, 255);
                dirLight.intensity = 1f;
                Destroy(nightDome);

                ppBloom.intensity.value = 0.25f;

                // Shop neon
                shopNeonMats = shopNeonRndr.materials;
                shopNeonMats[0] = neonDayBlue;
                shopNeonMats[1] = neonDayRed;
                shopNeonRndr.materials = shopNeonMats;
                break;
            case FourTime.Afternoon:
                RenderSettings.skybox = skyboxAfternoonMats[Random.Range(0, skyboxAfternoonMats.Length)];
                RenderSettings.ambientIntensity = 1.05f;
                dirLight.color = new Color32(255, 244, 214, 255);
                dirLight.intensity = 1f;
                Destroy(nightDome);

                ppBloom.intensity.value = 0.5f;

                // Shop neon
                shopNeonMats = shopNeonRndr.materials;
                shopNeonMats[0] = neonDayBlue;
                shopNeonMats[1] = neonDayRed;
                shopNeonRndr.materials = shopNeonMats;
                break;
            case FourTime.Evening:
                RenderSettings.skybox = skyboxEveningMats[Random.Range(0, skyboxEveningMats.Length)];
                RenderSettings.ambientMode = AmbientMode.Flat;
                RenderSettings.ambientLight = new Color32(143, 102, 90, 255);
                dirLight.color = new Color32(255, 244, 214, 255);
                dirLight.intensity = 1f;
                Destroy(nightDome);

                ppBloom.intensity.value = 0.75f;

                // Shop neon
                shopNeonMats = shopNeonRndr.materials;
                shopNeonMats[0] = neonDayBlue;
                shopNeonMats[1] = neonDayRed;
                shopNeonRndr.materials = shopNeonMats;
                break;
            case FourTime.Night:
                dirLight.color = new Color32(244, 175, 3, 255);
                dirLight.intensity = 0.175f;
                if (nightDome == null)
                    nightDome = Instantiate(nightDomePrefab);

                ppBloom.intensity.value = 1f;

                // Shop neon
                shopNeonMats = shopNeonRndr.materials;
                shopNeonMats[0] = neonNightBlue;
                shopNeonMats[1] = neonNightRed;
                shopNeonRndr.materials = shopNeonMats;
                break;
        }

        // Wind
        windStrength = Random.Range(-5, 5);

        // Cloud
        Destroy(cloudContainer);
        cloudContainer = new GameObject("Cloud container");
        for (int i = 0; i < 50; i++)
        {
            CloudAnchor cloudAnchor = Instantiate(cloudAnchorPrefab, Vector3.up * 0.25f, cloudAnchorPrefab.transform.rotation, cloudContainer.transform).GetComponent<CloudAnchor>();
            cloudAnchor.windStrength = windStrength;
        }
    }

    // Stage
    public void SelectStage(int stageIdx)
    {
        if (gm.dbHandler.StageMstDic[stageIdx].Name == "Shop")
            gm.LoadScene("Shop");
        else if (stageIdx >= 100 && stageIdx <= 108)
        {
            // Arcade
            int newWP = stageWPDic[stageIdx];
            MoveToWP(newWP);

            currWP = newWP;
            currStageIdx = stageIdx;
        }
        else
        {
            // Tour
            int newWP = stageWPDic[stageIdx];

            Vector3 wpPos = stageDollyTrack.EvaluatePositionAtUnit(newWP, CinemachinePathBase.PositionUnits.PathUnits);
            player.transform.position = wpPos + Vector3.up;

            currStageIdx = stageIdx;
        }
    }

    public IEnumerator MovePlayer(int stageIdx)
    {
        int newWP = stageWPDic[stageIdx];

        Animator playerAnim = player.GetComponentInChildren<Animator>();

        while (currWP < newWP)
        {
            playerAnim.SetFloat("Speed_f", 1f);
            MoveToWP(currWP + playerMoveSpeed * Time.deltaTime);

            yield return new WaitForEndOfFrame();
        }

        currWP = newWP;
        currStageIdx = stageIdx;
        playerAnim.SetFloat("Speed_f", 0);
    }

    void MoveToWP(float newWP)
    {
        currWP = stageDollyTrack.StandardizeUnit(newWP, CinemachinePathBase.PositionUnits.PathUnits);
        player.transform.position = stageDollyTrack.EvaluatePositionAtUnit(currWP, CinemachinePathBase.PositionUnits.PathUnits);
        player.transform.rotation = stageDollyTrack.EvaluateOrientationAtUnit(currWP, CinemachinePathBase.PositionUnits.PathUnits);
    }

    // Camera
    void LateUpdate()
    {
        if (titleCamAnchor != null)
            titleCamAnchor.transform.Rotate(Vector3.up * -5f * Time.deltaTime);
    }

    public void SetCamera(IslandCamType camType)
    {
        switch(camType)
        {
            case IslandCamType.TitleCam:
                titleCam.Priority = 1;
                titleZoomCam.Priority = 0;
                arcadeCam.Priority = 0;
                topCam.Priority = 0;
                sideCam.Priority = 0;
                break;
            case IslandCamType.TitleZoomCam:
                titleCam.Priority = 0;
                titleZoomCam.Priority = 1;
                arcadeCam.Priority = 0;
                topCam.Priority = 0;
                sideCam.Priority = 0;
                break;
            case IslandCamType.ArcadeCam:
                titleCam.Priority = 0;
                titleZoomCam.Priority = 0;
                arcadeCam.Priority = 1;
                topCam.Priority = 0;
                sideCam.Priority = 0;
                break;
            case IslandCamType.TopCam:
                titleCam.Priority = 0;
                titleZoomCam.Priority = 0;
                arcadeCam.Priority = 0;
                topCam.Priority = 1;
                sideCam.Priority = 0;
                break;
            case IslandCamType.SideCam:
                titleCam.Priority = 0;
                titleZoomCam.Priority = 0;
                arcadeCam.Priority = 0;
                topCam.Priority = 0;
                sideCam.Priority = 1;
                break;
        }
    }
}
