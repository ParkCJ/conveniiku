using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lava : MonoBehaviour
{
    const float force = 50000f;//35000f;
    const float radius = 1;
    const float lift = 3.0f;

    const float abnDuration = 4.5f;
    const float underAtkAngle = 90f;
    const float underAtkDur = 3f;

    [Header("Particle")]
    [SerializeField] ParticleSystem lavaExplosion;

    [Header("Audio")]
    [SerializeField] AudioClip explosionSound;

    bool isHit = false;

    Renderer lavaRndr;

    GameManager gm;

    void Awake()
    {
        gm = GameManager.Instance;
    }

    void Start()
    {
        lavaRndr = GetComponent<Renderer>();
        lavaRndr.sharedMaterial.DisableKeyword("_EMISSION");

        if (gm.gdStageMst != null && gm.gdStageMst.FourTime == FourTime.Night)
            lavaRndr.sharedMaterial.EnableKeyword("_EMISSION");
    }

    void OnCollisionEnter(Collision collision)
    {
        // collision이 플레이어나 동물등일때만 폭발한다. 영향도 플레이어나 동물에게만 미친다.
        if (collision.gameObject.layer == (int)Layer.Player || collision.gameObject.layer == (int)Layer.Enemy || collision.gameObject.layer == (int)Layer.Dragon)
        {
            Instantiate(lavaExplosion, transform.position, lavaExplosion.transform.rotation);
            AudioSource.PlayClipAtPoint(explosionSound, transform.position);

            Collider[] colliders = Physics.OverlapSphere(transform.position, radius);
            foreach (Collider hit in colliders)
            {
                Rigidbody rb = hit.GetComponent<Rigidbody>();

                if (rb != null && (rb.gameObject.layer == (int)Layer.Player || collision.gameObject.layer == (int)Layer.Enemy || collision.gameObject.layer == (int)Layer.Dragon))
                {
                    rb.AddExplosionForce(force, transform.position, radius, lift);
                    isHit = true;

                    Creature creature = rb.transform.GetComponent<Creature>();
                    if (creature != null)
                    {
                        creature.SetAbnState(AbnormalState.Burn, abnDuration, 0.99f);
                        creature.SetAttacked(underAtkAngle, underAtkDur, 50);
                    }
                }
            }

            if (isHit)
                Destroy(gameObject);
        }
    }
}
