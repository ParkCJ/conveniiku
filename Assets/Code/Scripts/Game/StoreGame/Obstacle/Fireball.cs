using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : MonoBehaviour
{
    const float maxBallSize = 0.5f;
    const float sizeGrowRate = 1.05f;
    const float fireUpDelay = 0.5f;
    const float readyToFireDelay = 1f;
    const float lifeSpan = 10;
    const float explForceVFoward = 3;
    const float explForceVUp = 5;
    const float explForceIFoward = 300;
    const float explForceIUp = 500;
    const float abnDuration = 2.5f;
    const float underAtkAngle = 45f;
    const float underAtkDur = 1.5f;

    [SerializeField] ParticleSystem fireBallParticle;
    [SerializeField] ParticleSystem explosionParticle;
    [SerializeField] AudioClip fireUpSound;
    [SerializeField] AudioClip explosionSound;

    public bool IsReadyToFire { get; private set; }

    Rigidbody rb;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Start()
    {
        float xTorque = (Random.value > 0.5f ? Random.Range(1, rb.maxAngularVelocity) : -Random.Range(1, rb.maxAngularVelocity));
        float yTorque = (Random.value > 0.5f ? Random.Range(1, rb.maxAngularVelocity) : -Random.Range(1, rb.maxAngularVelocity));
        float zTorque = (Random.value > 0.5f ? Random.Range(1, rb.maxAngularVelocity) : -Random.Range(1, rb.maxAngularVelocity));

        rb.AddTorque(transform.right * xTorque, ForceMode.Impulse);
        rb.AddTorque(transform.up * yTorque, ForceMode.Impulse);
        rb.AddTorque(transform.forward * zTorque, ForceMode.Impulse);

        StartCoroutine(CreateBall());
    }

    IEnumerator CreateBall()
    {
        float maxGlobalScaleY = maxBallSize / transform.parent.localScale.y;
        while (transform.localScale.y < maxGlobalScaleY)
        {
            yield return new WaitForFixedUpdate();
            transform.localScale *= sizeGrowRate;
        }

        StartCoroutine(FireUp());
        StartCoroutine(DestroySelf());
    }

    IEnumerator FireUp()
    {
        yield return new WaitForSeconds(fireUpDelay);

        fireBallParticle.gameObject.SetActive(true);
        AudioSource.PlayClipAtPoint(fireUpSound, transform.position, 5.0f);

        StartCoroutine(ReadyToFire());
    }

    IEnumerator DestroySelf()
    {
        yield return new WaitForSeconds(lifeSpan);

        Explode();
    }

    IEnumerator ReadyToFire()
    {
        yield return new WaitForSeconds(readyToFireDelay);

        IsReadyToFire = true;
    }

    void Explode()
    {
        Instantiate(explosionParticle, transform.position, explosionParticle.transform.rotation);
        AudioSource.PlayClipAtPoint(explosionSound, transform.position, 3.0f);

        Destroy(gameObject);
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.rigidbody != null)
        {
            Vector3 forward = (collision.transform.position - transform.position).normalized;

            if (collision.gameObject.layer == (int)Layer.Player || collision.gameObject.layer == (int)Layer.Enemy || collision.gameObject.layer == (int)Layer.Dragon)
            {
                collision.rigidbody.AddForce(forward * explForceVFoward, ForceMode.VelocityChange);
                collision.rigidbody.AddForce(Vector3.up * explForceVUp, ForceMode.VelocityChange);

                Creature creature = collision.transform.GetComponent<Creature>();
                if (creature != null)
                {
                    creature.SetAbnState(AbnormalState.Burn, abnDuration, 0.2f);
                    creature.SetAttacked(underAtkAngle, underAtkDur, 20f);
                }
            }
            else
            {
                collision.rigidbody.AddForce(forward * explForceIFoward, ForceMode.Impulse);
                collision.rigidbody.AddForce(Vector3.up * explForceIUp, ForceMode.Impulse);
            }
        }

        Explode();
    }
}