using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class HammerObst : MonoBehaviour
{
    Rigidbody rb;
    public Rigidbody stickRb;
    public Rigidbody hammerRb;

    public FixedJoint stickJoint;

    List<float> angleList = new List<float>();

    float rotSpeed = 50;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        rotSpeed = Random.Range(rotSpeed * 0.9f, rotSpeed * 1.1f);
        hammerRb.mass = Random.Range(hammerRb.mass * 0.9f, hammerRb.mass * 1.1f);
    }

    // Start is called before the first frame update
    void Start()
    {
        Vector3 eulerRot = transform.rotation.eulerAngles;
        eulerRot.z = Random.Range(-1.0f, 1.0f);
        transform.rotation = Quaternion.Euler(eulerRot);

        StartCoroutine(RecordAngle());
        StartCoroutine(Reset());
    }

    float GetAngle()
    {
        return transform.eulerAngles.z > 180 ? transform.eulerAngles.z - 360 : transform.eulerAngles.z;
    }

    IEnumerator RecordAngle()
    {
        while(true)
        {
            angleList.Add(GetAngle());
            yield return new WaitForSeconds(0.5f);
        }
    }

    IEnumerator Reset()
    {
        while (true)
        {
            float diff = angleList.Max() - angleList.Min();
            float currAngle = GetAngle();

            angleList.Clear();

            // reset position
            if (diff < 45)
            {
                rb.isKinematic = true;
                stickRb.isKinematic = true;
                hammerRb.isKinematic = true;

                if (currAngle >= 0)
                {
                    while (currAngle < 170)
                    {
                        transform.Rotate(Vector3.forward * rotSpeed * Time.deltaTime);
                        currAngle = GetAngle();

                        yield return new WaitForFixedUpdate();
                    }
                }
                else
                {
                    while (currAngle > -170)
                    {
                        transform.Rotate(-Vector3.forward * rotSpeed * Time.deltaTime);
                        currAngle = GetAngle();

                        yield return new WaitForFixedUpdate();
                    }
                }

                rb.isKinematic = false;
                stickRb.isKinematic = false;
                hammerRb.isKinematic = false;
            }

            yield return new WaitForSeconds(3);
        }
    }
}
