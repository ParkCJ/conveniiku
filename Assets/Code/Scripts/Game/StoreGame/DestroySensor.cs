using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroySensor : MonoBehaviour
{
    GameManager gm;

    void Awake()
    {
        gm = GameManager.Instance;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == (int)Layer.Player)
        {
            gm.gameCtrl.KillPlayer();
        }
        else
        {
            Destroy(other.gameObject);
        }
    }
}
