using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallBlock : Block
{
    const float shrinkRate = 0.9f;

    [Header("Audio")]
    [SerializeField] AudioClip fallSound;

    protected override void Awake()
    {
        base.Awake();

        activateDelay = 0.6f;
        sleepDelay = 0;
    }

    protected override void WhenActivated()
    {
        base.WhenActivated();

        rb.isKinematic = false;
        gameObject.layer = (int)Layer.ActiveGround;

        transform.localScale *= shrinkRate;

        AudioSource.PlayClipAtPoint(fallSound, transform.position);
    }
}