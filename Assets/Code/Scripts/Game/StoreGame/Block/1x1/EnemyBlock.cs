using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBlock : Block
{
    [Header("Animal")]
    [SerializeField] GameObject[] enemyPrefabs;

    [Header("Audio")]
    [SerializeField] AudioClip spawnSound;

    protected override void Awake()
    {
        base.Awake();

        activateDelay = 2;
        sleepDelay = 0;
    }

    protected override void WhenActivated()
    {
        base.WhenActivated();

        int idx = Random.Range(0, enemyPrefabs.Length);

        Instantiate(enemyPrefabs[idx], transform.position + Vector3.up, Quaternion.identity);

        AudioSource.PlayClipAtPoint(spawnSound, transform.position, 2.0f);
    }
}