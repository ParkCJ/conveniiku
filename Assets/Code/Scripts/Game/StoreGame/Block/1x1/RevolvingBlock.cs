using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RevolvingBlock : Block
{
    public GameObject pivot;
    public float rotSpeed = 500;

    protected override void Awake()
    {
        base.Awake();

        sleepDelay = 60;
    }

    protected override void WhenActivated()
    {
        base.WhenActivated();

        StartCoroutine(Up());
    }

    IEnumerator Up()
    {
        pivot.SetActive(true);

        while (pivot.transform.localScale.y < 1)
        {
            pivot.transform.localScale += Vector3.up * Time.deltaTime;
            yield return new WaitForFixedUpdate();
        }

        while (currState == State.Activated)
        {
            pivot.transform.Rotate(Vector3.up * rotSpeed * Time.deltaTime);
            yield return new WaitForFixedUpdate();
        }
    }

    protected override void WhenSleep()
    {
        base.WhenSleep();

        StartCoroutine(Down());
    }

    IEnumerator Down()
    {
        rnd.material = activatedMat;

        while (pivot.transform.localScale.y > 0)
        {
            pivot.transform.localScale -= Vector3.up * Time.deltaTime;

            yield return new WaitForFixedUpdate();
        }

        pivot.SetActive(false);

        rnd.material = sleepMat;
    }
}
