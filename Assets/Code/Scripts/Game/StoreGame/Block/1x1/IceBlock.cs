using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IceBlock : Block
{
    [SerializeField] GameObject iceParticle;
    [SerializeField] AudioClip iceSound;

    Dictionary<GameObject, Creature> collisions;

    protected override void Awake()
    {
        base.Awake();

        sleepDelay = 3;

        collisions = new Dictionary<GameObject, Creature>();
    }

    protected override void WhenActivated()
    {
        base.WhenActivated();
        iceParticle.gameObject.SetActive(true);
        AudioSource.PlayClipAtPoint(iceSound, transform.position);
    }

    protected override void OnCollisionEnter(Collision collision)
    {
        base.OnCollisionEnter(collision);
        if (collision.gameObject.layer == (int)Layer.Player || collision.gameObject.layer == (int)Layer.Enemy || collision.gameObject.layer == (int)Layer.Dragon)
        {
            collisions.Add(collision.gameObject, collision.transform.GetComponent<Creature>());
        }
    }

    protected override void OnCollisionStay(Collision collision)
    {
        base.OnCollisionStay(collision);
        if (currState == State.Activated && collisions.ContainsKey(collision.gameObject) && collisions[collision.gameObject] != null)
        {
            collisions[collision.gameObject].SetAbnState(AbnormalState.Frozen, 3.5f);
        }
    }

    protected override void OnCollisionExit(Collision collision)
    {
        base.OnCollisionExit(collision);
        if (collisions.ContainsKey(collision.gameObject))
        {
            collisions.Remove(collision.gameObject);
        }
    }

    protected override void WhenSleep()
    {
        base.WhenSleep();
        Destroy(iceParticle);
    }
}
