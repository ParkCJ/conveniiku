using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpBlock : Block
{
    const float shrinkRate = 0.9975f;
    const float upForce = 10000;

    [Header("Audio")]
    [SerializeField] AudioClip jumpSound;

    protected override void Awake()
    {
        base.Awake();

        sleepDelay = 0;
    }

    protected override void WhenActivated()
    {
        base.WhenActivated();

        rb.isKinematic = false;
        gameObject.layer = (int)Layer.ActiveGround;

        transform.localScale *= shrinkRate;
        rb.AddForce(Vector3.up * upForce, ForceMode.Impulse);

        AudioSource.PlayClipAtPoint(jumpSound, transform.position);
    }
}
