using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrampolineBlock : Block
{
    [Header("Audio")]
    [SerializeField] AudioClip trampolineSound;

    [Header("Setting")]
    [SerializeField] float mass = 1000;
    [SerializeField] float minSpring = 10000;
    [SerializeField] float maxSpring = 10000;

    protected override void Awake()
    {
        base.Awake();

        activateDelay = 0f;
    }

    protected override void OnCollisionEnter(Collision collision)
    {
        if (currState == State.InActive && collision.gameObject.layer == (int)Layer.Player)
            StartCoroutine(Activate());
    }

    protected override void WhenActivated()
    {
        base.WhenActivated();

        AudioSource.PlayClipAtPoint(trampolineSound, transform.position);

        rb.isKinematic = false;
        gameObject.layer = (int)Layer.ActiveGround;

        // 0. scale
        transform.localScale = new Vector3(1f, 1f, 1f);

        // 1. rb
        rb.mass = mass;
        rb.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;

        // 2. spring joint
        SpringJoint spJoint = gameObject.AddComponent<SpringJoint>();
        Rigidbody floorRb = GameObject.Find("Floor").GetComponent<Rigidbody>();

        // 1) connectedBody
        spJoint.connectedBody = floorRb;

        // 2) anchor
        spJoint.autoConfigureConnectedAnchor = false;
        spJoint.anchor = new Vector3(0, 0.5f, 0);

        Vector3 connectedAnchor = floorRb.transform.InverseTransformPoint(transform.position);
        spJoint.connectedAnchor = new Vector3(connectedAnchor.x, 2.5f, connectedAnchor.z);

        // 3) etc
        spJoint.spring = Random.Range(minSpring, maxSpring);
        spJoint.minDistance = 0;
        spJoint.maxDistance = 0;
        spJoint.enableCollision = true;
    }
}
