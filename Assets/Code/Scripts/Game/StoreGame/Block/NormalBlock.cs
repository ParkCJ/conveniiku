using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalBlock : Block
{
    protected override void Awake()
    {
        base.Awake();

        activateDelay = 0;
        sleepDelay = 0;
    }
}
