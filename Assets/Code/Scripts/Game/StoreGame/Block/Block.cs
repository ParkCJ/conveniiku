using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Block : MonoBehaviour
{
    public enum State
    {
        InActive,
        Activating,
        Activated,
        Sleep
    }

    [Header("Materials")]
    [SerializeField] protected Material activatingMat;
    [SerializeField] protected Material activatedMat;
    [SerializeField] protected Material sleepMat;

    protected Renderer rnd;
    protected Rigidbody rb;
    public State currState;

    protected float activateDelay = 1;
    protected float sleepDelay = 10;
    protected int blockCnt = 1;

    protected GameManager gm;

    protected virtual void Awake()
    {
        rb = GetComponent<Rigidbody>();
        rnd = GetComponent<Renderer>();

        gm = GameManager.Instance;
    }

    void FixedUpdate()
    {
        switch (currState)
        {
            case State.Activated:
                WhileActivated();
                break;
        }
    }

    protected virtual void OnCollisionEnter(Collision collision)
    {
        if (currState == State.InActive && collision.gameObject.layer == (int)Layer.Player)
            StartCoroutine(Activate());
    }

    protected virtual void OnCollisionStay(Collision collision)
    {

    }

    protected virtual void OnCollisionExit(Collision collision)
    {

    }

    protected virtual void OnTriggerEnter(Collider other)
    {
        if (currState == State.InActive && other.gameObject.layer == (int)Layer.Player)
            StartCoroutine(Activate());
    }

    protected IEnumerator Activate()
    {
        currState = State.Activating;
        rnd.material = activatingMat;

        WhenActivating();

        while (activateDelay > 0)
        {
            activateDelay -= Time.deltaTime;
            yield return new WaitForFixedUpdate();
        }
        
        currState = State.Activated;
        rnd.material = activatedMat;

        WhenActivated();

        while(sleepDelay > 0)
        {
            sleepDelay -= Time.deltaTime;
            yield return new WaitForFixedUpdate();
        }

        currState = State.Sleep;
        rnd.material = sleepMat;

        WhenSleep();
    }

    protected virtual void WhenActivating()
    {
        if (gm != null && gm.gameCtrl != null)
            gm.gameCtrl.AddBlock(blockCnt);

        //Debug.Log("WhileActivating");
    }

    protected virtual void WhenActivated()
    {
        //Debug.Log("WhenActivated");
    }

    protected virtual void WhileActivated()
    {
        //Debug.Log("WhileActivated");
    }

    protected virtual void WhenSleep()
    {
        //Debug.Log("WhenSleep");
    }

    protected virtual void ForceActivation()
    {
        blockCnt = 0;

        StartCoroutine(Activate());
    }

    public void StopActivationNow()
    {
        StopAllCoroutines();

        currState = State.Sleep;
        rnd.material = sleepMat;

        WhenSleep();
    }
}
