using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class VolcanoBlock : Block
{
    const float lev2UpWait = 0.25f;
    const float lev3UpWait = 0.5f;
    const float firstExplosionWait = 2f;
    const float upSpeed = 5;
    const int spoutOutForceMin = 700;
    const int spoutOutForceMax = 800;
    float firstEarthquakeMinRate = 1.35f;
    float firstEarthquakeMaxRate = 2;
    float spoutOutIntvMin = 0.5f;
    float spoutOutIntvMax = 1;

    public GameObject lev1;
    public GameObject lev2;
    public GameObject lev3;
    public GameObject crater;
    public GameObject lavaPrefab;

    [Header("Particle")]
    [SerializeField] GameObject craterFlame;

    [Header("Audio")]
    [SerializeField] AudioClip firstExplosionSound;
    [SerializeField] AudioClip spoutOutSound;

    CinemachineImpulseSource impSrc;

    BoxCollider cl;
    List<GameObject> cubes = new List<GameObject>();
    Rigidbody[] cubeRbs;

    protected override void Awake()
    {
        base.Awake();

        sleepDelay = 60;
        blockCnt = 5;

        cubeRbs = GetComponentsInChildren<Rigidbody>(true);
        cl = GetComponent<BoxCollider>();
        impSrc = GetComponent<CinemachineImpulseSource>();

        foreach (Transform transform in GetComponentsInChildren<Transform>(true))
        {
            if (transform.name == "Cube")
                cubes.Add(transform.gameObject);
        }
    }

    protected override void WhenActivated()
    {
        base.WhenActivated();

        StartCoroutine(Up());
    }

    IEnumerator Up()
    {
        // 1. Remove collider, renderer
        Destroy(cl);
        rnd.enabled = false;

        // 2. Enable levs
        lev1.SetActive(true);
        lev2.SetActive(true);
        lev3.SetActive(true);

        foreach (Rigidbody cubeRb in cubeRbs)
        {
            if (cubeRb != null)
                cubeRb.constraints = RigidbodyConstraints.FreezeRotation;
        }

        // 3. Lev1 up
        float upScale = 1;
        while (upScale < 2)
        {
            foreach (Transform child in lev1.transform)
                child.transform.localScale += Vector3.up * upSpeed * Time.deltaTime;
            foreach (Transform child in lev2.transform)
                child.transform.localScale += Vector3.up * upSpeed * Time.deltaTime;
            foreach (Transform child in lev3.transform)
                child.transform.localScale += Vector3.up * upSpeed * Time.deltaTime;

            upScale += upSpeed * Time.deltaTime;

            yield return new WaitForFixedUpdate();
        }

        // 4. Lev2 up
        yield return new WaitForSeconds(lev2UpWait);

        while (upScale < 3)
        {
            foreach (Transform child in lev2.transform)
                child.transform.localScale += Vector3.up * upSpeed * Time.deltaTime;
            foreach (Transform child in lev3.transform)
                child.transform.localScale += Vector3.up * upSpeed * Time.deltaTime;

            upScale += upSpeed * Time.deltaTime;

            yield return new WaitForFixedUpdate();
        }

        // 5. Lev3 up
        yield return new WaitForSeconds(lev3UpWait);

        while (upScale < 4)
        {
            foreach (Transform child in lev3.transform)
                child.transform.localScale += Vector3.up * upSpeed * Time.deltaTime;

            upScale += upSpeed * Time.deltaTime;

            yield return new WaitForFixedUpdate();
        }

        // 6. First explosion
        yield return new WaitForSeconds(firstExplosionWait);

        // 1) earthquake
        foreach (Rigidbody cubeRb in cubeRbs)
        {
            if (cubeRb != null)
                cubeRb.constraints = RigidbodyConstraints.FreezeAll;
        }

        // Uncheck blocks' Iskinematic
        GameObject blockContainer = GameObject.Find("Block container");
        if (blockContainer != null)
        {
            foreach (Transform child in blockContainer.transform)
            {
                if (child.gameObject == gameObject || child.name == "StartBlock(Clone)" || child.name == "GoalBlock(Clone)" || child.name == "NextBlock(Clone)")
                    continue;

                Rigidbody blockRb = child.GetComponent<Rigidbody>();
                if (blockRb != null)
                    blockRb.isKinematic = false;
            }
        }

        Vector3 originScale = lev1.transform.localScale;

        float earthquakeRate = Random.Range(firstEarthquakeMinRate, firstEarthquakeMaxRate);
        lev1.transform.localScale = new Vector3(lev1.transform.localScale.x * earthquakeRate, lev1.transform.localScale.y, lev1.transform.localScale.z * earthquakeRate);

        // 2) first spout out
        int lavaCnt = 3;
        while (lavaCnt > 0)
        {
            GameObject lava = Instantiate(lavaPrefab, crater.transform.position, Quaternion.identity);
            SpoutOut(lava);

            impSrc.GenerateImpulse(earthquakeRate * 5 * GameManager.Instance.gdPlayerData.ActionCamShakeRate / 10);

            lavaCnt--;
        }

        craterFlame.SetActive(true);
        AudioSource.PlayClipAtPoint(firstExplosionSound, crater.transform.position);

        // 2) after earthquake
        yield return new WaitForFixedUpdate();

        foreach (Rigidbody cubeRb in cubeRbs)
        {
            if (cubeRb != null)
                cubeRb.constraints = RigidbodyConstraints.None;
        }

        lev1.transform.localScale = originScale;

        // 7. Spout out
        while (currState == State.Activated)
        {
            if (crater != null)
            {
                lavaCnt = Random.Range(1, 3);
                while (lavaCnt > 0)
                {
                    GameObject lava = Instantiate(lavaPrefab, crater.transform.position, Quaternion.identity);
                    SpoutOut(lava);

                    AudioSource.PlayClipAtPoint(spoutOutSound, crater.transform.position);

                    lavaCnt--;
                }
            }

            yield return new WaitForSeconds(Random.Range(spoutOutIntvMin, spoutOutIntvMax));
        }
    }

    void SpoutOut(GameObject lava)
    {
        Rigidbody lavaRb = lava.GetComponent<Rigidbody>();

        lavaRb.AddForce(new Vector3(Random.Range(-0.3f, 0.3f), 1, Random.Range(-0.3f, 0.3f)) * Random.Range(spoutOutForceMin, spoutOutForceMax), ForceMode.Impulse);
        lavaRb.AddTorque(Random.Range(-100, 100), Random.Range(-100, 100), Random.Range(-100, 100), ForceMode.Impulse);

        impSrc.GenerateImpulse(GameManager.Instance.gdPlayerData.ActionCamShakeRate / 10);
    }

    protected override void ForceActivation()
    {
        blockCnt = 0;
        sleepDelay = 30;

        firstEarthquakeMinRate = 1;
        firstEarthquakeMaxRate = 1;

        StartCoroutine(Activate());
    }

    protected override void WhenSleep()
    {
        base.WhenSleep();

        if (crater != null)
            craterFlame.SetActive(false);

        foreach (GameObject cube in cubes)
        {
            if (cube != null)
                cube.GetComponent<MeshRenderer>().material = sleepMat;
        }
    }
}
