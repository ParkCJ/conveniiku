using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextBlock : MonoBehaviour
{
    GameManager gm;

    void Awake()
    {
        gm = GameManager.Instance;
    }

    void OnCollisionEnter(Collision collision)
    {
        if (gm.gameCtrl.gameProgress != GameProgress.Clear && collision.collider.gameObject.layer == (int)Layer.Player)
            gm.gameCtrl.GameClear();
    }
}
