using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalBlock : MonoBehaviour
{
    [SerializeField] MeshRenderer neonRndr;
    [SerializeField] Material goalNightBlue;
    [SerializeField] Material goalNightRed;

    GameManager gm;

    void Awake()
    {
        gm = GameManager.Instance;

        if (gm.gameMode == GameMode.Tour)
        {
            if (gm.gdPlayerData.LastFourTime == FourTime.Night)
            {
                var nightMats = neonRndr.materials;
                nightMats[0] = goalNightBlue;
                nightMats[1] = goalNightRed;

                neonRndr.materials = nightMats;
            }
        }
        else
        {
            if (gm.gdStageMst.FourTime == FourTime.Night)
            {
                var nightMats = neonRndr.materials;
                nightMats[0] = goalNightBlue;
                nightMats[1] = goalNightRed;

                neonRndr.materials = nightMats;
            }
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (gm.gameCtrl.gameProgress != GameProgress.Clear && collision.collider.gameObject.layer == (int)Layer.Player)
            gm.gameCtrl.GameClear();
    }
}
