using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZeroGravityBlock : Block
{
    const float minForce = 0.1f;
    const float maxForce = 0.5f;

    [SerializeField] AudioClip activatedSound;

    List<GameObject> cubes = new List<GameObject>();
    ZeroGravityBlockSensor sensor;

    protected override void Awake()
    {
        base.Awake();

        activateDelay = 1f;
        sleepDelay = 5.04f;
        blockCnt = 2;

        // cube
        foreach (Transform transform in GetComponentsInChildren<Transform>(true))
        {
            if (transform.name == "Cube")
                cubes.Add(transform.gameObject);
        }

        // sensor
        sensor = transform.Find("Sensor").GetComponent<ZeroGravityBlockSensor>();
    }

    protected override void WhenActivated()
    {
        base.WhenActivated();

        GetComponent<MeshRenderer>().enabled = false;
        Destroy(GetComponent<MeshFilter>());
        Destroy(GetComponent<BoxCollider>());
        Destroy(GetComponent<Rigidbody>());

        gameObject.layer = (int)Layer.ActiveGround;

        // cube
        foreach (GameObject cube in cubes)
            cube.SetActive(true);

        // sensor
        sensor.gameObject.SetActive(true);

        // Sound
        AudioSource.PlayClipAtPoint(activatedSound, transform.position, 0.5f);
    }

    protected override void WhileActivated()
    {
        base.WhileActivated();

        // cube
        int idx = Random.Range(0, cubes.Count);
        float rndForce = Random.Range(minForce, maxForce);

        if (cubes[idx] != null)
            cubes[idx].GetComponent<Rigidbody>().AddForce(Vector3.up * rndForce, ForceMode.VelocityChange);
    }

    protected override void WhenSleep()
    {
        base.WhenSleep();

        // cube
        foreach (GameObject cube in cubes)
        {
            if (cube != null)
                cube.GetComponent<BoxCollider>().material = null;
        }

        // sensor
        foreach (Rigidbody rb in sensor.SensoredRbList)
        {
            if (rb != null)
                rb.useGravity = true;
        }

        if (sensor != null)
            Destroy(sensor.gameObject);
    }
}
