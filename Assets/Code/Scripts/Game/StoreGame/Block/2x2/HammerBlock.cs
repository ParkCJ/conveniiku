using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class HammerBlock : Block
{
    const float rotSpeed = 50;
    const float upPosY = 7;
    const float upSpeed = 2;

    public Rigidbody stickRb;
    public Rigidbody hammerRb;

    public FixedJoint stickJoint;

    List<float> angleList = new List<float>();
    AudioSource audioSource;

    float GetAngle()
    {
        return transform.eulerAngles.z > 180 ? transform.eulerAngles.z - 360 : transform.eulerAngles.z;
    }

    protected override void Awake()
    {
        base.Awake();

        sleepDelay = 30000;

        audioSource = GetComponent<AudioSource>();
    }

    protected override void WhenActivated()
    {
        base.WhenActivated();

        gameObject.layer = (int)Layer.ActiveGround;

        StartCoroutine(Up());
    }

    IEnumerator Up()
    {
        audioSource.enabled = true;

        rb.isKinematic = true;
        stickRb.gameObject.SetActive(true);

        while (transform.position.y < upPosY)
        {
            transform.localPosition += Vector3.up * upSpeed * Time.deltaTime;
            yield return new WaitForFixedUpdate();
        }

        rb.isKinematic = false;
        rb.constraints = RigidbodyConstraints.FreezePosition | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY;

        StartCoroutine(RecordAngle());
        StartCoroutine(Reset());

        audioSource.enabled = false;
    }

    IEnumerator RecordAngle()
    {
        while (currState == State.Activated)
        {
            angleList.Add(GetAngle());
            yield return new WaitForSeconds(0.5f);
        }
    }

    IEnumerator Reset()
    {
        while (currState == State.Activated)
        {
            float diff = angleList.Max() - angleList.Min();
            float currAngle = GetAngle();

            angleList.Clear();

            // reset position
            if (diff < 45)
            {
                rb.isKinematic = true;
                stickRb.isKinematic = true;
                hammerRb.isKinematic = true;

                if (currAngle >= 0)
                {
                    while (currAngle < 170)
                    {
                        transform.Rotate(Vector3.forward * rotSpeed * Time.deltaTime);
                        currAngle = GetAngle();

                        yield return new WaitForFixedUpdate();
                    }
                }
                else
                {
                    while (currAngle > -170)
                    {
                        transform.Rotate(-Vector3.forward * rotSpeed * Time.deltaTime);
                        currAngle = GetAngle();

                        yield return new WaitForFixedUpdate();
                    }
                }

                rb.isKinematic = false;
                stickRb.isKinematic = false;
                hammerRb.isKinematic = false;
            }

            yield return new WaitForSeconds(3);
        }
    }

    protected override void WhenSleep()
    {
        base.WhenSleep();

        rb.constraints = RigidbodyConstraints.None;
        Destroy(stickJoint);
    }
}
