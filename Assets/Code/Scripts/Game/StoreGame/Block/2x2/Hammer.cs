using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hammer : MonoBehaviour
{
    [SerializeField] AudioClip impactSound;

    Rigidbody hammerRb;
    
    void Awake()
    {
        hammerRb = GetComponent<Rigidbody>();
    }

    void OnCollisionEnter(Collision collision)
    {
        float volume = hammerRb.velocity.magnitude / 10;
        AudioSource.PlayClipAtPoint(impactSound, transform.position, volume);
    }
}
