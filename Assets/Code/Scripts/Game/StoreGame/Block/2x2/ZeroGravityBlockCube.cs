using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZeroGravityBlockCube : MonoBehaviour
{
    [SerializeField] AudioClip conflictSound;

    Rigidbody rb;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    void OnCollisionEnter(Collision collision)
    {
        if (rb.useGravity)
        {
            AudioSource.PlayClipAtPoint(conflictSound, transform.position);
            Destroy(this);
        }
    }
}
