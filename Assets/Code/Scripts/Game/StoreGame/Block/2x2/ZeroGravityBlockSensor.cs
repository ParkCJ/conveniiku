using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZeroGravityBlockSensor : MonoBehaviour
{
    public List<Rigidbody> SensoredRbList { get; private set; } = new List<Rigidbody>();

    void OnTriggerEnter(Collider other)
    {
        if (other.attachedRigidbody != null && other.attachedRigidbody.useGravity)
        {
            other.attachedRigidbody.useGravity = false;
            SensoredRbList.Add(other.attachedRigidbody);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.attachedRigidbody != null && SensoredRbList.Contains(other.attachedRigidbody))
        {
            other.attachedRigidbody.useGravity = true;
            SensoredRbList.Remove(other.attachedRigidbody);
        }
    }
}
