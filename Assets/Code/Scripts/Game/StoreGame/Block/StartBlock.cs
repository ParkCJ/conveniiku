using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartBlock : MonoBehaviour
{
    [SerializeField] Renderer gridLineRndr;

    GameManager gm;

    void Awake()
    {
        gm = GameManager.Instance;

        if (gm.gdStageMst.FourTime == FourTime.Night)
            gridLineRndr.sharedMaterial.EnableKeyword("_EMISSION");
        else
            gridLineRndr.sharedMaterial.DisableKeyword("_EMISSION");
    }
}
