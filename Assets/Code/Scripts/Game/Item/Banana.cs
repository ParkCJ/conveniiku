using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Banana : Item
{
    protected override void Start()
    {
        base.Start();

        ItemMst = gm.dbHandler.ItemMstDic["Banana"];
    }
}