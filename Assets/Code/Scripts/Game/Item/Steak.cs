using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Steak : Item
{
    protected override void Start()
    {
        base.Start();

        ItemMst = gm.dbHandler.ItemMstDic["Steak"];
    }
}