using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Item : MonoBehaviour
{
    [Header("UI")]
    [SerializeField] protected GameObject pointUIPrefab;

    [Header("Particle")]
    [SerializeField] protected ParticleSystem getParticlePrefab;

    [Header("Sound")]
    [SerializeField] protected AudioClip getSound;

    public ItemMst ItemMst { get; protected set; }

    protected float rotationSpeed = 200;
    protected float shrinkRate = 0.75f;

    protected GameManager gm;

    protected virtual void Awake()
    {
        gm = GameManager.Instance;
    }

    protected virtual void Start()
    {
    }

    protected virtual void Update()
    {
        transform.Rotate(-Vector3.up * rotationSpeed * Time.deltaTime);
    }

    protected virtual void OnTriggerEnter(Collider other)
    {
        GetItem(other.gameObject);
    }

    protected virtual void OnCollisionEnter(Collision collision)
    {
        GetItem(collision.gameObject);
    }

    protected virtual void GetItem(GameObject other)
    {
        if (other.layer == (int)Layer.Player)
        {
            // 1. Add point
            switch (ItemMst.ItemType)
            {
                case ItemType.Life:
                    gm.gameCtrl.AddLife((int)ItemMst.PointCnt);
                    break;
                case ItemType.Score:
                    gm.gameCtrl.AddScore((int)ItemMst.PointCnt);
                    break;
                case ItemType.Money:
                    gm.gameCtrl.AddMoney((int)ItemMst.PointCnt);
                    break;
                case ItemType.Power:
                    gm.gameCtrl.AddPower(ItemMst.PointCnt);
                    break;
                case ItemType.Poison:
                    Creature creature = other.GetComponent<Creature>();
                    creature.SetPoisoned(ItemMst.PointCnt);
                    break;
                case ItemType.Skill:
                    gm.gameCtrl.ChangeSkill((int)ItemMst.PointCnt);
                    break;
                case ItemType.Bomb:
                    gm.gameCtrl.AddBomb((int)ItemMst.PointCnt);
                    break;
            }

            // 2. Show point ui
            if (ItemMst.ItemType == ItemType.Score && pointUIPrefab != null)
            {
                GameObject obj = Instantiate(pointUIPrefab, transform.position, Quaternion.identity);
                obj.GetComponent<PointUI>().SetText((int)ItemMst.PointCnt);
                obj.SetActive(true);
            }

            // 3. Play effect
            if (getParticlePrefab != null)
                Instantiate(getParticlePrefab, transform.position, getParticlePrefab.transform.rotation);

            // 4. Play sound
            if (getSound != null)
                AudioSource.PlayClipAtPoint(getSound, transform.position);

            BeforeDestroy();

            Destroy(gameObject);
        }
    }

    protected virtual void BeforeDestroy()
    {
    }
}