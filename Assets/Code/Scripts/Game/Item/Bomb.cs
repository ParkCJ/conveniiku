using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : Item
{
    protected override void Start()
    {
        base.Start();

        ItemMst = gm.dbHandler.ItemMstDic["Bomb"];
    }
}