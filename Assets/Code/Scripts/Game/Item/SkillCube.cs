using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillCube : Item
{
    const int skillLength = 4;
    const int toNextIntv = 3;

    [SerializeField] Material[] mats;

    MeshRenderer rndr;
    int currIdx = 0;

    protected override void Awake()
    {
        base.Awake();
        rndr = GetComponent<MeshRenderer>();
    }

    protected override void Start()
    {
        base.Start();

        ItemMst = new ItemMst(gm.dbHandler.ItemMstDic["SkillCube"].Name, gm.dbHandler.ItemMstDic["SkillCube"].ItemType, gm.dbHandler.ItemMstDic["SkillCube"].PointCnt);

        StartCoroutine(ToNext());
    }

    IEnumerator ToNext()
    {
        currIdx = Random.Range(0, skillLength);
        rndr.material = mats[currIdx];
        ItemMst.PointCnt = (int)currIdx;

        while (true)
        {
            yield return new WaitForSeconds(toNextIntv);

            currIdx = currIdx == skillLength - 1 ? 0 : currIdx + 1;
            rndr.material = mats[currIdx];
            ItemMst.PointCnt = (int)currIdx;
        }
    }
}