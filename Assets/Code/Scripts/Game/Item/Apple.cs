using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Apple : Item
{
    protected override void Start()
    {
        base.Start();

        ItemMst = gm.dbHandler.ItemMstDic["Apple"];
    }
}