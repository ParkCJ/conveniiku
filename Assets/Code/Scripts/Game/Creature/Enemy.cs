using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class Enemy : Creature
{
    protected enum State
    {
        Sleep,
        WakeUp,
        Move,
        Rest
    }

    [Header("Item")]
    [SerializeField] protected GameObject[] itemPrefabs;

    protected Destroyable destroyable;

    protected State currState;

    protected float wakeUpDuration = 0;
    protected float restDuration = 0;

    protected override void Awake()
    {
        base.Awake();
        destroyable = GetComponent<Destroyable>();
    }

    protected override void Start()
    {
        base.Start();
        StartCoroutine(WakeUp());
    }

    protected IEnumerator WakeUp()
    {
        currState = State.WakeUp;

        WhileWakeUp();

        yield return new WaitForSeconds(wakeUpDuration);

        BeforeMove();

        currState = State.Move;
    }

    protected abstract void WhileWakeUp();
    protected abstract void BeforeMove();

    protected void FixedUpdate()
    {
        if (currState == State.Move)
            Move();
    }

    protected abstract void Move();

    protected IEnumerator Rest()
    {
        currState = State.Rest;

        WhileRest();

        yield return new WaitForSeconds(restDuration);

        BeforeMove();

        currState = State.Move;
    }

    protected abstract void WhileRest();

    protected void LookAtObject(GameObject other, float? xAdjust = null, float? yAdjust = null, float? zAdjust = null)
    {
        Vector3 dir;
        if (other == null)
            dir = (bornPosition - transform.position).normalized;
        else
            dir = (other.transform.position - transform.position).normalized;

        dir.x = xAdjust ?? dir.x;
        dir.y = yAdjust ?? dir.y;
        dir.z = zAdjust ?? dir.z;

        if (dir != Vector3.zero)
            transform.forward = dir;
    }

    protected Vector3 GetLookAtDir(GameObject other, float? xAdjust = null, float? yAdjust = null, float? zAdjust = null)
    {
        Vector3 dir;
        if (other == null)
            dir = (bornPosition - transform.position).normalized;
        else
            dir = (other.transform.position - transform.position).normalized;

        dir.x = xAdjust ?? dir.x;
        dir.y = yAdjust ?? dir.y;
        dir.z = zAdjust ?? dir.z;

        if (dir != Vector3.zero)
            return dir;
        else
            return transform.forward;
    }

    protected override IEnumerator SetUnderAttack(float underAtkAngle, float underAtkDur, float damage = 0)
    {
        isUnderAttack = true;

        if (beforeAttackRot == null)
            beforeAttackRot = transform.rotation;

        anim.enabled = false;

        Vector3 newRot = beforeAttackRot.Value.eulerAngles;
        newRot.x += Random.Range(-underAtkAngle, 0);
        newRot.y += Random.Range(-underAtkAngle, underAtkAngle);
        newRot.z += Random.Range(-underAtkAngle, underAtkAngle);

        transform.rotation = Quaternion.Euler(newRot);

        DamageCreature(damage);

        yield return new WaitForSeconds(underAtkDur);

        if (abnState == AbnormalState.None)
            anim.enabled = true;

        transform.rotation = (Quaternion)beforeAttackRot;
        beforeAttackRot = null;

        isUnderAttack = false;
    }

    protected override void Dead()
    {
        base.Dead();

        destroyable.DestroyNow();

        if (GameManager.Instance.gdPlayerData.BattleMode && itemPrefabs.Length > 0)
        {
            GameObject itemPrefab = itemPrefabs[Random.Range(0, itemPrefabs.Length)];
            Instantiate(itemPrefab, transform.position, itemPrefab.transform.rotation);
        }
    }
}