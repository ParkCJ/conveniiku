using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EnemyGroundFire : EnemyGround
{
    protected enum MoveState
    {
        StartFire,
        Fire,
        AfterFire,
        Move
    }

    [SerializeField] protected GameObject fireballPrefab;
    [SerializeField] protected AudioClip fireSound;

    protected float moveDuration = 0;
    protected float afterFireDuration = 0;
    protected float fireballPosYOffset = 0;
    protected float fireballPosZOffset = 0;
    protected float fireballSpeed = 0;

    protected MoveState currMoveState = MoveState.StartFire;
    protected Fireball fireball;

    protected override void MoveOnTheGround()
    {
        if (abnState == AbnormalState.Frozen || abnState == AbnormalState.Burn || isUnderAttack)
            return;

        switch (currMoveState)
        {
            case MoveState.StartFire:
                StartCoroutine(Fire());

                anim.SetFloat("Speed_f", speed);
                anim.SetBool("Eat_b", false);
                break;
            case MoveState.Fire:
                LookAtObject(GameManager.Instance.gameCtrl.player, yAdjust: 0);

                anim.SetFloat("Speed_f", speed);
                anim.SetBool("Eat_b", false);
                break;
            case MoveState.AfterFire:
                anim.SetFloat("Speed_f", 0);
                anim.SetBool("Eat_b", true);
                break;
            case MoveState.Move:
                LookAtObject(GameManager.Instance.gameCtrl.player, yAdjust: 0);
                transform.Translate(Vector3.forward * Time.deltaTime * speed, Space.Self);

                anim.SetFloat("Speed_f", speed);
                anim.SetBool("Eat_b", false);

                if (!creatureAudio.isPlaying)
                    creatureAudio.PlayOneShot(moveSound, 0.1f);
                break;
        }
    }

    protected virtual IEnumerator Fire()
    {
        currMoveState = MoveState.Fire;

        Vector3 fireballPos = transform.position + (transform.up * fireballPosYOffset) + (transform.forward * fireballPosZOffset);
        fireball = Instantiate(fireballPrefab, fireballPos, fireballPrefab.transform.rotation, gameObject.transform).GetComponent<Fireball>();

        while (fireball != null && !fireball.IsReadyToFire)
            yield return new WaitForFixedUpdate();

        if (fireball != null)
        {
            // Set parent as gameCtrl (Becasue fireball moves too when spawner moves)
            fireball.transform.parent = GameManager.Instance.gameCtrl.transform;

            Rigidbody fireballRb = fireball.GetComponent<Rigidbody>();
            fireballRb.AddForce(transform.forward * fireballSpeed, ForceMode.Impulse);

            AudioSource.PlayClipAtPoint(fireSound, fireball.transform.position, 3.0f);
        }

        StartCoroutine(AfterFire());
    }

    protected virtual IEnumerator AfterFire()
    {
        currMoveState = MoveState.AfterFire;

        yield return new WaitForSeconds(afterFireDuration);

        StartCoroutine(MoveToPlayer());
    }

    protected virtual IEnumerator MoveToPlayer()
    {
        currMoveState = MoveState.Move;

        yield return new WaitForSeconds(moveDuration);

        currMoveState = MoveState.StartFire;
    }
}