using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class Creature : MonoBehaviour
{
    [Header("Creature")]
    [SerializeField] protected GameObject abnFrozenEffect;
    [SerializeField] protected AudioClip frozenSound;
    [SerializeField] protected GameObject abnBurnEffect;
    [SerializeField] protected AudioClip burnSound;
    [SerializeField] protected ParticleSystem abnPoisonEffect;
    [SerializeField] protected AudioClip poisonSound;

    [Header("Creature UI")]
    [SerializeField] protected GameObject healthBarUI;
    [SerializeField] protected Image healthBar;
    [SerializeField] protected GameObject damageUIPref;
    protected float health;

    protected Rigidbody rb;
    protected Animator anim;
    protected AudioSource creatureAudio;

    protected AbnormalState abnState;
    Coroutine underPoisonCoroutine;
    Coroutine underAttackCoroutine;
    protected Quaternion? beforeAttackRot = null;

    protected bool isDead;
    protected bool isUnderAttack;
    protected Vector3 bornPosition;
    public bool isGrounded { get; protected set; }

    protected virtual void Awake()
    {
        anim = transform.GetComponentInChildren<Animator>();
        rb = transform.GetComponentInChildren<Rigidbody>();
        creatureAudio = transform.GetComponentInChildren<AudioSource>();
        bornPosition = transform.position;
    }

    protected virtual void Start()
    {
        if (healthBarUI != null)
            healthBarUI.gameObject.SetActive(GameManager.Instance.gdPlayerData.BattleMode);
    }

    public void SetAbnState(AbnormalState newAbnState, float duration, float damage = 0)
    {
        if (isDead || abnState == AbnormalState.Burn || abnState == AbnormalState.Frozen)
            return;

        StartCoroutine(ChangeAbnState(newAbnState, duration, damage));
    }

    protected virtual IEnumerator ChangeAbnState(AbnormalState newAbnState, float duration, float damage = 0)
    {
        switch (newAbnState)
        {
            case AbnormalState.None:
                abnState = AbnormalState.None;
                yield break;
            case AbnormalState.Burn:
                abnState = AbnormalState.Burn;
                abnBurnEffect.SetActive(true);
                AudioSource.PlayClipAtPoint(burnSound, transform.position);

                anim.enabled = false;

                float sumDamage = 0;    // Damage creature every 1 seconds
                float timer = 0;
                float timeOut = 1;
                while (duration > 0)
                {
                    transform.Rotate(transform.up * Random.Range(-15, 15));

                    if (timer > timeOut)
                    {
                        DamageCreature(sumDamage);
                        sumDamage = 0;
                        timer = 0;
                    }

                    sumDamage += damage;
                    timer += Time.deltaTime;

                    if (isDead)
                        break;

                    yield return new WaitForFixedUpdate();
                    duration -= Time.deltaTime;
                }
                DamageCreature(sumDamage);

                if (isUnderAttack == false)
                    anim.enabled = true;

                abnBurnEffect.SetActive(false);
                abnState = AbnormalState.None;

                break;

            case AbnormalState.Frozen:
                abnState = AbnormalState.Frozen;
                abnFrozenEffect.SetActive(true);
                AudioSource.PlayClipAtPoint(frozenSound, transform.position);

                anim.enabled = false;

                while (duration > 0)
                {
                    if (isDead)
                        break;

                    yield return new WaitForFixedUpdate();
                    duration -= Time.deltaTime;
                }

                if (isUnderAttack == false)
                    anim.enabled = true;

                abnFrozenEffect.SetActive(false);
                abnState = AbnormalState.None;

                break;
        }
    }

    public void SetPoisoned(float duration)
    {
        if (isDead)
            return;

        if (underPoisonCoroutine != null)
            StopCoroutine(underPoisonCoroutine);

        underPoisonCoroutine = StartCoroutine(SetUnderPoision(duration));
    }

    protected virtual IEnumerator SetUnderPoision(float duration)
    {
        float underPoisonAngle = 30f;
        float underPoisonDur = 0.5f;
        float underPoisonIntv = 1;

        while (duration > 0)
        {
            SetAttacked(underPoisonAngle, underPoisonDur);
            if (abnPoisonEffect != null)
                Instantiate(abnPoisonEffect, transform.position, abnPoisonEffect.transform.rotation);
            if (poisonSound != null)
                AudioSource.PlayClipAtPoint(poisonSound, transform.position);

            if (isDead)
                break;

            yield return new WaitForSeconds(underPoisonIntv);
            duration -= underPoisonIntv;
        }
    }

    public void SetAttacked(float underAtkAngle, float underAtkDur, float damage = 0)
    {
        if (isDead)
            return;

        if (underAttackCoroutine != null)
            StopCoroutine(underAttackCoroutine);

        underAttackCoroutine = StartCoroutine(SetUnderAttack(underAtkAngle, underAtkDur, damage));
    }

    protected virtual IEnumerator SetUnderAttack(float underAtkAngle, float underAtkDur, float damage = 0)
    {
        isUnderAttack = true;

        if (beforeAttackRot == null)
            beforeAttackRot = transform.rotation;

        anim.enabled = false;

        DamageCreature(damage);

        yield return new WaitForSeconds(underAtkDur);

        if (abnState == AbnormalState.None)
            anim.enabled = true;

        transform.rotation = (Quaternion)beforeAttackRot;
        beforeAttackRot = null;

        isUnderAttack = false;
    }

    protected virtual void DamageCreature(float damage)
    {
        // Health bar
        if (GameManager.Instance.gdPlayerData.BattleMode && healthBar != null)
        {
            Vector2 healthLeft = healthBar.rectTransform.anchorMax;
            healthLeft.x -= healthLeft.x * damage / health;
            healthLeft.x = healthLeft.x > 1 ? 1 : healthLeft.x;
            healthBar.rectTransform.anchorMax = healthLeft;
        }

        /*if (damageUIPref != null && damage != 0)
        {
            GameObject obj = Instantiate(damageUIPref, transform.position, Quaternion.identity);
            obj.GetComponent<DamageUI>().SetText(damage);
            obj.SetActive(true);
        }*/

        // Health
        health -= damage;

        // Dead
        if (health <= 0 && !isDead)
            Dead();
    }

    protected virtual void Dead()
    {
        isDead = true;
    }
}