using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStateCtrl : Creature
{
    PlayerCtrl playerCtrl;
    StoreGameCtrl gmCtrl;

    protected override void Awake()
    {
        base.Awake();
        playerCtrl = GetComponent<PlayerCtrl>();
        gmCtrl = GameManager.Instance.gameCtrl;

        health = float.MaxValue;
    }

    void FixedUpdate()
    {
        playerCtrl.enabled = abnState != AbnormalState.Frozen && abnState != AbnormalState.Burn && !isUnderAttack && gmCtrl.gameProgress != GameProgress.Clear;
        if (gmCtrl.gameProgress == GameProgress.Clear)
        {
            transform.forward = Vector3.back;
            anim.SetFloat("Speed_f", 0);
            anim.SetBool("Dance", true);
        }
    }
}