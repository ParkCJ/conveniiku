using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySky : Enemy
{
    const float flyAroundMaxDistance = 30;

    protected enum MoveType
    {
        None,
        TakeOff,
        GoUp,
        AroundThePlayer,
        AttackPlayer
    }

    protected enum FlyAroundType
    {
        Foward,
        Right,
        Left
    }

    [Header("Enemy")]
    [SerializeField] protected AudioClip attackSound;
    [SerializeField] protected AudioClip goUpSound;
    [SerializeField] protected AudioClip startAttackSound;

    protected float yMinHeight = 0;
    protected float yMaxHeightMin = 0;
    protected float yMaxHeightMax = 0;
    protected float takeOffSpeed = 0;
    protected float goUpSpeed = 0;
    protected float goUpY = 0;
    protected FlyAroundType flyAroundType;
    protected float flyAroundSpeed = 0;
    protected float flyAroundTime = 0;
    protected float attackSpeed = 0;
    protected float attackForce = 0;
    protected float normalForce = 0;
    protected float underAtkAngle = 0;
    protected float underAtkDur = 0;

    protected MoveType currMoveType = MoveType.None;
    protected float yMaxHeight = 0;

    protected override void Awake()
    {
        base.Awake();
    }

    protected override void WhileWakeUp()
    {
        LookAtObject(GameManager.Instance.gameCtrl.player, yAdjust: 0);
    }

    protected override void BeforeMove()
    {
        yMaxHeight = Random.Range(yMaxHeightMin, yMaxHeightMax);
        currMoveType = MoveType.TakeOff;
    }

    protected override void Move()
    {
        switch (currMoveType)
        {
            case MoveType.TakeOff:
                GoUp(takeOffSpeed);
                break;
            case MoveType.GoUp:
                GoUp(goUpSpeed);
                break;
            case MoveType.AroundThePlayer:
                FlyAround();
                break;
            case MoveType.AttackPlayer:
                Attack();
                break;
        }
    }

    protected virtual void GoUp(float upSpeed)
    {
        if (abnState == AbnormalState.Frozen || abnState == AbnormalState.Burn || isUnderAttack)
            return;

        if (transform.position.y > yMaxHeight)
        {
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;

            float distance = flyAroundMaxDistance + 1;
            if (GameManager.Instance.gameCtrl.player != null)
                distance = Vector3.Distance(transform.position, GameManager.Instance.gameCtrl.player.transform.position);

            if (distance > flyAroundMaxDistance)
                flyAroundType = FlyAroundType.Foward;
            else
            {
                switch (Random.Range(0, 3))
                {
                    case 0:
                        flyAroundType = FlyAroundType.Foward;
                        break;
                    case 1:
                        flyAroundType = FlyAroundType.Right;
                        break;
                    case 2:
                        flyAroundType = FlyAroundType.Left;
                        break;
                }
            }

            currMoveType = MoveType.AroundThePlayer;

            if (GameManager.Instance.gameCtrl.player != null)
                StartCoroutine(StartAttackToPlayer());
        }
        else
        {
            LookAtObject(GameManager.Instance.gameCtrl.player, yAdjust: goUpY);
            rb.AddForce(transform.forward * upSpeed);

            if (!creatureAudio.isPlaying)
                creatureAudio.PlayOneShot(goUpSound, 0.6f);
        }
    }

    protected virtual void FlyAround()
    {
        if (abnState == AbnormalState.Frozen || abnState == AbnormalState.Burn || isUnderAttack)
            return;

        LookAtObject(GameManager.Instance.gameCtrl.player, yAdjust: 0);
        switch (flyAroundType)
        {
            case FlyAroundType.Foward:
                rb.AddForce(transform.forward * flyAroundSpeed);
                break;
            case FlyAroundType.Right:
                rb.AddForce(transform.forward * flyAroundSpeed);
                rb.AddForce(transform.right * flyAroundSpeed / 2);
                break;
            case FlyAroundType.Left:
                rb.AddForce(transform.forward * flyAroundSpeed);
                rb.AddForce(-transform.right * flyAroundSpeed / 2);
                break;
        }
    }

    protected virtual void Attack()
    {
        if (abnState == AbnormalState.Frozen || abnState == AbnormalState.Burn || isUnderAttack)
            return;

        if (transform.position.y < yMinHeight || GameManager.Instance.gameCtrl.player == null)
        {
            yMaxHeight = Random.Range(yMaxHeightMin, yMaxHeightMax);
            currMoveType = MoveType.GoUp;
        }
        else
        {
            LookAtObject(GameManager.Instance.gameCtrl.player);
            rb.AddForce(transform.forward * attackSpeed);
        }
    }

    protected virtual IEnumerator StartAttackToPlayer()
    {
        yield return new WaitForSeconds(flyAroundTime);

        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;

        currMoveType = MoveType.AttackPlayer;

        if (!creatureAudio.isPlaying)
            creatureAudio.PlayOneShot(startAttackSound, 0.6f);
    }

    protected virtual void OnCollisionEnter(Collision collision)
    {
        if (collision.rigidbody != null)
        {
            Vector3 dir = (collision.gameObject.transform.position - transform.position).normalized;
            dir = new Vector3(dir.x, 0, dir.z);

            if (collision.gameObject == GameManager.Instance.gameCtrl.player && currState == State.Move && currMoveType == MoveType.AttackPlayer && abnState == AbnormalState.None && !isUnderAttack)
            {
                collision.rigidbody.AddForce(dir * attackForce, ForceMode.Impulse);

                Creature creature = collision.transform.GetComponent<Creature>();
                creature.SetAttacked(underAtkAngle, underAtkDur);

                creatureAudio.PlayOneShot(attackSound, 2.0f);
            }
            else
                collision.rigidbody.AddForce(dir * normalForce, ForceMode.Impulse);

            yMaxHeight = Random.Range(yMaxHeightMin, yMaxHeightMax);
            currMoveType = MoveType.GoUp;
        }
    }

    protected override void WhileRest() { }
}