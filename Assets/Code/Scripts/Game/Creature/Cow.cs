using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cow : EnemyGround
{
    protected override void Awake()
    {
        base.Awake();

        health = 125;

        wakeUpDuration = 2.5f;
        restDuration = 1.25f;

        speed = 1;
        speedNg = 0.1f;
        attackForce = 200;// 196;// 88;
        normalForce = 20;
        underAtkAngle = 45f;
        underAtkDur = 0.5f;
    }
}