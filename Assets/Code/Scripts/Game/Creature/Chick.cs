using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chick : EnemyGroundFire
{
    protected override void Awake()
    {
        base.Awake();

        health = 75;

        wakeUpDuration = 2.5f;
        restDuration = 0;

        speed = 0.5f;
        speedNg = 0.05f;
        attackForce = 30;// 20;// 10;
        normalForce = 10;
        underAtkAngle = 15f;
        underAtkDur = 0.1f;

        moveDuration = 5f;
        afterFireDuration = 2.5f;

        fireballPosYOffset = -0.05f;
        fireballPosZOffset = 0.65f;
        fireballSpeed = 200;
    }
}