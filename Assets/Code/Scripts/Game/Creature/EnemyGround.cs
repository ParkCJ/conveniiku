using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGround : Enemy
{
    [Header("Enemy")]
    [SerializeField] protected AudioClip attackSound;
    [SerializeField] protected AudioClip moveSound;

    protected float speed;
    protected float speedNg;
    protected float attackForce = 0;
    protected float normalForce = 0;
    protected float underAtkAngle = 0;
    protected float underAtkDur = 0;

    protected override void WhileWakeUp()
    {
        LookAtObject(GameManager.Instance.gameCtrl.player, yAdjust: 0);

        anim.SetFloat("Speed_f", 0);
        anim.SetBool("Eat_b", true);
    }

    protected override void BeforeMove() { }

    protected override void Move()
    {
        if (rb.useGravity)
        {
            if (isGrounded)
                MoveOnTheGround();
            else
                MoveInTheAir();
        }
        else
            MoveNoGravity();
    }

    protected virtual void MoveOnTheGround()
    {
        if (abnState == AbnormalState.Frozen || abnState == AbnormalState.Burn || isUnderAttack)
            return;

        if (GameManager.Instance.gameCtrl.player == null)
        {
            anim.SetFloat("Speed_f", 0);
            anim.SetBool("Eat_b", true);
        }
        else
        {
            LookAtObject(GameManager.Instance.gameCtrl.player, yAdjust: 0);
            transform.Translate(Vector3.forward * Time.deltaTime * speed, Space.Self);

            anim.SetFloat("Speed_f", speed);
            anim.SetBool("Eat_b", false);

            if (!creatureAudio.isPlaying)
                creatureAudio.PlayOneShot(moveSound, 0.1f);
        }
    }

    protected virtual void MoveInTheAir()
    {
        if (abnState == AbnormalState.Frozen || abnState == AbnormalState.Burn || isUnderAttack)
            return;

        anim.SetFloat("Speed_f", 0);
        anim.SetBool("Eat_b", false);
    }

    protected virtual void MoveNoGravity()
    {
        if (abnState == AbnormalState.Frozen || abnState == AbnormalState.Burn || isUnderAttack)
            return;

        anim.SetFloat("Speed_f", 0);
        anim.SetBool("Eat_b", false);
    }

    protected override void WhileRest()
    {
        anim.SetFloat("Speed_f", 0);
        anim.SetBool("Eat_b", true);
    }

    void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.layer == (int)Layer.Ground || collision.gameObject.layer == (int)Layer.ActiveGround)
            isGrounded = true;

        float dot = Vector3.Dot(collision.GetContact(0).normal, Vector3.up);
        if (dot > 0.5f && (collision.gameObject.layer == (int)Layer.Player || collision.gameObject.layer == (int)Layer.Enemy || collision.gameObject.layer == (int)Layer.Dragon))
        {
            // If collision is from bottom, destroy gameObject
            GetComponent<Destroyable>().PassAway();
        }
        else if (dot < -0.5f)
        {
            // When something fall from the sky
            SetAttacked(0, 1f);
        }
        else if (dot > -0.15f && dot < 0.15f && (collision.gameObject.layer == (int)Layer.Player || collision.gameObject.layer == (int)Layer.Enemy) || collision.gameObject.layer == (int)Layer.Dragon)
        {
            if (currState == State.Move && abnState == AbnormalState.None && !isUnderAttack)
            {
                Vector3 dir = (collision.gameObject.transform.position - transform.position).normalized;
                dir.y = 0;

                if (collision.gameObject.layer == (int)Layer.Player)
                {
                    collision.rigidbody.AddForce(dir * attackForce, ForceMode.Impulse);

                    Creature creature = collision.transform.GetComponent<Creature>();
                    creature.SetAttacked(underAtkAngle, underAtkDur);

                    creatureAudio.PlayOneShot(attackSound, 3f);

                    StartCoroutine(Rest());
                }
                else if (collision.gameObject.layer == (int)Layer.Enemy || collision.gameObject.layer == (int)Layer.Dragon)
                {
                    collision.rigidbody.AddForce(dir * normalForce, ForceMode.Impulse);
                }
            }
        }
    }

    void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.layer == (int)Layer.Ground || collision.gameObject.layer == (int)Layer.ActiveGround)
            isGrounded = false;
    }
}