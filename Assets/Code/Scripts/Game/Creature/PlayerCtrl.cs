using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCtrl : MonoBehaviour
{
    [Header("Sound")]
    [SerializeField] AudioClip jumpSound;
    [SerializeField] AudioClip pushSound;
    [SerializeField] AudioClip pushHitSound;
    [SerializeField] AudioClip skillASound;
    [SerializeField] AudioClip skillBSound;
    [SerializeField] AudioClip skillCSound;
    [SerializeField] AudioClip skillDSound;
    [SerializeField] AudioClip skillBombSound;

    [Header("Skill")]
    [SerializeField] GameObject skillAPref;
    [SerializeField] GameObject skillBPref;
    [SerializeField] GameObject skillCPref;
    [SerializeField] GameObject skillDPref;
    [SerializeField] GameObject skillBombPrefab;

    Rigidbody rb;
    Animator anim;
    CapsuleCollider cl;

    // 1. IsGrounded
    public Transform groundCheck;
    public LayerMask groundMask;
    const float groundCheckRadius = 0.125f;
    public bool isGrounded;

    // 2. Rotate
    float rotatePitch;
    public GameObject camFollowTarget;

    // 2. Move
    float moveMagnitude = 0f;
    Vector3 moveDir;

    // 3. Jump
    float jumpTimeout; // 다음 점프까지 남은 시간

    // 4. Push
    public Transform pushCheck;
    public LayerMask pushMask;
    float pushTimeout;

    // 5. Attack
    float attackTimeout;
    float bombTimeout;

    GameManager gm;
    StoreGameCtrl gmCtrl;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        anim = GetComponentInChildren<Animator>();
        cl = GetComponent<CapsuleCollider>();

        gm = GameManager.Instance;
        gmCtrl = gm.gameCtrl;
    }

    void FixedUpdate()
    {
        GravityCheck();
        GroundedCheck();
        Rotate();
        Move();
        Jump();
        //Push();
        Attack();
        Bomb();
    }

    void GravityCheck()
    {
        anim.SetBool("IsZeroGravity", !rb.useGravity);
    }

    void GroundedCheck()
    {
        // GroundMask에 여러개의 Layer를 설정할 수 있다. (추후 동물등 추가)
        isGrounded = Physics.CheckSphere(groundCheck.position,  groundCheckRadius * transform.localScale.y, groundMask);
    }

    void Rotate()
    {
        if (gm.gdPlayerData.camType == CamType.FirstPerson)
        {
            float mouseX = Input.GetAxis("Mouse X") * 100 * Time.deltaTime;
            float mouseY = Input.GetAxis("Mouse Y") * 100 * Time.deltaTime;

            rotatePitch -= mouseY;
            if (gm.gdPlayerData.camType == CamType.FirstPerson)
                rotatePitch = Mathf.Clamp(rotatePitch, -90, 90); // Pitch의 경우, 각도에 제한을 둔다.
            else
                rotatePitch = Mathf.Clamp(rotatePitch, -70, 70);

            transform.Rotate(Vector3.up * mouseX); // 수평 회전
            camFollowTarget.transform.localRotation = Quaternion.Euler(rotatePitch, 0, 0); // 수직 회전
        }
        else
        {
            camFollowTarget.transform.localRotation = Quaternion.identity;
        }
    }

    void Move()
    {
        Vector3 input = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        moveMagnitude = Vector3.ClampMagnitude(input, 1.0f).magnitude;

        if (input.normalized != Vector3.zero)
        {
            float targetAngle;

            if (gm.gdPlayerData.camType == CamType.FirstPerson)
            {
                moveDir = ((transform.right * input.x) + (transform.forward * input.z)).normalized;
            }
            else if (gm.gdPlayerData.camType == CamType.ThirdPerson)
            {
                targetAngle = Mathf.Atan2(input.x, Mathf.Abs(input.z)) * Mathf.Rad2Deg + Camera.main.transform.eulerAngles.y;
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0f, targetAngle, 0f), 1.5f * Time.deltaTime);

                moveDir = (Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward).normalized;
                moveMagnitude = input.z < 0 ? -moveMagnitude : moveMagnitude;
            }
            else
            {
                targetAngle = Mathf.Atan2(input.x, input.z) * Mathf.Rad2Deg + Camera.main.transform.eulerAngles.y;
                transform.rotation = Quaternion.Euler(0f, targetAngle, 0f);

                moveDir = (Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward).normalized;
            }

            float speed = rb.useGravity ? gm.gdPlayerData.SpeedGr : gm.gdPlayerData.SpeedAir;
            
            // 공중에서 방향전환을 할 수 있게 하기 위해, IsGrounded 체크를 여기에 넣음. 공중 방향전환 없는 보통 버전은 vNew1.96 을 확인할 것
            if (!gm.gdPlayerData.MoveAir)
                speed = isGrounded ? speed : 0;

            transform.Translate(moveDir * moveMagnitude * speed * Time.deltaTime, Space.World);
        }

        // 애니메이션
        anim.SetFloat("Speed_f", isGrounded && rb.useGravity ? Mathf.Abs(moveMagnitude) : 0);
    }

    void Jump()
    {
        if (isGrounded && rb.useGravity)
        {
            // 점프를 눌렀고, 점프가능하면 => 점프
            if (Input.GetKey(KeyCode.Space) && jumpTimeout <= 0.0f)
            {
                // 위로 뛰므로, verticalVelocity가 양수가 된다.
                rb.AddForce(transform.up * gm.gdPlayerData.JumpForce, ForceMode.Impulse);
                rb.AddForce(moveDir * moveMagnitude * gm.gdPlayerData.SpeedGr * rb.mass, ForceMode.Impulse);

                // 점프타임아웃을 리셋한다.
                jumpTimeout = gm.gdPlayerData.JumpInterval;

                // 애니메이션
                anim.SetTrigger("Jump");

                // Sound
                AudioSource.PlayClipAtPoint(jumpSound, transform.position);
            }
        }

        // 점프타임아웃을 시간만큼 줄인다.
        if (jumpTimeout >= 0.0f)
        {
            jumpTimeout -= Time.deltaTime;
        }
    }

    void Push()
    {
        if (isGrounded && rb.useGravity)
        {
            if (Input.GetKey(KeyCode.LeftControl) && pushTimeout <= 0.0f)
            {
                RaycastHit[] hits = Physics.BoxCastAll(pushCheck.position, pushCheck.lossyScale / 2.0f, transform.forward, pushCheck.transform.rotation, pushCheck.lossyScale.z / 2.0f, pushMask);
                foreach(RaycastHit hit in hits)
                {
                    if (hit.rigidbody != null)
                    {
                        float force = gm.gdPlayerData.PushForce * hit.rigidbody.mass;
                        force = force + (force * moveMagnitude);// + running power
                        float upForce = force * 2f;

                        hit.rigidbody.AddForce(transform.forward * force, ForceMode.Impulse);
                        hit.rigidbody.AddForce(transform.up * upForce, ForceMode.Impulse);

                        AudioSource.PlayClipAtPoint(pushHitSound, hit.point);
                    }
                }
                
                // Modeling
                anim.SetTrigger("Attack");

                // Play push sound
                AudioSource.PlayClipAtPoint(pushSound, transform.position, 0.3f);

                // 푸쉬 타임아웃을 리셋한다.
                pushTimeout = gm.gdPlayerData.PushInterval;
            }

            // 푸쉬타임아웃을 시간만큼 줄인다.
            if (pushTimeout >= 0.0f)
            {
                pushTimeout -= Time.deltaTime;
            }
        }
    }

    void Attack()
    {
        //if (isGrounded && rb.useGravity)
        if (rb.useGravity)
        {
            if (attackTimeout <= 0.0f && gm.gdPlayerData.BattleMode)
            {
                if (Input.GetKey(KeyCode.LeftControl) ||
                    (gm.gdPlayerData.camType == CamType.FirstPerson && Input.GetMouseButtonDown(0)))
                {
                    Vector3 pos;
                    Quaternion rot;
                    Vector3 dir;
                    Transform skillDParent;

                    if (gm.gdPlayerData.camType == CamType.FirstPerson)
                    {
                        pos = gmCtrl.firstPersonCam.transform.position + (Camera.main.transform.forward * 0.5f * transform.localScale.y);
                        rot = Camera.main.transform.rotation;
                        dir = Camera.main.transform.forward;
                        skillDParent = Camera.main.transform;
                    }
                    else
                    {
                        pos = transform.position + (transform.forward * 0.5f * transform.localScale.y);
                        rot = transform.rotation;
                        dir = transform.forward;
                        skillDParent = gmCtrl.player.transform;
                    }

                    switch (gm.gdPlayerData.CurrSkillType)
                    {
                        case SkillType.SkillA:
                            SkillA skillA = Instantiate(skillAPref, pos, rot).GetComponent<SkillA>();
                            skillA.transform.localScale *= transform.localScale.y;
                            skillA.dir = dir;
                            skillA.powerUp = transform.localScale.y;

                            // Modeling
                            anim.SetTrigger("Attack");

                            // Play push sound
                            AudioSource.PlayClipAtPoint(skillASound, transform.position, 0.3f);

                            // 푸쉬 타임아웃을 리셋한다.
                            attackTimeout = gm.gdPlayerData.AttackInterval;
                            break;
                        case SkillType.SkillB:
                            SkillB skillB = Instantiate(skillBPref, pos, rot).GetComponent<SkillB>();
                            skillB.transform.localScale *= transform.localScale.y;
                            skillB.dir = dir;
                            skillB.powerUp = transform.localScale.y;

                            // Modeling
                            anim.SetTrigger("Attack");

                            // Play push sound
                            AudioSource.PlayClipAtPoint(skillBSound, transform.position, 0.3f);

                            // 푸쉬 타임아웃을 리셋한다.
                            attackTimeout = gm.gdPlayerData.AttackInterval;
                            break;
                        case SkillType.SkillC:
                            SkillC skillC = Instantiate(skillCPref, pos, rot).GetComponent<SkillC>();
                            skillC.transform.localScale *= transform.localScale.y;
                            skillC.dir = dir;
                            skillC.powerUp = transform.localScale.y;

                            // Modeling
                            anim.SetTrigger("Attack");

                            // Play push sound
                            AudioSource.PlayClipAtPoint(skillCSound, transform.position, 0.3f);

                            // 푸쉬 타임아웃을 리셋한다.
                            attackTimeout = gm.gdPlayerData.AttackInterval;
                            break;
                        case SkillType.SkillD:
                            GameObject skillD = Instantiate(skillDPref, pos, Quaternion.identity, skillDParent);
                            skillD.transform.rotation = skillDParent.transform.rotation * Quaternion.Euler(0, -90, 0);

                            // Play push sound
                            AudioSource.PlayClipAtPoint(skillDSound, transform.position, 0.3f);

                            // 푸쉬 타임아웃을 리셋한다.
                            attackTimeout = gm.gdPlayerData.AttackInterval;
                            break;
                    }
                }
            }

            // 푸쉬타임아웃을 시간만큼 줄인다.
            if (attackTimeout >= 0.0f)
            {
                attackTimeout -= Time.deltaTime;
            }
        }
    }

    void Bomb()
    {
        //if (isGrounded && rb.useGravity)
        if (rb.useGravity)
        {
            if (bombTimeout <= 0.0f && gm.gdPlayerData.BattleMode && gm.gdPlayerData.Bomb > 0)
            {
                if (Input.GetKey(KeyCode.LeftShift) ||
                    (gm.gdPlayerData.camType == CamType.FirstPerson && Input.GetMouseButtonDown(1)))
                {
                    gm.gameCtrl.AddBomb(-1);

                    Instantiate(skillBombPrefab, transform.position, Quaternion.identity);

                    // Play push sound
                    AudioSource.PlayClipAtPoint(skillBombSound, transform.position, 0.3f);

                    bombTimeout = gm.gdPlayerData.BombInterval;
                }
            }

            // 푸쉬타임아웃을 시간만큼 줄인다.
            if (bombTimeout >= 0.0f)
            {
                bombTimeout -= Time.deltaTime;
            }
        }
    }
}
