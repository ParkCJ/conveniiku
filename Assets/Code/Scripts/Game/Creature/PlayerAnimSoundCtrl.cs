using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimSoundCtrl : MonoBehaviour
{
    const float footStepVolume = 1f;
    public AudioClip[] footstepClips;

    [SerializeField] PlayerCtrl playerCtrl;

    public void PlayStep()
    {
        if (playerCtrl != null && playerCtrl.isGrounded && footstepClips.Length != 0)
        {
            AudioClip clip = footstepClips[Random.Range(0, footstepClips.Length)];
            AudioSource.PlayClipAtPoint(clip, transform.position, footStepVolume);
        }
    }

    public void Mark()
    {
        // Do nothing
    }
}
