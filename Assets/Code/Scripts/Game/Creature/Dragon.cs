using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dragon : EnemySky
{
    [SerializeField] ParticleSystem flameThrower;
    float attackTime = 5;
    Vector3 attackDir;

    protected override void Awake()
    {
        base.Awake();

        health = 1000;

        wakeUpDuration = 1f;

        attackForce = 500;// 250;
        normalForce = 100;
        underAtkAngle = 60f;
        underAtkDur = 0.75f;

        yMinHeight = 1.5f;
        yMaxHeightMin = 6f;
        yMaxHeightMax = 6.5f;
        takeOffSpeed = 1000f;
        goUpSpeed = 1000f;
        goUpY = 0.85f;
        flyAroundSpeed = 650;
        flyAroundTime = 5;
        attackSpeed = 1300;
    }

    protected override IEnumerator StartAttackToPlayer()
    {
        yield return new WaitForSeconds(flyAroundTime);

        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;

        currMoveType = MoveType.AttackPlayer;

        LookAtObject(GameManager.Instance.gameCtrl.player, yAdjust: -0.5f);
        attackDir = new Vector3(transform.forward.x, 0, transform.forward.z);

        if (!creatureAudio.isPlaying)
            creatureAudio.PlayOneShot(startAttackSound, 0.6f);

        StartCoroutine(StartGoUp());
    }

    protected override void Attack()
    {
        if (abnState == AbnormalState.Frozen || abnState == AbnormalState.Burn || isUnderAttack)
            return;

        if (transform.position.y < yMinHeight || GameManager.Instance.gameCtrl.player == null)
        {
            yMaxHeight = Random.Range(yMaxHeightMin, yMaxHeightMax);
            currMoveType = MoveType.GoUp;
        }
        else
        {
            if (transform.position.y > yMaxHeightMax)
            {
                // Too high -> go closer
                LookAtObject(GameManager.Instance.gameCtrl.player);
                rb.AddForce(transform.forward * attackSpeed);
            }
            else if (transform.position.y < yMaxHeightMin)
            {
                // Too low -> go back
                rb.AddForce(-transform.forward * attackSpeed);
            }
            else
            {
                // Normal -> go forward
                rb.AddForce(attackDir * attackSpeed);
            }
        }

        if (!flameThrower.gameObject.activeSelf)
            creatureAudio.PlayOneShot(attackSound, 0.6f);

        flameThrower.gameObject.SetActive(true);
    }

    protected virtual IEnumerator StartGoUp()
    {
        yield return new WaitForSeconds(attackTime);

        yMaxHeight = Random.Range(yMaxHeightMin, yMaxHeightMax);
        currMoveType = MoveType.GoUp;

        flameThrower.gameObject.SetActive(false);
    }

    protected override void OnCollisionEnter(Collision collision)
    {
        // do nothing
    }
}