using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kite : EnemySky
{
    protected override void Awake()
    {
        base.Awake();

        health = 100;

        wakeUpDuration = 1f;

        attackForce = 375;// 250;
        normalForce = 100;
        underAtkAngle = 60f;
        underAtkDur = 0.75f;

        yMinHeight = 0;
        yMaxHeightMin = 5f;
        yMaxHeightMax = 5.5f;
        takeOffSpeed = 25;
        goUpSpeed = 50;
        goUpY = 0.85f;
        flyAroundSpeed = 35;
        flyAroundTime = 5;
        attackSpeed = 350;
    }
}