using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class DragonFlame : MonoBehaviour
{
    const string fireOnGroundNm = "Fire03_Onetime(Clone)";
    const float explosionMinRate = 1.05f;
    const float explosionMaxRate = 1.15f;
    const float camShakeRate = 1.5f;

    const float abnDuration = 4.5f;
    const float underAtkAngle = 45f;
    const float underAtkDur = 0.5f;

    [SerializeField] ParticleSystem fireOnGround;
    [SerializeField] ParticleSystem sparkOnGround;
    [SerializeField] protected AudioClip burnSound;
    [SerializeField] protected AudioClip explosionSound;
    CinemachineImpulseSource impSrc;

    GameManager gm;
    Dictionary<GameObject, Vector3> grounds;

    void Awake()
    {
        gm = GameManager.Instance;
        impSrc = GetComponent<CinemachineImpulseSource>();
        grounds = new Dictionary<GameObject, Vector3>();
    }

    void OnParticleCollision(GameObject other)
    {
        if (other.name == "Dragon(Clone)")
            return;

        if (other.gameObject.layer == (int)Layer.Player)
        {
            Creature creature = other.transform.GetComponent<Creature>();
            if (creature != null)
            {
                creature.SetAbnState(AbnormalState.Burn, abnDuration, 0.2f);
                creature.SetAttacked(underAtkAngle, underAtkDur, 1);

                impSrc.GenerateImpulse(camShakeRate * gm.gdPlayerData.ActionCamShakeRate / 10);
            }

            impSrc.GenerateImpulse(camShakeRate * gm.gdPlayerData.ActionCamShakeRate / 10);
        }
        else if (other.gameObject.layer == (int)Layer.Enemy)
        {
            Creature creature = other.transform.GetComponent<Creature>();
            if (creature != null)
            {
                creature.SetAbnState(AbnormalState.Burn, abnDuration, 0.2f);
                creature.SetAttacked(underAtkAngle, underAtkDur, 1);

                impSrc.GenerateImpulse(camShakeRate * gm.gdPlayerData.ActionCamShakeRate / 10);
            }
        }
        else if (other.gameObject.layer == (int)Layer.Ground || other.gameObject.layer == (int)Layer.ActiveGround)
        {
            if (other.transform.Find(fireOnGroundNm) == null)
            {
                Instantiate(fireOnGround, other.transform);
                Instantiate(sparkOnGround, other.transform);
                AudioSource.PlayClipAtPoint(burnSound, other.transform.position, 2.0f);
                AudioSource.PlayClipAtPoint(explosionSound, other.transform.position, 1.0f);

                impSrc.GenerateImpulse(camShakeRate * gm.gdPlayerData.ActionCamShakeRate / 10);

                if (!grounds.ContainsKey(other))
                {
                    grounds.Add(other, other.transform.localScale);

                    // Explosion
                    other.transform.localScale *= Random.Range(explosionMinRate, explosionMaxRate);

                    StartCoroutine(AfterExplosion(other));
                }
            }
        }
        else
        {
            // do nothing
        }
    }

    IEnumerator AfterExplosion(GameObject other)
    {
        yield return new WaitForFixedUpdate();

        other.transform.localScale = grounds[other];
        grounds.Remove(other);
    }
}