using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerIntroCtrl : MonoBehaviour
{
    Animator anim;

    void Awake()
    {
        anim = GetComponentInChildren<Animator>();
    }

    void Start()
    {
        anim.SetBool("WaveHands", true);
    }

    void Update()
    {
        if (!anim.GetBool("SitDown"))
        {
            Vector3 lookAt = (Camera.main.transform.position - transform.position).normalized;
            lookAt.y = Mathf.Clamp(lookAt.y, 0, 0.3f);
            transform.forward = lookAt;
        }

        if (Input.GetKey(KeyCode.UpArrow))
        {
            anim.SetBool("WaveHands", true);
            anim.SetBool("WaveOneHand", false);
            anim.SetBool("HandOnHips", false);
            anim.SetBool("SitDown", false);
        }
        else if (Input.GetKey(KeyCode.LeftArrow))
        {
            anim.SetBool("WaveHands", false);
            anim.SetBool("WaveOneHand", true);
            anim.SetBool("HandOnHips", false);
            anim.SetBool("SitDown", false);
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            anim.SetBool("WaveHands", false);
            anim.SetBool("WaveOneHand", false);
            anim.SetBool("HandOnHips", true);
            anim.SetBool("SitDown", false);
        }
        else if (Input.GetKey(KeyCode.DownArrow))
        {
            anim.SetBool("WaveHands", false);
            anim.SetBool("WaveOneHand", false);
            anim.SetBool("HandOnHips", false);
            anim.SetBool("SitDown", true);
        }
    }
}
