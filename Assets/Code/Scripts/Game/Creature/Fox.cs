using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fox : EnemyGround
{
    protected override void Awake()
    {
        base.Awake();

        health = 100;

        wakeUpDuration = 1.5f;
        restDuration = 0.75f;

        speed = 2;
        speedNg = 0.2f;
        attackForce = 130;// 126;// 63;
        normalForce = 20;
        underAtkAngle = 30f;
        underAtkDur = 0.3f;
    }
}