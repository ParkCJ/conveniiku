using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class SkillD : MonoBehaviour
{
    const string fireOnGroundNm = "Fire03_Onetime(Clone)";
    const float lifeSpan = 1;
    const float explosionMinRate = 1.05f;
    const float explosionMaxRate = 1.15f;
    const float camShakeRate = 1.5f;

    const float abnDuration = 4.5f;
    const float underAtkAngle = 45f;
    const float underAtkDur = 0.5f;

    [SerializeField] ParticleSystem fireOnGround;
    [SerializeField] ParticleSystem sparkOnGround;
    [SerializeField] protected AudioClip burnSound;
    [SerializeField] protected AudioClip explosionSound;

    GameManager gm;
    Dictionary<GameObject, Vector3> grounds;

    void Awake()
    {
        gm = GameManager.Instance;
        grounds = new Dictionary<GameObject, Vector3>();
    }

    void OnParticleCollision(GameObject other)
    {
        if (other.gameObject.layer == (int)Layer.Player)
            return;
        else if (other.gameObject.layer == (int)Layer.Enemy || other.gameObject.layer == (int)Layer.Dragon)
        {
            Creature creature = other.transform.GetComponent<Creature>();
            if (creature != null)
            {
                creature.SetAbnState(AbnormalState.Burn, abnDuration, 0.2f);
                creature.SetAttacked(underAtkAngle, underAtkDur, 1);
            }
        }
        else if (other.gameObject.layer == (int)Layer.Ground || other.gameObject.layer == (int)Layer.ActiveGround)
        {
            if (other.transform.Find(fireOnGroundNm) == null)
            {
                Instantiate(fireOnGround, other.transform);
                Instantiate(sparkOnGround, other.transform);
                AudioSource.PlayClipAtPoint(burnSound, other.transform.position, 2.0f);
                AudioSource.PlayClipAtPoint(explosionSound, other.transform.position, 1.0f);

                if (!grounds.ContainsKey(other))
                {
                    grounds.Add(other, other.transform.localScale);

                    // Explosion
                    other.transform.localScale *= Random.Range(explosionMinRate, explosionMaxRate);

                    StartCoroutine(AfterExplosion(other));
                }
            }
        }
        else
        {
            // do nothing
        }
    }

    IEnumerator AfterExplosion(GameObject other)
    {
        yield return new WaitForFixedUpdate();

        other.transform.localScale = grounds[other];
        grounds.Remove(other);
    }

    void OnEnable()
    {
        StartCoroutine(DestorySelf());
    }

    IEnumerator DestorySelf()
    {
        yield return new WaitForSeconds(lifeSpan);
        Destroy(gameObject);
    }
}