using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillB : MonoBehaviour
{
    const float destroyShrinkRate = 0.5f;
    const float minForce = 1.0f;
    const float maxForce = 2.0f;
    const float underAtkAngle = 60;
    const float underAtkDur = 0.75f;
    const float groundExplForceFoward = 300;
    const float groundExplForceUp = 500;
    float moveSpeed = 1;
    float underAtckDamage = 5f;

    [SerializeField] ParticleSystem attackEffect;
    [SerializeField] ParticleSystem explosionEffect;
    [SerializeField] AudioClip attackSound;

    float lifespan = 5;
    public Vector3 dir;
    public float powerUp;

    void Start()
    {
        StartCoroutine(StartDetroy());

        moveSpeed = Mathf.Clamp(moveSpeed * powerUp, 0.5f, moveSpeed);
        underAtckDamage *= powerUp;
    }

    void FixedUpdate()
    {
        transform.Translate(dir * moveSpeed * Time.deltaTime, Space.World);
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.layer == (int)Layer.Player)
            return;

        if (other.gameObject.layer == (int)Layer.Enemy || other.gameObject.layer == (int)Layer.Dragon)
        {
            // Check if collision is from front
            Vector3 dir = (transform.position - other.transform.position).normalized;
            float dot = Vector3.Dot(dir, -transform.forward);

            if (dot > 0.5f)
            {
                Attack(other);
            }
        }
        else if (other.gameObject.layer == (int)Layer.Ground || other.gameObject.layer == (int)Layer.ActiveGround)
        {
            other.attachedRigidbody.AddForce(transform.forward * groundExplForceFoward, ForceMode.Impulse);
            other.attachedRigidbody.AddForce(transform.up * groundExplForceUp, ForceMode.Impulse);

            Instantiate(explosionEffect, other.ClosestPoint(transform.position), explosionEffect.transform.rotation);
            AudioSource.PlayClipAtPoint(attackSound, other.ClosestPoint(transform.position), 0.3f);

            Destroy(gameObject);
        }
    }

    void Attack(Collider other)
    {
        // Move backward moveSpeed * 2 to offset transition in FixedUpdate, then move back.
        transform.Translate(-transform.forward * moveSpeed * 2 * Time.deltaTime, Space.World);

        if (other.attachedRigidbody != null)
        {
            Instantiate(attackEffect, other.ClosestPoint(transform.position), attackEffect.transform.rotation);
            AudioSource.PlayClipAtPoint(attackSound, other.ClosestPoint(transform.position), 0.3f);

            other.attachedRigidbody.AddForce(transform.forward * Random.Range(minForce, maxForce) * other.attachedRigidbody.mass, ForceMode.Impulse);

            if (other.gameObject.layer == (int)Layer.Enemy || other.gameObject.layer == (int)Layer.Dragon)
            {
                Creature creature = other.GetComponent<Creature>();
                if (creature != null)
                    creature.SetAttacked(underAtkAngle, underAtkDur, underAtckDamage);
            }
        }
    }

    IEnumerator StartDetroy()
    {
        yield return new WaitForSeconds(lifespan);

        StartCoroutine(DestroySelf());
    }

    IEnumerator DestroySelf()
    {
        while (transform.localScale.x > 0.1f)
        {
            yield return new WaitForFixedUpdate();
            transform.localScale *= destroyShrinkRate;
        }

        Destroy(gameObject);
    }
}