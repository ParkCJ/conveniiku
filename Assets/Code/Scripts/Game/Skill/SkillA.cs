using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillA : MonoBehaviour
{
    const float shrinkRate = 0.75f;
    const int attackIntv = 500;     // millisecond
    const float underAtkAngle = 15;
    const float underAtkDur = 0.3f;
    const float groundExplForceFoward = 300;
    const float groundExplForceUp = 500;
    float moveSpeed = 2;      // moveSpeed
    float underAtkDamage = 15;

    class ColliderInfo
    {
        public DateTime LastAttackDt;
        public Vector3 LastPos;
    }

    [SerializeField] ParticleSystem attackEffect;
    [SerializeField] ParticleSystem destroyEffect;
    [SerializeField] AudioClip attackSound;
    [SerializeField] AudioClip destroySound;

    Dictionary<Collider, ColliderInfo> colliders;
    bool isAttacking = false;
    float lifespan = 5;

    public Vector3 dir;
    public float powerUp;

    void Awake()
    {
        colliders = new Dictionary<Collider, ColliderInfo>();
    }

    void Start()
    {
        StartCoroutine(StartDetroy());

        moveSpeed = Mathf.Clamp(moveSpeed * powerUp, 1, moveSpeed);
        underAtkDamage *= powerUp;
    }

    void Update()
    {
        if (!isAttacking)
            transform.Translate(dir * moveSpeed * Time.deltaTime, Space.World);
    }

    IEnumerator StartDetroy()
    {
        yield return new WaitForSeconds(lifespan);

        StartCoroutine(DestroySelf());
    }

    IEnumerator DestroySelf()
    {
        while (transform.localScale.x > 0.1f)
        {
            yield return new WaitForFixedUpdate();
            transform.localScale *= shrinkRate;
        }

        Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == (int)Layer.Player)
            return;

        if (other.gameObject.layer == (int)Layer.Enemy || other.gameObject.layer == (int)Layer.Dragon)
        {
            ColliderInfo info = new ColliderInfo();
            info.LastAttackDt = DateTime.Now;
            info.LastPos = other.attachedRigidbody.transform.position; // Must handle the gameObject which rigidbody component is attached to

            colliders.Add(other, info);

            // first attack
            Attack(other);
        }
        else if (other.gameObject.layer == (int)Layer.Ground || other.gameObject.layer == (int)Layer.ActiveGround)
        {
            if (other.attachedRigidbody != null)
            {
                other.attachedRigidbody.AddForce(transform.forward * groundExplForceFoward, ForceMode.Impulse);
                other.attachedRigidbody.AddForce(transform.up * groundExplForceUp, ForceMode.Impulse);

                Instantiate(destroyEffect, other.transform.position, destroyEffect.transform.rotation);
                AudioSource.PlayClipAtPoint(destroySound, other.ClosestPoint(transform.position));

                Destroy(gameObject);
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (colliders.ContainsKey(other))
        {
            TimeSpan span = DateTime.Now - colliders[other].LastAttackDt;

            if (span.Milliseconds > attackIntv)
                Attack(other);
        }
    }

    void Attack(Collider other)
    {
        Instantiate(attackEffect, other.transform.position, attackEffect.transform.rotation);

        colliders[other].LastAttackDt = DateTime.Now;
        colliders[other].LastPos = other.attachedRigidbody.transform.position;

        // Attack
        Creature creature = other.transform.GetComponent<Creature>();
        if (creature != null)
            creature.SetAttacked(underAtkAngle, underAtkDur, underAtkDamage);

        // Sound
        AudioSource.PlayClipAtPoint(attackSound, other.ClosestPoint(transform.position));

        StartCoroutine(StopMove(other));
    }

    IEnumerator StopMove(Collider other)
    {
        isAttacking = true;

        float duration = underAtkDur;
        while (duration > 0)
        {
            if (other != null && colliders.ContainsKey(other))
                other.attachedRigidbody.transform.position = colliders[other].LastPos;

            yield return new WaitForFixedUpdate();
            duration -= Time.deltaTime;
        }

        isAttacking = false;
    }

    void OnTriggerExit(Collider other)
    {
        if (colliders.ContainsKey(other))
        {
            colliders.Remove(other);
        }
    }
}