using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class SkillBomb : MonoBehaviour
{
    const float camShakeRate = 1.5f;

    public float speed = 10;
    public float agForce = 10000;
    public ParticleSystem explosionParticle;
    [SerializeField] AudioClip explosionSound;
    CinemachineImpulseSource impSrc;

    GameManager gm;

    // Start is called before the first frame update
    void Start()
    {
        gm = GameManager.Instance;
        impSrc = GetComponent<CinemachineImpulseSource>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (transform.localScale.y < 50)
            transform.localScale += Vector3.one * speed * Time.deltaTime;
        else
            Destroy(gameObject);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == (int)Layer.Player)
            return;

        if (other.gameObject.layer == (int)Layer.Enemy || other.gameObject.layer == (int)Layer.Dragon)
        {
            Creature creature = other.transform.GetComponent<Creature>();
            if (creature != null)
            {
                creature.SetAbnState(AbnormalState.Burn, 5f, 0.2f);
                creature.SetAttacked(45, 5f, 100f);
            }
            //other.GetComponent<Destroyable>().DestroyNow();

            Instantiate(explosionParticle, other.transform.position, Quaternion.identity);
            AudioSource.PlayClipAtPoint(explosionSound, other.transform.position);

            if (impSrc != null)
                impSrc.GenerateImpulse(camShakeRate * gm.gdPlayerData.ActionCamShakeRate / 10);
        }
        else if (other.gameObject.layer == (int)Layer.Obst)
        {
            Destroy(other.gameObject);

            Instantiate(explosionParticle, other.transform.position, Quaternion.identity);
            AudioSource.PlayClipAtPoint(explosionSound, other.transform.position);

            if (impSrc != null)
                impSrc.GenerateImpulse(camShakeRate * gm.gdPlayerData.ActionCamShakeRate / 10);
        }
        else if (other.gameObject.layer == (int)Layer.ActiveGround)
        {
            Rigidbody rb = other.attachedRigidbody;
            if (rb != null)
            {
                Vector3 dir = (other.transform.position - transform.position).normalized;
                rb.AddForce(dir * agForce, ForceMode.Impulse);

                if (impSrc != null)
                    impSrc.GenerateImpulse(camShakeRate * gm.gdPlayerData.ActionCamShakeRate / 10);
            }
        }
    }
}