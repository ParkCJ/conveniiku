using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillC : MonoBehaviour
{
    const float lifeSpan = 5;
    const float abnDuration = 2.5f;
    const float underAtkAngle = 45f;
    const float underAtkDur = 1.5f;
    float speed = 200;
    float explForceVFoward = 2;
    float explForceVUp = 4;
    float explForceIFoward = 300;
    float explForceIUp = 500;
    float abnDamage = 0.2f;
    float underAtkDamage = 20f;

    [SerializeField] ParticleSystem explosionParticle;
    [SerializeField] AudioClip explosionSound;
    [SerializeField] AudioClip fireSound;

    public Vector3 dir;
    public float powerUp;
    Rigidbody rb;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Start()
    {
        float xTorque = (Random.value > 0.5f ? Random.Range(1, rb.maxAngularVelocity) : -Random.Range(1, rb.maxAngularVelocity));
        float yTorque = (Random.value > 0.5f ? Random.Range(1, rb.maxAngularVelocity) : -Random.Range(1, rb.maxAngularVelocity));
        float zTorque = (Random.value > 0.5f ? Random.Range(1, rb.maxAngularVelocity) : -Random.Range(1, rb.maxAngularVelocity));

        rb.AddTorque(transform.right * xTorque, ForceMode.Impulse);
        rb.AddTorque(transform.up * yTorque, ForceMode.Impulse);
        rb.AddTorque(transform.forward * zTorque, ForceMode.Impulse);

        StartCoroutine(DestroySelf());

        speed = Mathf.Clamp(speed * powerUp, 100, speed);
        explForceVFoward *= powerUp;
        explForceVUp *= powerUp;
        explForceIFoward *= powerUp;
        explForceIUp *= powerUp;
        abnDamage *= powerUp;
        underAtkDamage *= powerUp;

        rb.AddForce(dir * speed, ForceMode.Impulse);

        AudioSource.PlayClipAtPoint(fireSound, transform.position);
    }

    IEnumerator DestroySelf()
    {
        yield return new WaitForSeconds(lifeSpan);

        Explode();
    }

    void Explode()
    {
        Instantiate(explosionParticle, transform.position, explosionParticle.transform.rotation);
        AudioSource.PlayClipAtPoint(explosionSound, transform.position, 3.0f);

        Destroy(gameObject);
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.rigidbody != null)
        {
            Vector3 forward = (collision.transform.position - transform.position).normalized;

            if (collision.gameObject.layer == (int)Layer.Player)
            {
                // Do nothing
            }
            else if (collision.gameObject.layer == (int)Layer.Enemy || collision.gameObject.layer == (int)Layer.Dragon)
            {
                collision.rigidbody.AddForce(forward * explForceVFoward, ForceMode.VelocityChange);
                collision.rigidbody.AddForce(Vector3.up * explForceVUp, ForceMode.VelocityChange);

                Creature creature = collision.transform.GetComponent<Creature>();
                if (creature != null)
                {
                    creature.SetAbnState(AbnormalState.Burn, abnDuration, abnDamage);
                    creature.SetAttacked(underAtkAngle, underAtkDur, underAtkDamage);
                }
            }
            else
            {
                collision.rigidbody.AddForce(forward * explForceIFoward, ForceMode.Impulse);
                collision.rigidbody.AddForce(Vector3.up * explForceIUp, ForceMode.Impulse);
            }
        }

        Explode();
    }
}