using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillBBlade : MonoBehaviour
{
    const float torque = 10000;
    public enum Direction
    {
        X,
        Y,
        Z
    }

    [SerializeField] Direction dir;

    void Update()
    {
        switch (dir)
        {
            case Direction.X:
                transform.Rotate(Vector3.right * torque * Time.deltaTime);
                break;
            case Direction.Y:
                transform.Rotate(Vector3.up * torque * Time.deltaTime);
                break;
            case Direction.Z:
                transform.Rotate(Vector3.forward * -torque * Time.deltaTime);
                break;
        }
    }
}
