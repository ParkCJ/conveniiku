using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NightDome : MonoBehaviour
{
    [SerializeField] float rotSpeed;

    void Update()
    {
        transform.Rotate(Vector3.up * Time.deltaTime * rotSpeed);
    }

    public void SetSpeed(float rotSpeed)
    {
        this.rotSpeed = rotSpeed;
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }
}