using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Soundscape : MonoBehaviour
{
    AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.playOnAwake = false;
        audioSource.volume = 0;
    }

    void Start()
    {
        // Start playing from random position
        audioSource.time = Random.Range(0f, audioSource.clip.length);
        audioSource.Play();

        StartCoroutine(VolumeUp());
    }

    IEnumerator VolumeUp()
    {
        float up = Random.Range(0.1f, 0.5f);

        while(true)
        {
            yield return new WaitForEndOfFrame();
            audioSource.volume += up;

            if (Mathf.Approximately(1.0f, audioSource.volume))
                break;
        }
    }
}
