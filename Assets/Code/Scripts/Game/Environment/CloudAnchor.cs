using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudAnchor : MonoBehaviour
{
    GameObject cloud;
    public GameObject[] cloudPrefabs;

    float minDistance = 30;
    float maxDistance = 200;
    public float windStrength;

    void Awake()
    {
        // cloud
        cloud = Instantiate(cloudPrefabs[Random.Range(0, cloudPrefabs.Length)], transform);

        // cloud position
        Vector3 pos = Random.insideUnitSphere.normalized * Random.Range(minDistance, maxDistance);
        cloud.transform.position = pos;

        // cloud shape
        int size = Random.Range(1, 5);
        cloud.transform.localScale *= size;

    }

    void Start()
    {
        windStrength = Random.Range(windStrength * 0.5f, windStrength * 1.5f);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        transform.Rotate(Vector3.up * -windStrength * Time.deltaTime);
    }
}
