using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class ThirdPersonCam : MonoBehaviour
{
    [SerializeField] LayerMask cullingMask;

    CinemachineVirtualCamera cam;

    void Awake()
    {
        cam = GetComponent<CinemachineVirtualCamera>();
        cam.m_Transitions.m_OnCameraLive.AddListener((vcamIn, vcamOut) =>
        {
            Camera.main.cullingMask = cullingMask;
        });
    }

    void LateUpdate()
    {
        if (!Mathf.Approximately(Input.GetAxis("Mouse ScrollWheel"), 0))
        {
            Zoom(Input.mouseScrollDelta.y);
        }
    }

    void Zoom(float zoom)
    {
        Cinemachine3rdPersonFollow bd = cam.GetCinemachineComponent<Cinemachine3rdPersonFollow>();
        float vMin = 0;
        float vMax = 2;
        float dMin = 1;
        float dMax = 3;
        float vAdjust = bd.VerticalArmLength - (zoom * 0.1f);
        float dAdjust = bd.CameraDistance - (zoom * 0.1f);

        bd.VerticalArmLength = Mathf.Clamp(vAdjust, vMin, vMax);
        bd.CameraDistance = Mathf.Clamp(dAdjust, dMin, dMax);
    }
}