using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class TopCam : MonoBehaviour
{
    [SerializeField] LayerMask cullingMask;

    CinemachineVirtualCamera cam;

    void Awake()
    {
        cam = GetComponent<CinemachineVirtualCamera>();
        cam.m_Transitions.m_OnCameraLive.AddListener((vcamIn, vcamOut) =>
        {
            Camera.main.cullingMask = cullingMask;
        });
    }

    private void LateUpdate()
    {
        if (!Mathf.Approximately(Input.GetAxis("Mouse ScrollWheel"), 0))
        {
            Zoom(Input.mouseScrollDelta.y);
        }
    }

    void Zoom(float zoom)
    {
        float min = 30;
        float max = 90;
        float adjust = cam.m_Lens.FieldOfView + (-zoom * 5);

        cam.m_Lens.FieldOfView = Mathf.Clamp(adjust, min, max);
    }
}
