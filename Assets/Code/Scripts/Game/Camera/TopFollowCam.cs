using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class TopFollowCam : MonoBehaviour
{
    [SerializeField] LayerMask cullingMask;

    CinemachineVirtualCamera cam;

    void Awake()
    {
        cam = GetComponent<CinemachineVirtualCamera>();
        cam.m_Transitions.m_OnCameraLive.AddListener((vcamIn, vcamOut) =>
        {
            Camera.main.cullingMask = cullingMask;
        });
    }

    private void LateUpdate()
    {
        if (!Mathf.Approximately(Input.GetAxis("Mouse ScrollWheel"), 0))
        {
            Zoom(Input.mouseScrollDelta.y);
        }
    }

    void Zoom(float zoom)
    {
        CinemachineFramingTransposer ft = cam.GetCinemachineComponent<CinemachineFramingTransposer>();
        float min = 3;
        float max = 6;
        float adjust = ft.m_CameraDistance + (-zoom * 0.5f);

        ft.m_CameraDistance = Mathf.Clamp(adjust, min, max);
    }
}
