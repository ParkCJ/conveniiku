using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class FirstPersonCam : MonoBehaviour
{
    [SerializeField] LayerMask cullingMask;

    CinemachineVirtualCamera cam;

    void Awake()
    {
        cam = GetComponent<CinemachineVirtualCamera>();
        cam.m_Transitions.m_OnCameraLive.AddListener((vcamIn, vcamOut) =>
        {
            Camera.main.cullingMask = cullingMask;
        });
    }

    private void LateUpdate()
    {
        if (!Mathf.Approximately(Input.GetAxis("Mouse ScrollWheel"), 0))
            Zoom(Input.mouseScrollDelta.y);
    }

    void Zoom(float zoom)
    {
        float min = 30;
        float max = 60;
        float adjust = cam.m_Lens.FieldOfView - (zoom * 10f);

        cam.m_Lens.FieldOfView = Mathf.Clamp(adjust, min, max);
    }
}
