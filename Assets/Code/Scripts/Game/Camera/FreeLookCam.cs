using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class FreeLookCam : MonoBehaviour
{
    [SerializeField] LayerMask cullingMask;

    CinemachineFreeLook cam;
    float yCenterVelocity;
    //float yCenterSmoothTime = 0.5f;

    void Awake()
    {
        cam = GetComponent<CinemachineFreeLook>();
        cam.m_Transitions.m_OnCameraLive.AddListener((vcamIn, vcamOut) =>
        {
            Camera.main.cullingMask = cullingMask;
        });
    }

    void LateUpdate()
    {
        /*
        if (Input.GetAxis("Horizontal") > 0 || Input.GetAxis("Vertical") > 0)
            cam.m_YAxisRecentering.m_enabled = true;
        else
            cam.m_YAxisRecentering.m_enabled = false;
        */

        /*Vector3 input = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        if (input.normalized != Vector3.zero)
        {
            float yVal = Mathf.SmoothDamp(cam.m_YAxis.Value, 0.55f, ref yCenterVelocity, yCenterSmoothTime);
            cam.m_YAxis.Value = yVal;
        }*/

        if (!Mathf.Approximately(Input.GetAxis("Mouse ScrollWheel"), 0))
        {
            Zoom(Input.mouseScrollDelta.y);
        }
    }

    void Zoom(float zoom)
    {
        float topRigHeightMin = 1.62f;
        float topRigHeightMax = 7.62f;
        float midRigRadiusMin = 1.5f;
        float midRigRadiusMax = 7.5f;

        float topRigHeightAdjust = cam.m_Orbits[0].m_Height + (-zoom * 1.5f);
        float midRigRadiusAdjust = cam.m_Orbits[1].m_Radius + (-zoom * 1.5f);

        cam.m_Orbits[0].m_Height = Mathf.Clamp(topRigHeightAdjust, topRigHeightMin, topRigHeightMax);
        cam.m_Orbits[1].m_Radius = Mathf.Clamp(midRigRadiusAdjust, midRigRadiusMin, midRigRadiusMax);
    }
}
