using System.Collections;
using System.Collections.Generic;
using System;

public static class TimeHandler
{
    static TimeSpan DayStart = new TimeSpan(6, 0, 0);
    static TimeSpan AfternoonStart = new TimeSpan(12, 0, 0);
    static TimeSpan EveningStart = new TimeSpan(18, 0, 0);
    static TimeSpan NightStart = new TimeSpan(20, 0, 0);

    public static bool IsDay(FourTime fourTime)
    {
        bool isDay = false;

        switch (fourTime)
        {
            case FourTime.Morning:
            case FourTime.Afternoon:
            case FourTime.Evening:
                isDay = true;
                break;
            case FourTime.Night:
                isDay = false;
                break;
        }

        return isDay;
    }

    public static float GetSunXRot(FourTime fourTime)
    {
        float sunXRot = 50;

        switch (fourTime)
        {
            case FourTime.Morning:
                sunXRot = 50f;
                break;
            case FourTime.Afternoon:
                sunXRot = 90f;
                break;
            case FourTime.Evening:
                sunXRot = 158.57f;
                break;
            case FourTime.Night:
                sunXRot = 10f;
                break;
        }

        return sunXRot;
    }

    public static FourTime GetRealTime()
    {
        if (DateTime.Now.TimeOfDay >= DayStart && DateTime.Now.TimeOfDay <= AfternoonStart)
            return FourTime.Morning;
        else if (DateTime.Now.TimeOfDay >= AfternoonStart && DateTime.Now.TimeOfDay <= EveningStart)
            return FourTime.Afternoon;
        else if (DateTime.Now.TimeOfDay >= EveningStart && DateTime.Now.TimeOfDay <= NightStart)
            return FourTime.Evening;
        else
            return FourTime.Night;
    }
}
