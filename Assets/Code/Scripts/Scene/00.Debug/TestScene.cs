using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TestScene : MonoBehaviour
{
    [Header("UI")]
    public Text heartCnt;
    public Text score;
    public Text block;
    public Text money;
    public Text gameMsg;
    public Button pauseBtn;
    public Button continueBtn;
    public Button toTitleBtn;
    public Button resultBtn;

    public GameObject storeGameCtrlPrefab;
    GameManager gm;

    void Awake()
    {
        gm = GameManager.Instance;

        pauseBtn.onClick.AddListener(() =>
        {
            gm.gameCtrl.Pause();
        });

        continueBtn.onClick.AddListener(() =>
        {
            gm.gameCtrl.Continue();
            continueBtn.gameObject.SetActive(false);
            toTitleBtn.gameObject.SetActive(false);
        });

        toTitleBtn.onClick.AddListener(() =>
        {
            //storeGameCtrl.NextGame(false);
            gm.LoadScene("Title");
        });

        resultBtn.onClick.AddListener(() =>
        {
            //storeGameCtrl.NextGame(false);
            gm.LoadScene("Shop");
        });
    }

    // Start is called before the first frame update
    void Start()
    {
        gm.gameCtrl = Instantiate(storeGameCtrlPrefab).GetComponent<StoreGameCtrl>();
    }

    // Update is called once per frame
    void Update()
    {
        heartCnt.text = gm.gdPlayerData.Life.ToString();
        score.text = gm.gdPlayerData.Score.ToString();
        block.text = gm.gdPlayerData.Block.ToString();
        money.text = gm.gdPlayerData.Money.ToString();

        if (gm.gameCtrl.gameProgress == GameProgress.Clear)
        {
            gameMsg.text = "Clear!";
            resultBtn.gameObject.SetActive(true);
        }
        else if (gm.gameCtrl.gameProgress == GameProgress.Fail)
        {
            gameMsg.text = "Game Over";
            continueBtn.gameObject.SetActive(true);
            toTitleBtn.gameObject.SetActive(true);
        }
        else
        {
            gameMsg.text = "";
        }
    }
}
