using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Cinemachine;

public class TourScene : MonoBehaviour
{
    [Header("Island")]
    [SerializeField] Island island;

    [Header("UI")]
    [SerializeField] Text moneyText;
    [SerializeField] Text scoreText;
    [SerializeField] Text blockText;
    [SerializeField] Text titleText;

    [SerializeField] Button morningBtn;
    [SerializeField] Button afternoonBtn;
    [SerializeField] Button eveningBtn;
    [SerializeField] Button nightBtn;

    [SerializeField] Button titleCamBtn;
    [SerializeField] Button titleZoomCamBtn;
    [SerializeField] Button topCam;
    [SerializeField] Button sideCam;

    [SerializeField] Button titleBtn;
    [SerializeField] Button bookBtn;
    [SerializeField] Button creditBtn;
    [SerializeField] Button startBtn;

    GameManager gm;

    void Awake()
    {
        gm = GameManager.Instance;
    }

    void Start()
    {
        gm.LoadUIText();

        // Fourtime
        morningBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();
            ChangeTime(FourTime.Afternoon);
        });
        afternoonBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();
            ChangeTime(FourTime.Evening);
        });
        eveningBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();
            ChangeTime(FourTime.Night);
        });
        nightBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();
            ChangeTime(FourTime.Morning);
        });

        // Camera
        titleCamBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();
            ChangeZoomCam(IslandCamType.TitleZoomCam);
        });
        titleZoomCamBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();
            ChangeZoomCam(IslandCamType.TitleCam);
        });
        topCam.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();
            ChangePosCam(IslandCamType.SideCam);
        });
        sideCam.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();
            ChangePosCam(IslandCamType.TopCam);
        });

        titleBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();

            gm.LoadScene("Title");
        });

        bookBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();

            gm.LoadScene("Book");
        });

        creditBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();

            gm.LoadScene("Credits");
        });

        startBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();

            gm.gdStageMst = gm.dbHandler.StageMstDic[island.currStageIdx];

            gm.InitPlayerData(true);
            gm.gdPlayerData.LastStageIdx = island.currStageIdx;

            gm.LoadScene("Game");
        });

        // Last stage
        island.SelectStage(gm.gdPlayerData.LastStageIdx);

        // Last fourtime
        switch(gm.gdPlayerData.LastFourTime)
        {
            case FourTime.Morning:
                ChangeTime(FourTime.Morning);
                break;
            case FourTime.Afternoon:
                ChangeTime(FourTime.Afternoon);
                break;
            case FourTime.Evening:
                ChangeTime(FourTime.Evening);
                break;
            case FourTime.Night:
                ChangeTime(FourTime.Night);
                break;
        }

        // Last camera
        IslandCamType lastCamType = gm.gdPlayerData.LastIslandCamType;

        ChangeZoomCam(IslandCamType.TitleCam);
        ChangePosCam(IslandCamType.TopCam);

        switch (lastCamType)
        {
            case IslandCamType.TitleCam:
                ChangeZoomCam(IslandCamType.TitleCam);
                break;
            case IslandCamType.TitleZoomCam:
                ChangeZoomCam(IslandCamType.TitleZoomCam);
                break;
            case IslandCamType.TopCam:
                ChangePosCam(IslandCamType.TopCam);
                break;
            case IslandCamType.SideCam:
                ChangePosCam(IslandCamType.SideCam);
                break;
        }

        // Money
        moneyText.text = gm.gdPlayerData.Money.ToString();

        // Score
        scoreText.text = gm.gdPlayerData.Score.ToString();

        // Block
        blockText.text = gm.gdPlayerData.Block.ToString();
    }

    void Update()
    {
        int locMStId = gm.dbHandler.StageMstDic[island.currStageIdx].LocMStId;
        if (locMStId != -1)
            titleText.text = gm.dbHandler.LocMstDic[locMStId][gm.gdPlayerData.Lang];
    }

    void ChangeTime(FourTime time)
    {
        gm.gdPlayerData.LastFourTime = time;
        island.CreateEnvironment(gm.gdPlayerData.LastFourTime);

        morningBtn.gameObject.SetActive(time == FourTime.Morning);
        afternoonBtn.gameObject.SetActive(time == FourTime.Afternoon);
        eveningBtn.gameObject.SetActive(time == FourTime.Evening);
        nightBtn.gameObject.SetActive(time == FourTime.Night);
    }

    void ChangeZoomCam(IslandCamType islandCamType)
    {
        island.SetCamera(islandCamType);
        gm.gdPlayerData.LastIslandCamType = islandCamType;

        titleCamBtn.gameObject.SetActive(islandCamType == IslandCamType.TitleCam);
        titleZoomCamBtn.gameObject.SetActive(islandCamType == IslandCamType.TitleZoomCam);
    }

    void ChangePosCam(IslandCamType islandCamType)
    {
        island.SetCamera(islandCamType);
        gm.gdPlayerData.LastIslandCamType = islandCamType;

        topCam.gameObject.SetActive(islandCamType == IslandCamType.TopCam);
        sideCam.gameObject.SetActive(islandCamType == IslandCamType.SideCam);
    }
}
