using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BookScene : MonoBehaviour
{
    enum Page
    {
        Page1,
        Page2
    }

    const float blockStateCheckInterval = 1;
    const float blockDestroyDelay = 3;
    const float bgImgMoveSpeed = 5f;

    [Header("Studio Player")]
    [SerializeField] Animator playerAnim;

    [Header("Studio Creature")]
    [SerializeField] Animator cowAnim;
    [SerializeField] Animator foxAnim;
    [SerializeField] Animator chickAnim;

    [Header("Studio Volcano")]
    [SerializeField] GameObject studioVolcano;
    [SerializeField] GameObject volcanoBlockPlaceholder;

    [Header("Studio Item")]
    [SerializeField] GameObject studioItem;
    [SerializeField] GameObject appleItemPlaceholder;
    [SerializeField] GameObject steakItemPlaceholder;
    [SerializeField] GameObject boneItemPlaceholder;
    [SerializeField] GameObject pooItemPlaceholder;

    [Header("BlockPrefabs")]
    [SerializeField] GameObject volcanoBlockPrefab;

    [Header("ItemPrefabs")]
    [SerializeField] GameObject appleItemPrefab;
    [SerializeField] GameObject steakItemPrefab;
    [SerializeField] GameObject boneItemPrefab;
    [SerializeField] GameObject pooItemPrefab;

    [Header("Camera images")]
    [SerializeField] RawImage playerCamImg;
    [SerializeField] Camera playerCam;
    [SerializeField] RawImage goalBlockCamImg;
    [SerializeField] Camera goalBlockCam;
    [SerializeField] RawImage cowCamImg;
    [SerializeField] Camera cowCam;
    [SerializeField] RawImage foxCamImg;
    [SerializeField] Camera foxCam;
    [SerializeField] RawImage chickCamImg;
    [SerializeField] Camera chickCam;
    [SerializeField] RawImage kiteCamImg;
    [SerializeField] Camera kiteCam;
    [SerializeField] RawImage dragonCamImg;
    [SerializeField] Camera dragonCam;
    [SerializeField] RawImage volcanoBlockCamImg;
    [SerializeField] Camera volcanoBlockCam;
    [SerializeField] RawImage appleItemCamImg;
    [SerializeField] Camera appleItemCam;
    [SerializeField] RawImage steakItemCamImg;
    [SerializeField] Camera steakItemCam;
    [SerializeField] RawImage boneItemCamImg;
    [SerializeField] Camera boneItemCam;
    [SerializeField] RawImage pooItemCamImg;
    [SerializeField] Camera pooItemCam;

    RenderTexture playerCamRndTexture;
    RenderTexture goalBlockCamRndTexture;
    RenderTexture cowCamRndTexture;
    RenderTexture foxCamRndTexture;
    RenderTexture chickCamRndTexture;
    RenderTexture kiteCamRndTexture;
    RenderTexture dragonCamRndTexture;
    RenderTexture volcanoBlockCamRndTexture;
    RenderTexture appleItemCamRndTexture;
    RenderTexture steakItemCamRndTexture;
    RenderTexture boneItemCamRndTexture;
    RenderTexture pooItemCamRndTexture;

    [Header("UI")]
    [SerializeField] Image bgImg;
    [SerializeField] Text prologueText;
    [SerializeField] Text bookText;
    [SerializeField] GameObject page1;
    [SerializeField] GameObject page2;
    [SerializeField] Button skipBtn;
    [SerializeField] Button nextBtn;
    [SerializeField] Button tourBtn;

    Page currentPage = Page.Page1;
    Vector3 bgImgDefPos;
    float bgImgHalfWidth;

    GameManager gm;

    void Awake()
    {
        gm = GameManager.Instance;

        bgImgDefPos = bgImg.transform.position;
        bgImgHalfWidth = bgImg.rectTransform.rect.width / 2;

        // Setup page1
        playerAnim.SetBool("HandOnHips", true);
        cowAnim.SetFloat("Speed_f", 0);
        cowAnim.SetBool("Eat_b", true);
        foxAnim.SetFloat("Speed_f", 0);
        foxAnim.SetBool("Eat_b", true);
        chickAnim.SetFloat("Speed_f", 0);
        chickAnim.SetBool("Eat_b", true);

        gm.SetCamera(playerCam, playerCamRndTexture, playerCamImg);
        gm.SetCamera(goalBlockCam, goalBlockCamRndTexture, goalBlockCamImg);
        gm.SetCamera(cowCam, cowCamRndTexture, cowCamImg);
        gm.SetCamera(foxCam, foxCamRndTexture, foxCamImg);
        gm.SetCamera(chickCam, chickCamRndTexture, chickCamImg);
        gm.SetCamera(kiteCam, kiteCamRndTexture, kiteCamImg);
        gm.SetCamera(dragonCam, dragonCamRndTexture, dragonCamImg);

        prologueText.gameObject.SetActive(gm.gameMode != GameMode.Tour);
        bookText.gameObject.SetActive(gm.gameMode == GameMode.Tour);

        skipBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();

            if (gm.gameMode == GameMode.Tour)
                gm.LoadScene("Tour");
            else
                gm.LoadScene("Prologue");
        });

        nextBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();

            switch (currentPage)
            {
                case Page.Page1:
                    // Volcano
                    studioVolcano.SetActive(true);
                    StartCoroutine(ActivateBlock(volcanoBlockPrefab, volcanoBlockPlaceholder));

                    gm.SetCamera(volcanoBlockCam, volcanoBlockCamRndTexture, volcanoBlockCamImg);

                    // Item
                    studioItem.SetActive(true);
                    Instantiate(appleItemPrefab, appleItemPlaceholder.transform.position, appleItemPrefab.transform.rotation, appleItemPlaceholder.transform);
                    Instantiate(steakItemPrefab, steakItemPlaceholder.transform.position, steakItemPrefab.transform.rotation, steakItemPlaceholder.transform);
                    Instantiate(boneItemPrefab, boneItemPlaceholder.transform.position, boneItemPrefab.transform.rotation, boneItemPlaceholder.transform);
                    Instantiate(pooItemPrefab, pooItemPlaceholder.transform.position, pooItemPrefab.transform.rotation, pooItemPlaceholder.transform);

                    gm.SetCamera(appleItemCam, appleItemCamRndTexture, appleItemCamImg);
                    gm.SetCamera(steakItemCam, steakItemCamRndTexture, steakItemCamImg);
                    gm.SetCamera(boneItemCam, boneItemCamRndTexture, boneItemCamImg);
                    gm.SetCamera(pooItemCam, pooItemCamRndTexture, pooItemCamImg);

                    // UI
                    page1.SetActive(false);
                    page2.SetActive(true);

                    skipBtn.gameObject.SetActive(false);
                    if (gm.gameMode == GameMode.Tour)
                    {
                        tourBtn.gameObject.SetActive(true);
                        nextBtn.gameObject.SetActive(false);
                    }
                    else
                        tourBtn.gameObject.SetActive(false);

                    currentPage = Page.Page2;
                    break;
                case Page.Page2:
                    gm.LoadScene("Prologue");
                    break;
                default:
                    break;
            }
        });

        tourBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();
            gm.LoadScene("Tour");
        });
    }

    void Start()
    {
        gm.LoadUIText();
    }

    void Update()
    {
        // Sky, skyRoof
        bgImg.gameObject.SetActive(true);
        bgImg.gameObject.SetActive(true);

        if (bgImg.rectTransform.offsetMin.x < -bgImgHalfWidth)
            bgImg.transform.position = bgImgDefPos;
        else
            bgImg.transform.Translate(Vector3.left * bgImgMoveSpeed * Time.deltaTime);
    }

    IEnumerator ActivateBlock(GameObject blockPrefab, GameObject placeHolder)
    {
        // 1. Instantiate block prefab
        GameObject obj = Instantiate(blockPrefab, placeHolder.transform.position, blockPrefab.transform.rotation, placeHolder.transform);
        Block block = obj.GetComponent<Block>();
        if (block != null) // Normal block
        {
            block.Invoke("ForceActivation", 0);

            // 2. Wait for sleep state
            while (true)
            {
                if (block.currState == Block.State.Sleep)
                    break;

                yield return new WaitForSeconds(blockStateCheckInterval);
            }

            // 3. Destroy block
            yield return new WaitForSeconds(blockDestroyDelay);

            Destroy(obj);

            // 4. Reinstantiate block prefab
            StartCoroutine(ActivateBlock(blockPrefab, placeHolder));
        }
    }

    void OnDestroy()
    {
        if (playerCamRndTexture != null)
            playerCamRndTexture.Release();

        if (goalBlockCamRndTexture != null)
            goalBlockCamRndTexture.Release();

        if (cowCamRndTexture != null)
            cowCamRndTexture.Release();

        if (foxCamRndTexture != null)
            foxCamRndTexture.Release();

        if (chickCamRndTexture != null)
            chickCamRndTexture.Release();

        if (kiteCamRndTexture != null)
            kiteCamRndTexture.Release();

        if (dragonCamRndTexture != null)
            dragonCamRndTexture.Release();

        if (volcanoBlockCamRndTexture != null)
            volcanoBlockCamRndTexture.Release();

        if (appleItemCamRndTexture != null)
            appleItemCamRndTexture.Release();

        if (steakItemCamRndTexture != null)
            steakItemCamRndTexture.Release();

        if (boneItemCamRndTexture != null)
            boneItemCamRndTexture.Release();

        if (pooItemCamRndTexture != null)
            pooItemCamRndTexture.Release();
    }
}