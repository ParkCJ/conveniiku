using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopScene : MonoBehaviour
{
    [Header("UI")]
    [SerializeField] Text coinCnt;
    [SerializeField] VerticalLayoutGroup sweetsGrp;
    [SerializeField] VerticalLayoutGroup abilityGrp;
    [SerializeField] SweetUI sweetUIPrefab;
    [SerializeField] Button epilogueBtn;
    [SerializeField] Button tourBtn;
    [SerializeField] Button myStageBtn;

    [Header("Owner")]
    [SerializeField] Animator ownerAnim;
    [SerializeField] Animator playerAnim;
    [SerializeField] RawImage shopCamImg;
    [SerializeField] Camera shopCam;
    Dictionary<int, string> ownerAnimDic = new Dictionary<int, string>();
    Dictionary<int, string> playerAnimDic = new Dictionary<int, string>();
    Coroutine buyCoroutine;
    RenderTexture ownerCamRndTexture;

    GameManager gm;

    void Awake()
    {
        gm = GameManager.Instance;
    }

    void Start()
    {
        // Sweets
        foreach (string sweetNm in gm.dbHandler.StageSweetMstDic[gm.gameMode].Keys)
        {
            if (gm.dbHandler.StageSweetMstDic[gm.gameMode][sweetNm])
            {
                SweetMst sweetMst = gm.dbHandler.SweetMstDic[sweetNm];

                SweetUI sweetUI;

                if (sweetMst.Ability != Ability.None)
                    sweetUI = Instantiate(sweetUIPrefab, abilityGrp.transform);
                else
                    sweetUI = Instantiate(sweetUIPrefab, sweetsGrp.transform);

                sweetUI.SweetMst = sweetMst;
            }
        }

        gm.LoadUIText();

        // UI
        epilogueBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();

            gm.LoadScene("Epilogue");
        });
        epilogueBtn.gameObject.SetActive(gm.gameMode == GameMode.Arcade);

        tourBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();
            gm.SaveData();
            gm.LoadScene("Tour");
        });
        tourBtn.gameObject.SetActive(gm.gameMode == GameMode.Tour);

        myStageBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();
            gm.SaveData();
            gm.LoadScene("MyStage");
        });
        myStageBtn.gameObject.SetActive(gm.gameMode == GameMode.MyStage);

        // Character image
        gm.SetCamera(shopCam, ownerCamRndTexture, shopCamImg);

        ownerAnimDic[0] = "HandOnHips";
        ownerAnimDic[1] = "CrossArms";
        ownerAnimDic[2] = "Dance";
        ownerAnimDic[3] = "IsSaluting";
        ownerAnimDic[4] = "IsHappyIdle";
        playerAnimDic[0] = "HandOnHips";
        playerAnimDic[1] = "CrossArms";
        playerAnimDic[2] = "Dance";
        playerAnimDic[3] = "IsSaluting";
        playerAnimDic[4] = "IsHappyIdle";

        StartCoroutine(PlayRndAnim(ownerAnim, ownerAnimDic));
        StartCoroutine(PlayRndAnim(playerAnim, playerAnimDic));

        // Update
        UpdateStatus();
    }

    void UpdateStatus()
    {
        // Coin count
        coinCnt.text = gm.gdPlayerData.Money.ToString();

        // Sweets
        foreach (SweetUI sweetUI in sweetsGrp.GetComponentsInChildren<SweetUI>())
            sweetUI.UpdateStatus();

        // Abilities
        foreach (SweetUI sweetUI in abilityGrp.GetComponentsInChildren<SweetUI>())
            sweetUI.UpdateStatus();
    }

    public void Buy()
    {
        UpdateStatus();

        if (buyCoroutine == null)
        {
            StopAllCoroutines();
            buyCoroutine = StartCoroutine(PlayBuyAnim());
        }
    }

    IEnumerator PlayRndAnim(Animator anim, Dictionary<int, string> animDic)
    {
        int idx = 0;

        while (true)
        {
            anim.SetBool(animDic[idx], false);

            idx = Random.Range(0, animDic.Keys.Count);
            anim.SetBool(animDic[idx], true);

            yield return new WaitForSeconds(Random.Range(2, 3.0f));
        }
    }

    IEnumerator PlayBuyAnim()
    {
        ownerAnim.Rebind();
        ownerAnim.SetTrigger("Buy1");

        playerAnim.Rebind();
        playerAnim.SetTrigger("Buy2");

        yield return new WaitForSeconds(1.8f); // Buy animation duration

        StartCoroutine(PlayRndAnim(ownerAnim, ownerAnimDic));
        StartCoroutine(PlayRndAnim(playerAnim, playerAnimDic));

        buyCoroutine = null;
    }

    void OnDestroy()
    {
        if (ownerCamRndTexture != null)
            ownerCamRndTexture.Release();
    }
}