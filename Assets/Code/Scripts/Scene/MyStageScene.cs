using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MyStageScene : MonoBehaviour
{
    const float bgImgMoveSpeed = 5f;

    [Header("UI")]
    [SerializeField] Image bgImg;
    [SerializeField] Text myStageText;
    [SerializeField] Text stageMsg;
    [SerializeField] Text blockMsg;
    [SerializeField] Text obstMsg;
    [SerializeField] Text itemMsg;

    [SerializeField] Button morningBtn;
    [SerializeField] Button afternoonBtn;
    [SerializeField] Button eveningBtn;
    [SerializeField] Button nightBtn;

    [SerializeField] Button titleBtn;
    [SerializeField] Button randomBtn;
    [SerializeField] Button playBtn;

    [Header("Play")]
    [SerializeField] OptionSliderUI lifeSliderUI;
    [SerializeField] OptionSliderUI stageSizeXSliderUI;
    [SerializeField] OptionSliderUI stageSizeZSliderUI;
    [SerializeField] OptionSliderUI obstBlockRateSliderUI;

    [Header("Block")]
    [SerializeField] OptionSliderUI jumpBlockSliderUI;
    [SerializeField] OptionSliderUI fallBlockSliderUI;
    [SerializeField] OptionSliderUI trampolineBlockSliderUI;
    [SerializeField] OptionSliderUI fireBlockSliderUI;
    [SerializeField] OptionSliderUI iceBlockSliderUI;
    [SerializeField] OptionSliderUI enemyGroundBlockSliderUI;
    [SerializeField] OptionSliderUI enemySkyBlockSliderUI;
    [SerializeField] OptionSliderUI zeroGravityBlockSliderUI;
    [SerializeField] OptionSliderUI hammerBlockSliderUI;
    [SerializeField] OptionSliderUI dragonBlockSliderUI;
    [SerializeField] OptionSliderUI volcanoBlockSliderUI;

    [Header("Obstacle")]
    [SerializeField] OptionSliderUI cowObstSliderUI;
    [SerializeField] OptionSliderUI foxObstSliderUI;
    [SerializeField] OptionSliderUI chickObstSliderUI;
    [SerializeField] OptionSliderUI kiteObstSliderUI;
    [SerializeField] OptionSliderUI dragonObstSliderUI;
    [SerializeField] OptionSliderUI hammerObstSliderUI;

    [Header("Item")]
    [SerializeField] OptionSliderUI coinItemSliderUI;
    [SerializeField] OptionSliderUI steakItemSliderUI;
    [SerializeField] OptionSliderUI boneItemSliderUI;
    [SerializeField] OptionSliderUI appleItemSliderUI;
    [SerializeField] OptionSliderUI bananaItemSliderUI;
    [SerializeField] OptionSliderUI pizzaItemSliderUI;
    [SerializeField] OptionSliderUI pooItemSliderUI;

    [Header("Environment")]
    [SerializeField] NightDome nightDome;

    Vector3 camPos;
    Vector3 camRot;
    //KeyValuePair<MSD, Dictionary<string, BlockMst>> blockMstPair;
    //KeyValuePair<MSD, Dictionary<string, ObstacleMst>> obstMstPair;
    //KeyValuePair<MSD, Dictionary<string, ItemMst>> itemMstPair;

    Vector3 bgImgDefPos;
    float bgImgHalfWidth;

    FourTime fourTime;

    GameManager gm;
    PlayerMyStage myStageSave;

    void Awake()
    {
        gm = GameManager.Instance;
        myStageSave = gm.gdPlayerData.PlayerMyStage;

        bgImgDefPos = bgImg.transform.position;
        bgImgHalfWidth = bgImg.rectTransform.rect.width / 2;

        titleBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();

            gm.LoadScene("Title");
        });

        playBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();

            // 1. setup game
            StageMst stageMst = gm.dbHandler.StageMstDic[300];
            Dictionary<string, int> blockMstDic = gm.dbHandler.BlockMstDic["MyStage"];
            Dictionary<string, int> obstMstDic = gm.dbHandler.ObstMstDic["MyStage"];
            Dictionary<string, int> itemMstDic = gm.dbHandler.StageItemMstDic["MyStage"];

            // 1) set stage
            stageMst.XSize = stageSizeXSliderUI.Value;
            stageMst.ZSize = stageSizeZSliderUI.Value;
            stageMst.ObstBlockRate = obstBlockRateSliderUI.Value / 100.0f;
            stageMst.FourTime = fourTime;

            // 2) set block
            blockMstDic["JumpBlock"] = jumpBlockSliderUI.Value;
            blockMstDic["FallBlock"] = fallBlockSliderUI.Value;
            blockMstDic["TrampolineBlock"] = trampolineBlockSliderUI.Value;
            blockMstDic["FireBlock"] = fireBlockSliderUI.Value;
            blockMstDic["IceBlock"] = iceBlockSliderUI.Value;

            blockMstDic["CowBlock"] = 0;
            blockMstDic["FoxBlock"] = 0;
            blockMstDic["ChickBlock"] = 0;
            blockMstDic["KiteBlock"] = 0;

            blockMstDic["EnemyGroundBlock"] = enemyGroundBlockSliderUI.Value;
            blockMstDic["EnemySkyBlock"] = enemySkyBlockSliderUI.Value;
            blockMstDic["ZeroGravityBlock"] = zeroGravityBlockSliderUI.Value;
            blockMstDic["HammerBlock"] = hammerBlockSliderUI.Value;
            blockMstDic["DragonBlock"] = dragonBlockSliderUI.Value;
            blockMstDic["VolcanoBlock"] = volcanoBlockSliderUI.Value;

            // 3) set obstacle
            obstMstDic["Cow"] = cowObstSliderUI.Value;
            obstMstDic["Fox"] = foxObstSliderUI.Value;
            obstMstDic["Kite"] = kiteObstSliderUI.Value;
            obstMstDic["Chick"] = chickObstSliderUI.Value;
            obstMstDic["Dragon"] = dragonObstSliderUI.Value;
            obstMstDic["HammerObst"] = hammerObstSliderUI.Value;

            // 4) set item
            itemMstDic["Apple"] = appleItemSliderUI.Value;
            itemMstDic["Banana"] = bananaItemSliderUI.Value;
            itemMstDic["Bone"] = boneItemSliderUI.Value;
            itemMstDic["Coin"] = coinItemSliderUI.Value;
            itemMstDic["Pizza"] = pizzaItemSliderUI.Value;
            itemMstDic["Poo"] = pooItemSliderUI.Value;
            itemMstDic["Steak"] = steakItemSliderUI.Value;

            // 2. Update PlayerData
            myStageSave.Life = lifeSliderUI.Value;
            myStageSave.XSize = stageSizeXSliderUI.Value;
            myStageSave.ZSize = stageSizeZSliderUI.Value;
            myStageSave.ObstBlockRate = obstBlockRateSliderUI.Value / 100.0f;
            myStageSave.fourTime = fourTime;

            myStageSave.JumpBlock = jumpBlockSliderUI.Value;
            myStageSave.FallBlock = fallBlockSliderUI.Value;
            myStageSave.TrampolineBlock = trampolineBlockSliderUI.Value;
            myStageSave.FireBlock = fireBlockSliderUI.Value;
            myStageSave.IceBlock = iceBlockSliderUI.Value;
            myStageSave.EnemyGroundBlock = enemyGroundBlockSliderUI.Value;
            myStageSave.EnemySkyBlock = enemySkyBlockSliderUI.Value;
            myStageSave.ZeroGravityBlock = zeroGravityBlockSliderUI.Value;
            myStageSave.HammerBlock = hammerBlockSliderUI.Value;
            myStageSave.DragonBlock = dragonBlockSliderUI.Value;
            myStageSave.VolcanoBlock = volcanoBlockSliderUI.Value;

            myStageSave.CowObst = cowObstSliderUI.Value;
            myStageSave.FoxObst = foxObstSliderUI.Value;
            myStageSave.ChickObst = chickObstSliderUI.Value;
            myStageSave.KiteObst = kiteObstSliderUI.Value;
            myStageSave.DragonObst = dragonObstSliderUI.Value;
            myStageSave.HammerObst = hammerObstSliderUI.Value;

            myStageSave.CoinItem = coinItemSliderUI.Value;
            myStageSave.SteakItem = steakItemSliderUI.Value;
            myStageSave.BoneItem = boneItemSliderUI.Value;
            myStageSave.AppleItem = appleItemSliderUI.Value;
            myStageSave.BananaItem = bananaItemSliderUI.Value;
            myStageSave.PizzaItem = pizzaItemSliderUI.Value;
            myStageSave.PooItem = pooItemSliderUI.Value;

            // 3. play
            gm.gdStageMst = stageMst;

            gm.InitPlayerData(true);
            gm.gdPlayerData.Life = lifeSliderUI.Value;

            gm.LoadScene("Game");
        });

        randomBtn.onClick.AddListener(() =>
        {
            foreach (OptionSliderUI optSlider in GameObject.FindObjectsOfType<OptionSliderUI>())
                optSlider.SetRandom();

            fourTime = (FourTime)Random.Range(0, 4);
            morningBtn.gameObject.SetActive(fourTime == FourTime.Morning);
            afternoonBtn.gameObject.SetActive(fourTime == FourTime.Afternoon);
            eveningBtn.gameObject.SetActive(fourTime == FourTime.Evening);
            nightBtn.gameObject.SetActive(fourTime == FourTime.Night);

            if (!ValidateStageSize() || !ValidateBlockOptions() || !ValidateObstOptions() || !ValidateItemOptions())
                randomBtn.onClick.Invoke();
            else
                gm.PlayBtnClickSound();
        });

        // Fourtime
        morningBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();

            fourTime = FourTime.Afternoon;

            morningBtn.gameObject.SetActive(false);
            afternoonBtn.gameObject.SetActive(true);
            eveningBtn.gameObject.SetActive(false);
            nightBtn.gameObject.SetActive(false);
        });
        afternoonBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();

            fourTime = FourTime.Evening;

            morningBtn.gameObject.SetActive(false);
            afternoonBtn.gameObject.SetActive(false);
            eveningBtn.gameObject.SetActive(true);
            nightBtn.gameObject.SetActive(false);
        });
        eveningBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();

            fourTime = FourTime.Night;

            morningBtn.gameObject.SetActive(false);
            afternoonBtn.gameObject.SetActive(false);
            eveningBtn.gameObject.SetActive(false);
            nightBtn.gameObject.SetActive(true);
        });
        nightBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();

            fourTime = FourTime.Morning;

            morningBtn.gameObject.SetActive(true);
            afternoonBtn.gameObject.SetActive(false);
            eveningBtn.gameObject.SetActive(false);
            nightBtn.gameObject.SetActive(false);
        });
    }

    void Start()
    {
        // 1. load text
        gm.LoadUIText();

        // 2. load savedata
        // Play
        lifeSliderUI.Init(10001, 1, 5, myStageSave.Life);
        stageSizeXSliderUI.Init(10002, 1, 30, myStageSave.XSize);
        stageSizeZSliderUI.Init(10003, 1, 30, myStageSave.ZSize);
        obstBlockRateSliderUI.Init(10004, 0, 100, Mathf.RoundToInt(myStageSave.ObstBlockRate * 100));
        fourTime = myStageSave.fourTime;
        morningBtn.gameObject.SetActive(fourTime == FourTime.Morning);
        afternoonBtn.gameObject.SetActive(fourTime == FourTime.Afternoon);
        eveningBtn.gameObject.SetActive(fourTime == FourTime.Evening);
        nightBtn.gameObject.SetActive(fourTime == FourTime.Night);

        // Block
        jumpBlockSliderUI.Init(1001, -1, 10, myStageSave.JumpBlock);
        fallBlockSliderUI.Init(1002, -1, 10, myStageSave.FallBlock);
        trampolineBlockSliderUI.Init(1003, -1, 10, myStageSave.TrampolineBlock);
        fireBlockSliderUI.Init(1004, -1, 10, myStageSave.FireBlock);
        iceBlockSliderUI.Init(1005, -1, 10, myStageSave.IceBlock);
        enemyGroundBlockSliderUI.Init(1007, -1, 10, myStageSave.EnemyGroundBlock);
        enemySkyBlockSliderUI.Init(1008, -1, 10, myStageSave.EnemySkyBlock);
        zeroGravityBlockSliderUI.Init(1009, -1, 10, myStageSave.ZeroGravityBlock);
        hammerBlockSliderUI.Init(1010, -1, 10, myStageSave.HammerBlock);
        dragonBlockSliderUI.Init(1011, -1, 10, myStageSave.DragonBlock);
        volcanoBlockSliderUI.Init(1012, -1, 10, myStageSave.VolcanoBlock);

        // Obstacle
        cowObstSliderUI.Init(1101, 0, 5, myStageSave.CowObst);
        foxObstSliderUI.Init(1102, 0, 5, myStageSave.FoxObst);
        chickObstSliderUI.Init(1103, 0, 5, myStageSave.ChickObst);
        kiteObstSliderUI.Init(1104, 0, 5, myStageSave.KiteObst);
        dragonObstSliderUI.Init(1105, 0, 5, myStageSave.DragonObst);
        hammerObstSliderUI.Init(1106, 0, 1, myStageSave.HammerObst);

        // Item
        coinItemSliderUI.Init(1201, 0, 5, myStageSave.CoinItem);
        steakItemSliderUI.Init(1202, 0, 1, myStageSave.SteakItem);
        boneItemSliderUI.Init(1203, 0, 1, myStageSave.BoneItem);
        appleItemSliderUI.Init(1204, 0, 5, myStageSave.AppleItem);
        bananaItemSliderUI.Init(1205, 0, 5, myStageSave.BananaItem);
        pizzaItemSliderUI.Init(1206, 0, 5, myStageSave.PizzaItem);
        pooItemSliderUI.Init(1207, 0, 5, myStageSave.PooItem);
    }

    Vector3 GetPos()
    {
        float x = (stageSizeXSliderUI.Value * 0.5f) - 0.5f;

        int max = Mathf.Max(stageSizeXSliderUI.Value, stageSizeZSliderUI.Value);
        float y = max >= 14 ? max * 0.7f : (max > 10 ? max * 0.85f : (max > 4 ? max : (max * 2)));

        float zRate = y < max && stageSizeZSliderUI.Value >= stageSizeXSliderUI.Value ? y : stageSizeZSliderUI.Value;
        float z = -y + ((zRate * 0.5f) + 0.5f);

        return new Vector3(x, y, z);
    }

    void Update()
    {
        // Sky, skyRoof
        bgImg.gameObject.SetActive(true);
        bgImg.gameObject.SetActive(true);

        if (bgImg.rectTransform.offsetMin.x < -bgImgHalfWidth)
            bgImg.transform.position = bgImgDefPos;
        else
            bgImg.transform.Translate(Vector3.left * bgImgMoveSpeed * Time.deltaTime);

        // Night dome
        nightDome.gameObject.SetActive(false);

        // Validation
        bool validStage = ValidateStageSize();
        bool validBlock = ValidateBlockOptions();
        bool validObst = ValidateObstOptions();
        bool validItem = ValidateItemOptions();
        playBtn.interactable = validStage && validBlock && validObst && validItem;
    }

    bool ValidateStageSize()
    {
        if (stageSizeXSliderUI.Value == 1 && stageSizeZSliderUI.Value == 1)
        {
            stageMsg.gameObject.SetActive(true);
            return false;
        }
        else
        {
            stageMsg.gameObject.SetActive(false);
            return true;
        }
    }

    bool ValidateBlockOptions()
    {
        if (obstBlockRateSliderUI.Value == 0)
        {
            blockMsg.gameObject.SetActive(false);
            return true;
        }
        else if (jumpBlockSliderUI.Value < 0 || fallBlockSliderUI.Value < 0 || trampolineBlockSliderUI.Value < 0 ||
            fireBlockSliderUI.Value < 0 || iceBlockSliderUI.Value < 0 ||
            enemyGroundBlockSliderUI.Value < 0 || enemySkyBlockSliderUI.Value < 0)
        {
            blockMsg.gameObject.SetActive(false);
            return true;
        }
        else
        {
            blockMsg.gameObject.SetActive(true);
            return false;
        }
    }

    bool ValidateObstOptions()
    {
        int maxCnt = (stageSizeXSliderUI.Value * stageSizeZSliderUI.Value) - 2;
        int obstCnt = cowObstSliderUI.Value +
                      foxObstSliderUI.Value +
                      chickObstSliderUI.Value +
                      kiteObstSliderUI.Value +
                      dragonObstSliderUI.Value +
                      hammerObstSliderUI.Value;

        if (obstCnt <= maxCnt)
        {
            obstMsg.gameObject.SetActive(false);
            return true;
        }
        else
        {
            obstMsg.gameObject.SetActive(true);
            return false;
        }
    }

    bool ValidateItemOptions()
    {
        int maxCnt = (stageSizeXSliderUI.Value * stageSizeZSliderUI.Value) - 2;
        int itemCnt = coinItemSliderUI.Value +
                      steakItemSliderUI.Value +
                      boneItemSliderUI.Value +
                      appleItemSliderUI.Value +
                      bananaItemSliderUI.Value +
                      pizzaItemSliderUI.Value +
                      pooItemSliderUI.Value;

        if (itemCnt <= maxCnt)
        {
            itemMsg.gameObject.SetActive(false);
            return true;
        }
        else
        {
            itemMsg.gameObject.SetActive(true);
            return false;
        }
    }
}