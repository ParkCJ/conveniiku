using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PrologueScene : MonoBehaviour
{
    enum Page
    {
        Page1,
        Page2
    }

    const float bgImgMoveSpeed = 5f;

    [Header("Studio0")]
    [SerializeField] GameObject studio0;
    [SerializeField] Animator playerAnim;

    [Header("Camera images")]
    [SerializeField] RawImage page1PlayerCamImg;
    [SerializeField] RawImage page2PlayerCamImg;
    [SerializeField] Camera playerCam;

    RenderTexture playerCamRndTexture;

    [Header("UI")]
    [SerializeField] Image bgImg;
    [SerializeField] GameObject page1;
    [SerializeField] GameObject page2;
    [SerializeField] Button skipBtn;
    [SerializeField] Button nextBtn;
    [SerializeField] Button tutorialBtn;

    Page currentPage = Page.Page1;
    Vector3 bgImgDefPos;
    float bgImgHalfWidth;

    GameManager gm;

    void Awake()
    {
        gm = GameManager.Instance;

        bgImgDefPos = bgImg.transform.position;
        bgImgHalfWidth = bgImg.rectTransform.rect.width / 2;

        // Setup page1
        playerAnim.SetBool("HandOnHips", true);

        skipBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();

            gm.LoadScene("Tutorial");
        });

        nextBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();

            switch (currentPage)
            {
                case Page.Page1:
                    // Studio
                    playerAnim.SetBool("HandOnHips", false);
                    playerAnim.SetBool("CrossArms", true);

                    // UI
                    page1.SetActive(false);
                    page2.SetActive(true);

                    skipBtn.gameObject.SetActive(false);
                    nextBtn.gameObject.SetActive(false);
                    tutorialBtn.gameObject.SetActive(true);

                    currentPage = Page.Page2;
                    break;
                default:
                    break;
            }
        });

        tutorialBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();

            gm.LoadScene("Tutorial");
        });
    }

    void Start()
    {
        gm.LoadUIText();
        gm.SetCamera(playerCam, playerCamRndTexture, page1PlayerCamImg, page2PlayerCamImg);
    }

    void Update()
    {
        // Sky, skyRoof
        bgImg.gameObject.SetActive(true);

        if (bgImg.rectTransform.offsetMin.x < -bgImgHalfWidth)
            bgImg.transform.position = bgImgDefPos;
        else
            bgImg.transform.Translate(Vector3.left * bgImgMoveSpeed * Time.deltaTime);
    }

    void OnDestroy()
    {
        if (playerCamRndTexture != null)
            playerCamRndTexture.Release();
    }
}