using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Cinemachine;

public class TutorialScene : MonoBehaviour
{
    const float blockStateCheckInterval = 1;
    const float blockDestroyDelay = 3;
    const float bgImgMoveSpeed = 5f;

    [Header("StudioTutorial")]
    [SerializeField] GameObject studioTutorial;

    [Header("Camera images")]
    RenderTexture tutorialCamRndTexture;

    [Header("UI")]
    [SerializeField] Image bgImg;
    [SerializeField] GameObject attackArea;
    [SerializeField] Button startBtn;

    [Header("Tutorial")]
    [SerializeField] CinemachineVirtualCamera tutorialCam;
    [SerializeField] RawImage tutorialCamImg;
    [SerializeField] Text tutorialMsg;
    [SerializeField] Text tutorialSuccessMsg;

    Vector3 bgImgDefPos;
    float bgImgHalfWidth;

    public GameObject storeGameCtrlPrefab;
    CamType playerCamType;
    GameManager gm;

    void Awake()
    {
        gm = GameManager.Instance;

        bgImgDefPos = bgImg.transform.position;
        bgImgHalfWidth = bgImg.rectTransform.rect.width / 2;

        gm.SetCamera(Camera.main, tutorialCamRndTexture, tutorialCamImg);

        attackArea.SetActive(gm.gdPlayerData.BattleMode);

        startBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();

            gm.gdStageMst = gm.dbHandler.StageMstDic[100];

            gm.InitPlayerData(true);
            gm.gdPlayerData.camType = playerCamType;

            gm.LoadScene("Game");
        });
    }

    void Start()
    {
        gm.LoadUIText();

        gm.gdStageMst = gm.dbHandler.StageMstDic[1];
        gm.gdPlayerData.Life = gm.dbHandler.DefDataMst.DefMaxLifeCnt;
        gm.gdPlayerData.Bomb = gm.dbHandler.DefDataMst.DefMaxBomb;

        Instantiate(storeGameCtrlPrefab).GetComponent<StoreGameCtrl>();

        playerCamType = gm.gdPlayerData.camType;
        gm.gdPlayerData.camType = CamType.Top;
        gm.gameCtrl.SetCam();
    }

    void Update()
    {
        // Sky, skyRoof
        bgImg.gameObject.SetActive(true);
        bgImg.gameObject.SetActive(true);

        if (bgImg.rectTransform.offsetMin.x < -bgImgHalfWidth)
            bgImg.transform.position = bgImgDefPos;
        else
            bgImg.transform.Translate(Vector3.left * bgImgMoveSpeed * Time.deltaTime);

        // Tutorial
        if (gm.gameCtrl.gameProgress == GameProgress.Clear)
        {
            tutorialMsg.gameObject.SetActive(false);
            tutorialSuccessMsg.gameObject.SetActive(true);
        }
    }

    void OnDestroy()
    {
        if (tutorialCamRndTexture != null)
            tutorialCamRndTexture.Release();
    }
}