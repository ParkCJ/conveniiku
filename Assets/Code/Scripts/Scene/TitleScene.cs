using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class TitleScene : MonoBehaviour
{
    const float pumpInterval = 4;
    const float pumpScaleMax = 1.2f;

    [Header("UI")]
    [SerializeField] GameObject selectArea;
    [SerializeField] Button startBtn;
    [SerializeField] Button configBtn;

    [SerializeField] GameObject gameModeArea;
    [SerializeField] Button arcadeBtn;
    [SerializeField] Button tourBtn;
    [SerializeField] Button myStageBtn;
    [SerializeField] Button gameModeBackBtn;

    [SerializeField] GameObject configArea;
    [SerializeField] Button modeBtn;
    [SerializeField] Button langBtn;
    [SerializeField] Button camBtn;
    [SerializeField] Image dataIcon;
    [SerializeField] Button dataBtn;
    [SerializeField] Button configBackBtn;

    [SerializeField] GameObject modeArea;
    [SerializeField] Button modeOriginalBtn;
    [SerializeField] Button modeBattleBtn;
    [SerializeField] Button modeBackBtn;

    [SerializeField] GameObject camArea;
    [SerializeField] Button camHRotNorBtn;
    [SerializeField] Button camHRotRevBtn;
    [SerializeField] Button camVRotNorBtn;
    [SerializeField] Button camVRotRevBtn;
    [SerializeField] Slider camShakeRateSlider;
    [SerializeField] Text camShakeRateSliderCurr;
    [SerializeField] Button camBackBtn;

    [SerializeField] GameObject langArea;
    [SerializeField] Button langJaBtn;
    [SerializeField] Button langEnBtn;
    [SerializeField] Button langKoBtn;
    [SerializeField] Button langBackBtn;

    [SerializeField] GameObject saveDataArea;
    [SerializeField] Button delSaveDataYesBtn;
    [SerializeField] Button delSaveDataNoBtn;

    Coroutine pumpCoroutine;
    bool pump = false;

    GameManager gm;

    void Start()
    {
        gm = GameManager.Instance;

        gm.LoadUIText();
        SetUI();

        // StartBtn
        pumpCoroutine = StartCoroutine(PumpStartBtn());
    }

    void Update()
    {
        if (pump)
        {
            if (startBtn.transform.localScale.x < pumpScaleMax)
                startBtn.transform.localScale += Vector3.one * Time.deltaTime;
            else
                pump = false;
        }
        else
        {
            if (startBtn.transform.localScale.x > Vector3.one.x)
                startBtn.transform.localScale -= Vector3.one * Time.deltaTime;
        }
    }

    // UI
    void SetUI()
    {
        startBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();

            selectArea.SetActive(false);
            gameModeArea.SetActive(true);

            StopCoroutine(pumpCoroutine);
        });

        arcadeBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();

            gm.gameMode = GameMode.Arcade;
            gm.LoadScene("Book");
        });

        tourBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();

            gm.gameMode = GameMode.Tour;
            gm.LoadScene("Tour");
        });
        tourBtn.interactable = gm.IsSaveDataExist;

        myStageBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();

            gm.gameMode = GameMode.MyStage;
            gm.LoadScene("MyStage");
        });
        myStageBtn.interactable = gm.gdPlayerData.MyStageUnlocked;

        gameModeBackBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();

            selectArea.SetActive(true);
            gameModeArea.SetActive(false);

            startBtn.transform.localScale = Vector3.one;
            pumpCoroutine = StartCoroutine(PumpStartBtn());
        });

        configBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();

            selectArea.SetActive(false);
            configArea.SetActive(true);

            StopCoroutine(pumpCoroutine);
        });

        configBackBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();

            selectArea.SetActive(true);
            configArea.SetActive(false);

            startBtn.transform.localScale = Vector3.one;
            pumpCoroutine = StartCoroutine(PumpStartBtn());
        });

        // Mode config
        modeBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();

            modeArea.SetActive(true);
            configArea.SetActive(false);

            StopCoroutine(pumpCoroutine);
        });

        modeOriginalBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();
            gm.gdPlayerData.BattleMode = false;
            modeOriginalBtn.gameObject.GetComponent<Text>().fontStyle = FontStyle.Bold;
            modeBattleBtn.gameObject.GetComponent<Text>().fontStyle = FontStyle.Normal;
        });

        modeBattleBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();
            gm.gdPlayerData.BattleMode = true;
            modeOriginalBtn.gameObject.GetComponent<Text>().fontStyle = FontStyle.Normal;
            modeBattleBtn.gameObject.GetComponent<Text>().fontStyle = FontStyle.Bold;
        });

        modeBackBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();

            configArea.SetActive(true);
            modeArea.SetActive(false);
        });

        // Camera config
        camBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();

            camArea.SetActive(true);
            configArea.SetActive(false);

            StopCoroutine(pumpCoroutine);
        });

        camHRotNorBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();
            gm.gdPlayerData.ActionCamHori = Reverse.Normal;
            camHRotNorBtn.gameObject.GetComponent<Text>().fontStyle = FontStyle.Bold;
            camHRotRevBtn.gameObject.GetComponent<Text>().fontStyle = FontStyle.Normal;
        });

        camHRotRevBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();
            gm.gdPlayerData.ActionCamHori = Reverse.Reverse;
            camHRotNorBtn.gameObject.GetComponent<Text>().fontStyle = FontStyle.Normal;
            camHRotRevBtn.gameObject.GetComponent<Text>().fontStyle = FontStyle.Bold;
        });

        camVRotNorBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();
            gm.gdPlayerData.ActionCamVert = Reverse.Normal;
            camVRotNorBtn.gameObject.GetComponent<Text>().fontStyle = FontStyle.Bold;
            camVRotRevBtn.gameObject.GetComponent<Text>().fontStyle = FontStyle.Normal;
        });

        camVRotRevBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();
            gm.gdPlayerData.ActionCamVert = Reverse.Reverse;
            camVRotNorBtn.gameObject.GetComponent<Text>().fontStyle = FontStyle.Normal;
            camVRotRevBtn.gameObject.GetComponent<Text>().fontStyle = FontStyle.Bold;
        });

        camShakeRateSlider.onValueChanged.AddListener((x) =>
        {
            gm.PlayBtnClickSound();

            gm.gdPlayerData.ActionCamShakeRate = x;
            camShakeRateSliderCurr.text = x.ToString();
        });

        camBackBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();

            configArea.SetActive(true);
            camArea.SetActive(false);
        });

        // Language config
        langBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();

            langArea.SetActive(true);
            configArea.SetActive(false);

            StopCoroutine(pumpCoroutine);
        });

        langJaBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();
            gm.gdPlayerData.Lang = Language.JA;
            gm.LoadUIText();
            langJaBtn.gameObject.GetComponent<Text>().fontStyle = FontStyle.Bold;
            langEnBtn.gameObject.GetComponent<Text>().fontStyle = FontStyle.Normal;
            langKoBtn.gameObject.GetComponent<Text>().fontStyle = FontStyle.Normal;
        });

        langEnBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();
            gm.gdPlayerData.Lang = Language.EN;
            gm.LoadUIText();
            langJaBtn.gameObject.GetComponent<Text>().fontStyle = FontStyle.Normal;
            langEnBtn.gameObject.GetComponent<Text>().fontStyle = FontStyle.Bold;
            langKoBtn.gameObject.GetComponent<Text>().fontStyle = FontStyle.Normal;
        });

        langKoBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();
            gm.gdPlayerData.Lang = Language.KO;
            gm.LoadUIText();
            langJaBtn.gameObject.GetComponent<Text>().fontStyle = FontStyle.Normal;
            langEnBtn.gameObject.GetComponent<Text>().fontStyle = FontStyle.Normal;
            langKoBtn.gameObject.GetComponent<Text>().fontStyle = FontStyle.Bold;
        });

        langBackBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();

            configArea.SetActive(true);
            langArea.SetActive(false);
        });

        // Delete savedata
        dataBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();

            saveDataArea.SetActive(true);
            configArea.SetActive(false);

            StopCoroutine(pumpCoroutine);
        });
        dataBtn.interactable = gm.IsSaveDataExist;
        dataIcon.gameObject.GetComponent<CanvasGroup>().alpha = gm.IsSaveDataExist ? 1 : 0.5f;

        delSaveDataYesBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();

            gm.DeleteData();
        });

        delSaveDataNoBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();

            configArea.SetActive(true);
            saveDataArea.SetActive(false);
        });

        // Initialize config
        modeOriginalBtn.gameObject.GetComponent<Text>().fontStyle = gm.gdPlayerData.BattleMode == false ? FontStyle.Bold : FontStyle.Normal;
        modeBattleBtn.gameObject.GetComponent<Text>().fontStyle = gm.gdPlayerData.BattleMode == true ? FontStyle.Bold : FontStyle.Normal;

        camHRotNorBtn.gameObject.GetComponent<Text>().fontStyle = gm.gdPlayerData.ActionCamHori == Reverse.Normal ? FontStyle.Bold : FontStyle.Normal;
        camHRotRevBtn.gameObject.GetComponent<Text>().fontStyle = gm.gdPlayerData.ActionCamHori == Reverse.Reverse ? FontStyle.Bold : FontStyle.Normal;
        camVRotNorBtn.gameObject.GetComponent<Text>().fontStyle = gm.gdPlayerData.ActionCamVert == Reverse.Normal ? FontStyle.Bold : FontStyle.Normal;
        camVRotRevBtn.gameObject.GetComponent<Text>().fontStyle = gm.gdPlayerData.ActionCamVert == Reverse.Reverse ? FontStyle.Bold : FontStyle.Normal;
        camShakeRateSlider.value = gm.gdPlayerData.ActionCamShakeRate;
        camShakeRateSliderCurr.text = gm.gdPlayerData.ActionCamShakeRate.ToString();

        langJaBtn.gameObject.GetComponent<Text>().fontStyle = gm.gdPlayerData.Lang == Language.JA ? FontStyle.Bold : FontStyle.Normal;
        langEnBtn.gameObject.GetComponent<Text>().fontStyle = gm.gdPlayerData.Lang == Language.EN ? FontStyle.Bold : FontStyle.Normal;
        langKoBtn.gameObject.GetComponent<Text>().fontStyle = gm.gdPlayerData.Lang == Language.KO ? FontStyle.Bold : FontStyle.Normal;
    }

    // Pump
    IEnumerator PumpStartBtn()
    {
        while (true)
        {
            yield return new WaitForSeconds(pumpInterval);

            pump = true;
        }
    }
}
