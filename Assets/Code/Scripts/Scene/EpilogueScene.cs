using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EpilogueScene : MonoBehaviour
{
    enum Page
    {
        Page1,
        Page2
    }

    const float bgImgMoveSpeed = 5f;

    [Header("Studio")]
    [SerializeField] GameObject studio;
    [SerializeField] Animator playerAnim;

    [Header("Camera images")]
    [SerializeField] RawImage page1PlayerCamImg;
    [SerializeField] RawImage page2PlayerCamImg;
    [SerializeField] Camera playerCam;

    RenderTexture playerCamRndTexture;

    [Header("UI")]
    [SerializeField] Image bgImg;
    [SerializeField] GameObject page1;
    [SerializeField] Text page1ChatText;
    [SerializeField] Text page1Sweets;
    [SerializeField] GameObject page2;
    [SerializeField] Button skipBtn;
    [SerializeField] Button nextBtn;
    [SerializeField] Button toCreditsBtn;

    Page currentPage = Page.Page1;
    Vector3 bgImgDefPos;
    float bgImgHalfWidth;

    GameManager gm;

    void Awake()
    {
        gm = GameManager.Instance;

        bgImgDefPos = bgImg.transform.position;
        bgImgHalfWidth = bgImg.rectTransform.rect.width / 2;

        // UI
        skipBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();
            gm.LoadScene("Credits");
        });

        nextBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();

            switch (currentPage)
            {
                case Page.Page1:
                    // Studio
                    playerAnim.SetBool("WipeMouth", false);
                    playerAnim.SetBool("CrossArms", true);

                    // UI
                    skipBtn.gameObject.SetActive(false);
                    nextBtn.gameObject.SetActive(false);
                    toCreditsBtn.gameObject.SetActive(true);

                    page1.SetActive(false);
                    page2.SetActive(true);
                    currentPage = Page.Page2;
                    break;
                default:
                    break;
            }
        });

        toCreditsBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();
            gm.LoadScene("Credits");
        });

        gm.SetCamera(playerCam, playerCamRndTexture, page1PlayerCamImg, page2PlayerCamImg);
    }

    void Start()
    {
        gm.LoadUIText();
        EatSweets();
    }

    void EatSweets()
    {
        if (gm.gdPlayerData.PlayerSweets.Count == 0)
            page1ChatText.text = "...";
        else
        {
            List<string> sweetNames = new List<string>();

            foreach (SweetMst sMst in gm.gdPlayerData.PlayerSweets.Keys)
            {
                string nm = gm.dbHandler.LocMstDic[sMst.LocMStId][gm.gdPlayerData.Lang];
                int cnt = gm.gdPlayerData.PlayerSweets[sMst];

                sweetNames.Add($"{nm}x{cnt}");
            }

            gm.gdPlayerData.ConsumeSweets();

            page1Sweets.text = string.Join(" ", sweetNames);
            playerAnim.SetBool("WipeMouth", true);
        }
    }

    void Update()
    {
        // Sky, skyRoof
        bgImg.gameObject.SetActive(true);
        bgImg.gameObject.SetActive(true);

        if (bgImg.rectTransform.offsetMin.x < -bgImgHalfWidth)
            bgImg.transform.position = bgImgDefPos;
        else
            bgImg.transform.Translate(Vector3.left * bgImgMoveSpeed * Time.deltaTime);
    }

    void OnDestroy()
    {
        if (playerCamRndTexture != null)
            playerCamRndTexture.Release();
    }
}