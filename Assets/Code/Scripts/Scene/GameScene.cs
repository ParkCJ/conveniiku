using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class GameScene : MonoBehaviour
{
    [Header("UI")]
    [SerializeField] Text heartText;
    [SerializeField] Text coinText;
    [SerializeField] Text scoreText;
    [SerializeField] Text blockText;
    [SerializeField] GameObject attackArea;
    [SerializeField] Text bombText;

    [SerializeField] Image centerMark;

    [SerializeField] GameObject secondCamArea;
    [SerializeField] RawImage secondCamImg;
    [SerializeField] Button camChangeBtn;
    [SerializeField] Text fpsCamNameText;
    [SerializeField] Text freeCamNameText;
    [SerializeField] Text tpsCamNameText;
    [SerializeField] Text followCamNameText;
    [SerializeField] Text topCamNameText;
    [SerializeField] Text continueText;
    [SerializeField] Text successText;
    [SerializeField] Text countText;
    [SerializeField] Button continueBtn;
    [SerializeField] GameObject ContinueBtnPriceInfo;
    [SerializeField] Button titleBtn;
    [SerializeField] Button myStageBtn;
    [SerializeField] Button tourBtn;
    [SerializeField] Button nextBtn;
    [SerializeField] Button shopBtn;
    [SerializeField] Text pausedText;
    [SerializeField] Button resumeBtn;
    [SerializeField] Button pausedTitleBtn;
    [SerializeField] Button pausedMyStageBtn;
    [SerializeField] Button pausedTourBtn;

    public GameObject storeGameCtrlPrefab;
    GameManager gm;

    Coroutine gameOverCountdownCoroutine;

    int toTitleCountDown = 10;

    void Awake()
    {
        gm = GameManager.Instance;

        // UI
        continueBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();

            continueText.gameObject.SetActive(false);
            continueBtn.gameObject.SetActive(false);
            titleBtn.gameObject.SetActive(false);
            myStageBtn.gameObject.SetActive(false);
            tourBtn.gameObject.SetActive(false);
            countText.gameObject.SetActive(false);

            StopCoroutine(gameOverCountdownCoroutine);

            gm.gameCtrl.AddMoney(-gm.dbHandler.DefDataMst.ContinueMoneyCnt);
            gm.gameCtrl.Continue();
            StartCoroutine(UpdateUI());
        });

        titleBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();
            gm.InitPlayerData(true, true);

            Cursor.lockState = CursorLockMode.None;
            gm.LoadScene("Title");
        });

        tourBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();
            gm.InitPlayerData(true, true);

            Cursor.lockState = CursorLockMode.None;
            gm.LoadScene("Tour");
        });

        myStageBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();
            gm.InitPlayerData(true, true);

            Cursor.lockState = CursorLockMode.None;
            gm.LoadScene("MyStage");
        });

        nextBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();
            if (gm.gdStageMst.Idx == 107)
                gm.InitPlayerData(true);

            Cursor.lockState = CursorLockMode.None;
            gm.LoadScene("GameNext");
        });

        shopBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();
            gm.InitPlayerData(true);

            Cursor.lockState = CursorLockMode.None;
            gm.LoadScene("Shop");
        });

        resumeBtn.onClick.AddListener(() =>
        {
            AudioListener.pause = false;

            AudioSource audio = resumeBtn.GetComponentInParent<AudioSource>();
            audio.Play();

            pausedText.gameObject.SetActive(false);
            resumeBtn.gameObject.SetActive(false);
            pausedTitleBtn.gameObject.SetActive(false);
            pausedMyStageBtn.gameObject.SetActive(false);
            pausedTourBtn.gameObject.SetActive(false);
            camChangeBtn.interactable = true;
            Time.timeScale = 1;

            gm.gameCtrl.SetCam();
        });

        pausedTitleBtn.onClick.AddListener(() =>
        {
            AudioListener.pause = false;

            AudioSource audio = resumeBtn.GetComponentInParent<AudioSource>();
            audio.Play();

            Time.timeScale = 1;
            
            gm.InitPlayerData(true, true);

            Cursor.lockState = CursorLockMode.None;
            gm.LoadScene("Title");
        });

        pausedTourBtn.onClick.AddListener(() =>
        {
            AudioListener.pause = false;

            AudioSource audio = resumeBtn.GetComponentInParent<AudioSource>();
            audio.Play();

            Time.timeScale = 1;

            gm.InitPlayerData(true, true);

            Cursor.lockState = CursorLockMode.None;
            gm.LoadScene("Tour");
        });

        pausedMyStageBtn.onClick.AddListener(() =>
        {
            AudioListener.pause = false;

            AudioSource audio = resumeBtn.GetComponentInParent<AudioSource>();
            audio.Play();

            Time.timeScale = 1;
            
            gm.InitPlayerData(true, true);

            Cursor.lockState = CursorLockMode.None;
            gm.LoadScene("MyStage");
        });
    }

    void Start()
    {
        Instantiate(storeGameCtrlPrefab).GetComponent<StoreGameCtrl>();

        gm.LoadUIText();

        secondCamArea.gameObject.SetActive(true);
        secondCamImg.texture = gm.gameCtrl.SecondCamRndTexture;

        camChangeBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();

            if (fpsCamNameText.IsActive())
                ChangeMainCam(CamType.FreeLook);
            else if (freeCamNameText.IsActive())
                ChangeMainCam(CamType.ThirdPerson);
            else if (tpsCamNameText.IsActive())
                ChangeMainCam(CamType.TopFollow);
            else if (followCamNameText.IsActive())
                ChangeMainCam(CamType.Top);
            else
                ChangeMainCam(CamType.FirstPerson);

            EventSystem.current.SetSelectedGameObject(null);
        });

        StartCoroutine(UpdateUI());
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.P))
        {
            Cursor.lockState = CursorLockMode.None;

            if (gm.gameCtrl != null && gm.gameCtrl.gameProgress == GameProgress.Play)
            {
                pausedText.gameObject.SetActive(true);
                resumeBtn.gameObject.SetActive(true);
                pausedTitleBtn.gameObject.SetActive(gm.gameMode == GameMode.Arcade);
                pausedMyStageBtn.gameObject.SetActive(gm.gameMode == GameMode.MyStage);
                pausedTourBtn.gameObject.SetActive(gm.gameMode == GameMode.Tour);
                camChangeBtn.interactable = false;
                AudioListener.pause = true;
                Time.timeScale = 0;
            }
        }
        else if (camChangeBtn.interactable)
        {
            if (Input.GetKey(KeyCode.Alpha1))
                ChangeMainCam(CamType.FirstPerson);
            else if (Input.GetKey(KeyCode.Alpha2))
                ChangeMainCam(CamType.FreeLook);
            else if (Input.GetKey(KeyCode.Alpha3))
                ChangeMainCam(CamType.ThirdPerson);
            else if (Input.GetKey(KeyCode.Alpha4))
                ChangeMainCam(CamType.TopFollow);
            else if (Input.GetKey(KeyCode.Alpha5))
                ChangeMainCam(CamType.Top);
        }
    }

    IEnumerator UpdateUI()
    {
        attackArea.SetActive(gm.gdPlayerData.BattleMode);

        while (gm.gameCtrl.gameProgress == GameProgress.Play)
        {
            UpdatePlayerInfo();
            yield return new WaitForEndOfFrame();
        }

        UpdatePlayerInfo();

        Cursor.lockState = CursorLockMode.None;

        if (gm.gameCtrl.gameProgress == GameProgress.Clear)
        {
            successText.gameObject.SetActive(true);
            nextBtn.gameObject.SetActive(gm.gameMode == GameMode.Arcade);
            shopBtn.gameObject.SetActive(gm.gameMode != GameMode.Arcade);
        }
        else
        {
            continueText.gameObject.SetActive(true);
            continueBtn.gameObject.SetActive(true);
            titleBtn.gameObject.SetActive(gm.gameMode == GameMode.Arcade);
            myStageBtn.gameObject.SetActive(gm.gameMode == GameMode.MyStage);
            tourBtn.gameObject.SetActive(gm.gameMode == GameMode.Tour);
            countText.gameObject.SetActive(true);

            gameOverCountdownCoroutine = StartCoroutine(GameOverCountdown());

            if (gm.gdPlayerData.Money >= gm.dbHandler.DefDataMst.ContinueMoneyCnt)
            {
                continueBtn.interactable = true;
                ContinueBtnPriceInfo.GetComponent<CanvasGroup>().alpha = 1;
            }
            else
            {
                continueBtn.interactable = false;
                ContinueBtnPriceInfo.GetComponent<CanvasGroup>().alpha = 0.5f;
            }
        }
    }

    void UpdatePlayerInfo()
    {
        // UI
        heartText.text = gm.gdPlayerData.Life.ToString();
        coinText.text = gm.gdPlayerData.Money.ToString();
        scoreText.text = gm.gdPlayerData.Score.ToString();
        blockText.text = gm.gdPlayerData.Block.ToString();
        bombText.text = gm.gdPlayerData.Bomb.ToString();
        //tempCubeCnt.text = gameCtrl.TempCubeCnt.ToString();
        //rewardCoinCnt.text = gameCtrl.TempRewardCoinCnt.ToString();
        //atkState.alpha = gameCtrl.PlayerCtrl != null && !gameCtrl.PlayerCtrl.IsAttacking ? 1 : 0.5f;
        //spAtkState.alpha = gameCtrl.PlayerCtrl != null && gameCtrl.PlayerCtrl.SPAttackEnabled && !gameCtrl.PlayerCtrl.IsSPAttacking ? 1 : 0.5f;
        //sp2AtkState.alpha = gameCtrl.PlayerCtrl != null && gameCtrl.PlayerCtrl.SPAttack2Enabled && !gameCtrl.PlayerCtrl.IsSP2Attacking ? 1 : 0.5f;

        // CAM
        fpsCamNameText.gameObject.SetActive(gm.gdPlayerData.camType == CamType.FirstPerson);
        centerMark.gameObject.SetActive(gm.gdPlayerData.camType == CamType.FirstPerson);
        freeCamNameText.gameObject.SetActive(gm.gdPlayerData.camType == CamType.FreeLook);
        tpsCamNameText.gameObject.SetActive(gm.gdPlayerData.camType == CamType.ThirdPerson);
        followCamNameText.gameObject.SetActive(gm.gdPlayerData.camType == CamType.TopFollow);
        topCamNameText.gameObject.SetActive(gm.gdPlayerData.camType == CamType.Top);
    }

    IEnumerator GameOverCountdown()
    {
        int timeLeft = toTitleCountDown;

        while (timeLeft >= 0)
        {
            countText.text = timeLeft.ToString();
            timeLeft--;

            yield return new WaitForSeconds(1);
        }

        gm.InitPlayerData(true, true);

        if (gm.gameMode == GameMode.Arcade)
        {
            Cursor.lockState = CursorLockMode.None;
            gm.LoadScene("Title");
        }
        else if (gm.gameMode == GameMode.MyStage)
        {
            Cursor.lockState = CursorLockMode.None;
            gm.LoadScene("MyStage");
        }
        else
        {
            Cursor.lockState = CursorLockMode.None;
            gm.LoadScene("Tour");
        }
    }

    void ChangeMainCam(CamType mCamType)
    {
        if (gm.gameCtrl != null)// && gm.gameCtrl.gameProgress == GameProgress.Play)
        {
            // Prevent cam change while SkillD
            if (GameObject.Find("SkillD(Clone)") != null)
                return;

            gm.gdPlayerData.camType = mCamType;
            gm.gameCtrl.SetCam();
        }
    }
}