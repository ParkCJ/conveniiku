using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class GameNextScene : MonoBehaviour
{
    [Header("Island")]
    [SerializeField] Island island;

    [Header("UI")]
    [SerializeField] Text stageTitle;
    [SerializeField] Button skipBtn;

    GameManager gm;
    bool isMoving;

    void Awake()
    {
        gm = GameManager.Instance;
    }

    void Start()
    {
        gm.LoadUIText();

        skipBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();

            StopAllCoroutines();

            if (!isMoving)
                gm.gdStageMst = gm.dbHandler.StageMstDic[gm.gdStageMst.Next];

            if (gm.gdStageMst.Name == "Shop")
                gm.LoadScene("Shop");
            else
                gm.LoadScene("Game");
        });

        //StartCoroutine(DollyTrackTest());
        StartCoroutine(Move());
    }

    IEnumerator Move()
    {
        // Current stage
        island.CreateEnvironment(gm.gdStageMst.FourTime);
        if (gm.gdStageMst.Idx != 100)
            island.SelectStage(gm.gdStageMst.Idx);

        int locMStId = gm.dbHandler.StageMstDic[gm.gdStageMst.Idx].LocMStId;
        stageTitle.text = gm.dbHandler.LocMstDic[locMStId][gm.gdPlayerData.Lang];

        // Delay
        yield return new WaitForSeconds(1.5f);

        // Move player to next stage
        gm.gdStageMst = gm.dbHandler.StageMstDic[gm.gdStageMst.Next];
        isMoving = true;
        yield return island.MovePlayer(gm.gdStageMst.Idx);

        island.CreateEnvironment(gm.gdStageMst.FourTime);

        locMStId = gm.dbHandler.StageMstDic[gm.gdStageMst.Idx].LocMStId;
        stageTitle.text = gm.dbHandler.LocMstDic[locMStId][gm.gdPlayerData.Lang];

        yield return new WaitForSeconds(1.5f);

        // Load next stage
        if (gm.gdStageMst.Name == "Shop")
        {
            gm.LoadScene("Shop");
        }
        else
        {
            gm.LoadScene("Game");
        }
    }

    IEnumerator DollyTrackTest()
    {
        while(true)
        {
            // Current stage
            island.SelectStage(106);

            // Delay
            yield return new WaitForSeconds(1);
            yield return island.MovePlayer(108);
            yield return new WaitForSeconds(1);
        }
    }
}
