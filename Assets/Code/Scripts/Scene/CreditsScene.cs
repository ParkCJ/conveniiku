using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class CreditsScene : MonoBehaviour
{
    // Studio
    const float treesMoveSpeed = 150;
    const float skyMoveSpeed = 2f;
    const float groundMoveSpeed = 10;

    // Credits
    const float creditsAppearDefDur = 1;
    const float creditsDisappearDefDur = 1;
    const float creditsShowDur = 4f;

    // Fade out
    const float fadeOutCamRotXMin = 310;
    const float fadeOutCamRotSpeed = 10;

    const float fadeOutKiteDelay = 2.5f;
    const float fadeOutKiteSpeed = 10;
    const float fadeOutKitePosXMax = 50;
    const float fadeOutKiteSpeedPlus = 0.1f;

    const float dragonSpeed = 10;

    const float fadeOutCurtainDelay = 2.5f;
    const float fadeOutCurtainAlphaPlus = 0.75f;

    // Game over
    const float gameOverMsgDelay = 0.5f;

    [Header("Studio")]
    [SerializeField] GameObject studio;
    [SerializeField] GameObject trees;
    [SerializeField] GameObject sky;
    [SerializeField] GameObject skyRoof;
    [SerializeField] GameObject ground;

    [Header("Creatures")]
    [SerializeField] GameObject kite;
    [SerializeField] GameObject chick;
    [SerializeField] GameObject player;
    [SerializeField] GameObject cow;
    [SerializeField] GameObject fox;
    [SerializeField] GameObject dragon;

    [Header("UI")]
    [SerializeField] GameObject[] creditsList;
    [SerializeField] Image fadeOutCurtain;
    [SerializeField] GameObject gameOverMsg;
    [SerializeField] Button skipBtn;
    [SerializeField] Button tourBtn;
    [SerializeField] Button toTitleBtn;

    [Header("Environment")]
    [SerializeField] Sprite[] skyMoriningSprites;
    [SerializeField] Sprite[] skyAfternoonSprites;
    [SerializeField] Sprite[] skyEveningSprites;

    [SerializeField] GameObject[] cloudPrefabs;

    [SerializeField] GameObject afternoonEnvironment;
    [SerializeField] GameObject nightEnvironment;

    AudioSource creditsAudio;

    Vector3 treesStartPos;
    float treesDoubleWidth;

    Vector3 skyStartPos;
    float skyHalfWidth;

    Vector3 skyRoofStartPos;
    float skyRoofHalfWidth;

    Vector3 groundStartPos;
    float groundQuarterWidth;

    int currCreditsIdx;

    FourTime fourTime;
    Dictionary<GameObject, float> clouds;

    GameManager gm;

    void Awake()
    {
        gm = GameManager.Instance;

        creditsAudio = Camera.main.GetComponent<AudioSource>();

        treesStartPos = trees.transform.position;
        treesDoubleWidth = trees.transform.localScale.x * 2;

        skyStartPos = sky.transform.position;
        skyHalfWidth = sky.transform.GetComponent<BoxCollider>().size.x * sky.transform.localScale.x / 2;

        skyRoofStartPos = skyRoof.transform.position;
        skyRoofHalfWidth = skyRoof.transform.GetComponent<BoxCollider>().size.x * skyRoof.transform.localScale.x / 2;

        groundStartPos = ground.transform.position;
        groundQuarterWidth = ground.transform.localScale.x / 4;

        // Environment
        Light dirLight = GameObject.Find("Directional Light").GetComponent<Light>();
        dirLight.gameObject.transform.rotation = Quaternion.Euler(new Vector3(TimeHandler.GetSunXRot(fourTime), -30, 0));

        //Volume ppVol = GameObject.Find("Global Volume").GetComponent<Volume>();
        //Bloom ppBloom;
        //ppVol.profile.TryGet(out ppBloom);

        clouds = new Dictionary<GameObject, float>();
        for (int i = 0; i < 150; i++) // cloud
        {
            // 1. Instantiate
            Vector3 pos = new Vector3(Random.Range(-265, 60), Random.Range(15, 150), Random.Range(-50, 15));
            GameObject cloud = Instantiate(cloudPrefabs[Random.Range(0, cloudPrefabs.Length)], pos, Quaternion.Euler(0, 90, 0), transform);
            cloud.transform.localScale *= Random.Range(1, 5);
            clouds.Add(cloud, skyMoveSpeed + 1.5f);
        }

        // FourTime
        if (gm.gameMode == GameMode.Arcade)
            fourTime = FourTime.Morning;
        else if (gm.gameMode == GameMode.Tour)
            fourTime = gm.gdPlayerData.LastFourTime;
        else
            fourTime = gm.gdStageMst.FourTime;
        
        Sprite sprite;
        switch (fourTime)
        {
            case FourTime.Morning:
                dirLight.color = new Color32(255, 244, 214, 255);
                dirLight.intensity = 1f;
                //ppBloom.intensity.value = 0.25f;

                sprite = skyMoriningSprites[Random.Range(0, skyMoriningSprites.Length)];
                sky.GetComponent<SpriteRenderer>().sprite = sprite;
                skyRoof.GetComponent<SpriteRenderer>().sprite = sprite;
                break;
            case FourTime.Afternoon:
                RenderSettings.ambientIntensity = 1.05f;
                dirLight.color = new Color32(255, 244, 214, 255);
                dirLight.intensity = 1f;
                //ppBloom.intensity.value = 0.5f;

                sprite = skyAfternoonSprites[Random.Range(0, skyAfternoonSprites.Length)];
                sky.GetComponent<SpriteRenderer>().sprite = sprite;
                skyRoof.GetComponent<SpriteRenderer>().sprite = sprite;

                // Environment
                afternoonEnvironment.SetActive(true);
                break;
            case FourTime.Evening:
                RenderSettings.ambientMode = AmbientMode.Flat;
                RenderSettings.ambientLight = new Color32(143, 102, 90, 255);
                dirLight.color = new Color32(255, 244, 214, 255);
                dirLight.intensity = 1f;
                //ppBloom.intensity.value = 0.75f;

                sprite = skyEveningSprites[Random.Range(0, skyEveningSprites.Length)];
                sky.GetComponent<SpriteRenderer>().sprite = sprite;
                skyRoof.GetComponent<SpriteRenderer>().sprite = sprite;
                break;
            case FourTime.Night:
                dirLight.color = new Color32(244, 175, 3, 255);
                dirLight.intensity = 0.175f;
                //ppBloom.intensity.value = 1f;

                // Environment
                sky.SetActive(false);
                skyRoof.SetActive(false);
                nightEnvironment.SetActive(true);
                break;
        }

        // UI
        skipBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();
            ShowGameOverMsg();
        });
        skipBtn.gameObject.SetActive(gm.gameMode != GameMode.Tour);

        tourBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();
            gm.LoadScene("Tour");
        });
        tourBtn.gameObject.SetActive(gm.gameMode == GameMode.Tour);

        toTitleBtn.onClick.AddListener(() =>
        {
            gm.PlayBtnClickSound();
            gm.SaveData();
            gm.LoadScene("Title");
        });

        // Start player animation
        player.GetComponent<Animator>().SetFloat("Speed_f", 1);

        //Time.timeScale = 5;
    }

    void Start()
    {
        gm.LoadUIText();

        // Play ending theme
        creditsAudio.Play();
    }

    void Update()
    {
        if (!gameOverMsg.activeSelf)
        {
            // Trees
            if (trees != null)
            {
                trees.transform.Translate(Vector3.right * treesMoveSpeed * Time.deltaTime);
                if (trees.transform.position.x > treesStartPos.x + treesDoubleWidth)
                {
                    Destroy(trees);

                    // Show credits
                    StartCoroutine(ShowCredits());
                }
            }

            // Sky
            if (fourTime != FourTime.Night)
            {
                // Sky, skyRoof
                sky.SetActive(true);
                skyRoof.SetActive(true);

                sky.transform.Translate(Vector3.right * skyMoveSpeed * Time.deltaTime);
                if (sky.transform.position.x > skyStartPos.x + skyHalfWidth)
                {
                    sky.transform.position = skyStartPos;
                }
                skyRoof.transform.Translate(Vector3.right * skyMoveSpeed * Time.deltaTime);
                if (skyRoof.transform.position.x > skyRoofStartPos.x + skyRoofHalfWidth)
                {
                    skyRoof.transform.position = skyRoofStartPos;
                }
            }

            // Cloud
            foreach(GameObject cloud in clouds.Keys)
            {
                cloud.transform.Translate(Vector3.right * clouds[cloud] * Time.deltaTime, Space.World);
            }

            // Environment
            afternoonEnvironment.transform.Translate(Vector3.right * Time.deltaTime);

            // Ground
            ground.transform.Translate(Vector3.right * groundMoveSpeed * Time.deltaTime);
            if (ground.transform.position.x > groundStartPos.x + groundQuarterWidth)
            {
                ground.transform.position = groundStartPos;
            }

            // Dragon
            if (dragon != null)
                dragon.transform.Translate(transform.forward * dragonSpeed * Time.deltaTime);
        }
    }

    IEnumerator ShowCredits()
    {
        GameObject credits = creditsList[currCreditsIdx];
        float appearDur = creditsAppearDefDur;
        float disappearDur = creditsDisappearDefDur;

        CanvasGroup canvasGroup = credits.GetComponent<CanvasGroup>();

        // Start fade out if it is the last credits
        if (currCreditsIdx == creditsList.Length - 1)
        {
            StartCoroutine(ActivateFadeOutCam());
            StartCoroutine(ActivateFadeOutKite());

            Destroy(dragon);
        }

        // Appear
        while (appearDur > 0)
        {
            canvasGroup.alpha += Time.deltaTime;

            yield return new WaitForEndOfFrame();
            appearDur -= Time.deltaTime;
        }

        // Show
        yield return new WaitForSeconds(creditsShowDur);

        // Disappear
        while (disappearDur > 0)
        {
            canvasGroup.alpha -= Time.deltaTime;

            yield return new WaitForEndOfFrame();
            disappearDur -= Time.deltaTime;
        }

        credits.SetActive(false);

        // Show next credits
        if (currCreditsIdx < creditsList.Length - 1)
        {
            currCreditsIdx++;
            StartCoroutine(ShowCredits());
        }
    }

    IEnumerator ActivateFadeOutCam()
    {
        while (Camera.main.transform.eulerAngles.x > fadeOutCamRotXMin)
        {
            Camera.main.transform.Rotate(-Vector3.right * fadeOutCamRotSpeed * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }
    }

    IEnumerator ActivateFadeOutKite()
    {
        yield return new WaitForSeconds(fadeOutKiteDelay);

        StartCoroutine(ActivateFadeOutCurtain());

        // Disappear
        float speed = fadeOutKiteSpeed;
        while (kite.transform.position.x < fadeOutKitePosXMax)
        {
            kite.transform.Translate(-transform.forward * speed * Time.deltaTime);
            kite.transform.Translate(transform.up * speed * Time.deltaTime);
            yield return new WaitForEndOfFrame();

            speed += fadeOutKiteSpeedPlus;
        }
    }

    IEnumerator ActivateFadeOutCurtain()
    {
        yield return new WaitForSeconds(fadeOutCurtainDelay);

        // UI
        skipBtn.gameObject.SetActive(false);
        tourBtn.gameObject.SetActive(false);

        // Activate curtain
        while (fadeOutCurtain.color.a < 1)
        {
            Color tmpColor = fadeOutCurtain.color;
            tmpColor.a += fadeOutCurtainAlphaPlus * Time.deltaTime;
            fadeOutCurtain.color = tmpColor;

            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(gameOverMsgDelay);

        if (gm.gameMode == GameMode.Tour)
            gm.LoadScene("Tour");
        else
            ShowGameOverMsg();
    }

    void ShowGameOverMsg()
    {
        // Stop coroutine
        StopAllCoroutines();

        // Show fadeout curtain
        Color tmpColor = fadeOutCurtain.color;
        tmpColor.a = 1;
        fadeOutCurtain.color = tmpColor;

        // Hide createdBy credits
        creditsList[creditsList.Length - 1].GetComponent<CanvasGroup>().alpha = 0;

        // Show game over msg
        gameOverMsg.SetActive(true);

        // Destroy studio
        Destroy(studio);

        // UI
        skipBtn.gameObject.SetActive(false);
        tourBtn.gameObject.SetActive(false);
        toTitleBtn.gameObject.SetActive(true);
    }
}