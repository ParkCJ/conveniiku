using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [Header("UI Fonts")]
    [SerializeField] Font jaDefaultFont;
    [SerializeField] Font enDefaultFont;
    [SerializeField] Font koDefaultFont;
    [SerializeField] Font jaFont;
    [SerializeField] Font enFont;
    [SerializeField] Font koFont;

    [Header("Audio")]
    [SerializeField] AudioClip btnClickSound;

    [Header("Loading")]
    [SerializeField] Image loadingImg;

    // Permanent data
    public DbHandler dbHandler;
    public PlayerData PlayerData { get; private set; }

    // Game data
    public StageMst gdStageMst; // <-- FourTime은 여기서 수정
    public PlayerData gdPlayerData;
    public GameMode gameMode;

    // Else
    public static GameManager Instance;
    public string beforeScene = null;
    public bool IsSaveDataExist;
    public StoreGameCtrl gameCtrl;
    bool isLoadingScene = false;

    void Awake()
    {
        // DontDestroyOnLoad
        if (GameManager.Instance != null)
        {
            Destroy(gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(gameObject);

        // Permanent data
        dbHandler = new DbHandler(jaDefaultFont, enDefaultFont, koDefaultFont, jaFont, enFont, koFont);
        LoadSaveData();
    }

    // Savedata
    public void LoadSaveData()
    {
        if (File.Exists(dbHandler.DefDataMst.SaveDataPath))
        {
            string json = File.ReadAllText(dbHandler.DefDataMst.SaveDataPath);
            PlayerData = JsonUtility.FromJson<PlayerData>(json);
            gdPlayerData = JsonUtility.FromJson<PlayerData>(json);

            IsSaveDataExist = true;
        }
        else
        {
            PlayerData = new PlayerData(dbHandler.DefDataMst);
            gdPlayerData = new PlayerData(dbHandler.DefDataMst);

            IsSaveDataExist = false;
        }
    }

    public void SaveData()
    {
        gdPlayerData.PowerUp = 1.0f;

        string json = JsonUtility.ToJson(gdPlayerData);
        File.WriteAllText(dbHandler.DefDataMst.SaveDataPath, json);

        LoadSaveData();
    }

    public void DeleteData()
    {
        File.Delete(dbHandler.DefDataMst.SaveDataPath);
        Destroy(gameObject);
        SceneManager.LoadScene("Title");
    }

    public void InitPlayerData(bool clearAll = false, bool includeMoney = false)
    {
        gdPlayerData.SpeedGr = PlayerData.SpeedGr;
        gdPlayerData.SpeedAir = PlayerData.SpeedAir;
        gdPlayerData.JumpForce = PlayerData.JumpForce;
        gdPlayerData.JumpInterval = PlayerData.JumpInterval;
        gdPlayerData.PushForce = PlayerData.PushForce;
        gdPlayerData.PushInterval = PlayerData.PushInterval;
        gdPlayerData.AttackInterval = PlayerData.AttackInterval;
        gdPlayerData.BombInterval = PlayerData.BombInterval;
        gdPlayerData.PowerUp = PlayerData.PowerUp;

        if (clearAll)
        {
            gdPlayerData.Life = PlayerData.Life;
            gdPlayerData.Bomb = PlayerData.Bomb;
        }

        if (includeMoney)
        {
            gdPlayerData.Money = Mathf.Min(gdPlayerData.Money, PlayerData.Money);
            gdPlayerData.Score = PlayerData.Score;
            gdPlayerData.Block = PlayerData.Block;
        }
    }

    // Common
    public void LoadScene(string sceneName)
    {
        beforeScene = SceneManager.GetActiveScene().name;
        StartCoroutine(LoadSceneAsync(sceneName));
    }

    IEnumerator LoadSceneAsync(string sceneName)
    {
        if (isLoadingScene)
            yield break;

        isLoadingScene = true;

        loadingImg.gameObject.SetActive(true);

        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName);

        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone)
        {
            yield return null;
        }

        loadingImg.gameObject.SetActive(false);

        isLoadingScene = false;
    }

    public void LoadUIText()
    {
        Text[] textCompArray = FindObjectsOfType<Text>(true);

        foreach (Text textComp in textCompArray)
        {
            if (dbHandler.UITextDic[SceneManager.GetActiveScene().name].ContainsKey(textComp.gameObject.name))
            {
                UITextMst UITextSet = dbHandler.UITextDic[SceneManager.GetActiveScene().name][textComp.gameObject.name][gdPlayerData.Lang];

                textComp.text = UITextSet.Text;
                textComp.font = UITextSet.Font;
            }
        }
    }

    public void SetCamera(Camera camera, RenderTexture rndTexture, params RawImage[] cameraImgs)
    {
        rndTexture = new RenderTexture(256, 256, 24);
        rndTexture.name = System.Guid.NewGuid().ToString();
        rndTexture.Create();
        camera.targetTexture = rndTexture;

        foreach (RawImage img in cameraImgs)
            img.texture = rndTexture;
    }

    public void PlayBtnClickSound()
    {
        AudioSource.PlayClipAtPoint(btnClickSound, Camera.main.transform.position, 0.15f);
    }
}
