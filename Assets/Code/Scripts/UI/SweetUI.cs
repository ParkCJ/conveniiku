using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SweetUI : MonoBehaviour
{
    [Header("UI")]
    [SerializeField] Text sweetName;
    [SerializeField] Text sweetDesc;
    [SerializeField] Text sweetStatus;
    [SerializeField] Text sweetPrice;
    [SerializeField] Button sweetBuyBtn;
    [SerializeField] Button refundBtn;

    public SweetMst SweetMst { get; set; }

    GameManager gm;
    ShopScene shopScene;

    void Awake()
    {
        gm = GameManager.Instance;
        shopScene = GameObject.Find("ShopScene").GetComponent<ShopScene>();
    }

    void Start()
    {
        UpdateStatus();
        sweetBuyBtn.onClick.AddListener(() =>
        {
            gm.gdPlayerData.BuySweet(SweetMst);
            gm.PlayBtnClickSound();

            shopScene.Buy();
        });
        refundBtn.onClick.AddListener(() =>
        {
            gm.gdPlayerData.RefundSweet(SweetMst);
            gm.PlayBtnClickSound();

            shopScene.Buy();
        });
    }

    public void UpdateStatus()
    {
        sweetName.text = gm.dbHandler.LocMstDic[SweetMst.LocMStId][gm.gdPlayerData.Lang];
        if (gm.dbHandler.LocMstDic.ContainsKey(SweetMst.LocMStId + 1))
            sweetDesc.text = gm.dbHandler.LocMstDic[SweetMst.LocMStId + 1][gm.gdPlayerData.Lang];

        sweetPrice.text = SweetMst.CoinCnt.ToString();

        if (gm.gdPlayerData.Money >= SweetMst.CoinCnt)
            sweetBuyBtn.interactable = true;
        else
            sweetBuyBtn.interactable = false;

        if (gm.gdPlayerData.PlayerSweets.ContainsKey(SweetMst))
            sweetStatus.text = $"{gm.gdPlayerData.PlayerSweets[SweetMst]}->{gm.gdPlayerData.PlayerSweets[SweetMst] + 1}";
        else
            sweetStatus.text = $"0->1";

        switch (SweetMst.Ability)
        {
            case Ability.None:
                refundBtn.interactable = gm.gdPlayerData.PlayerSweets.ContainsKey(SweetMst) && gm.gdPlayerData.PlayerSweets[SweetMst] > 0;
                break;
            case Ability.Life:
                sweetStatus.text = $"{gm.gdPlayerData.Life.ToString("F1")}->{(gm.gdPlayerData.Life + 1f).ToString("F1")}";
                refundBtn.interactable = gm.gdPlayerData.Life > gm.dbHandler.DefDataMst.DefLifeCnt;
                break;
            case Ability.Jump:
                sweetStatus.text = $"{gm.gdPlayerData.JumpForce.ToString("F1")}->{(gm.gdPlayerData.JumpForce + 1f).ToString("F1")}";
                refundBtn.interactable = gm.gdPlayerData.JumpForce > gm.dbHandler.DefDataMst.DefJumpForce;
                break;
            case Ability.Speed:
                sweetStatus.text = $"{gm.gdPlayerData.SpeedGr.ToString("F1")}->{(gm.gdPlayerData.SpeedGr + 0.1f).ToString("F1")}";
                refundBtn.interactable = gm.gdPlayerData.SpeedGr > gm.dbHandler.DefDataMst.DefSpeedGr;
                break;
            case Ability.Bomb:
                sweetStatus.text = $"{gm.gdPlayerData.Bomb.ToString()}->{(gm.gdPlayerData.Bomb + 1).ToString()}";
                refundBtn.interactable = gm.gdPlayerData.Bomb > gm.dbHandler.DefDataMst.DefBombCnt;
                break;
            case Ability.MoveAir:
                sweetStatus.text = gm.gdPlayerData.MoveAir ? "O" : "X->O";
                if (gm.gdPlayerData.MoveAir)
                    sweetBuyBtn.interactable = false;
                refundBtn.interactable = gm.gdPlayerData.MoveAir;
                break;
            case Ability.MyStage:
                sweetStatus.text = gm.gdPlayerData.MyStageUnlocked ? "O" : "X->O";
                if (gm.gdPlayerData.MyStageUnlocked)
                    sweetBuyBtn.interactable = false;
                refundBtn.gameObject.SetActive(false);
                break;
        }

    }
}