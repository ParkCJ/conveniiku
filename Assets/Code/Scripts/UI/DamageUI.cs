using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DamageUI : MonoBehaviour
{
    const float upSpeed = 1f;
    const float alphaChange = 0.1f;
    const float showTime = 0.5f;

    public Canvas cvs;
    public TextMeshProUGUI damageText;
    public CanvasGroup cvsGrp;

    public void SetText(float damage)
    {
        damageText.text = Mathf.Abs(damage).ToString();
    }

    void Start()
    {
        cvs.worldCamera = Camera.main;
        StartCoroutine(ShowUI());
    }

    void LateUpdate()
    {
        transform.LookAt(transform.position + Camera.main.transform.forward);
    }

    IEnumerator ShowUI()
    {
        while (cvsGrp.alpha < 1f)
        {
            cvsGrp.alpha += alphaChange;
            transform.Translate(transform.up * upSpeed * Time.deltaTime);

            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(showTime);

        while (!Mathf.Approximately(cvsGrp.alpha, 0))
        {
            cvsGrp.alpha -= alphaChange;
            transform.Translate(transform.up * upSpeed * Time.deltaTime);

            yield return new WaitForEndOfFrame();
        }

        Destroy(gameObject);
    }
}