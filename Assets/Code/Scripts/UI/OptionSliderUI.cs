using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionSliderUI : MonoBehaviour
{
    [SerializeField] Text optionTitle;
    [SerializeField] Text optionMin;
    [SerializeField] Text optionMax;
    [SerializeField] Slider optionSlider;
    [SerializeField] Text optionCurr;

    bool playSound = true;

    public int Value
    {
        get
        {
            return (int)optionSlider.value;
        }
        set
        {
            this.optionSlider.value = value;
        }
    }

    public void Init(int locMStId, int min, int max, int curr)
    {
        this.optionTitle.text = GameManager.Instance.dbHandler.LocMstDic[locMStId][GameManager.Instance.gdPlayerData.Lang];
        this.optionMin.text = min.ToString();
        this.optionMax.text = max.ToString();
        this.optionCurr.text = curr.ToString();

        optionSlider.minValue = min;
        optionSlider.maxValue = max;
        optionSlider.value = curr;
        optionSlider.onValueChanged.AddListener(x =>
        {
            if (playSound)
                GameManager.Instance.PlayBtnClickSound();

            optionCurr.text = ((int)x).ToString();
        });
    }

    public void SetRandom()
    {
        playSound = false;

        optionSlider.value = Random.Range(optionSlider.minValue, optionSlider.maxValue);

        playSound = true;
    }
}