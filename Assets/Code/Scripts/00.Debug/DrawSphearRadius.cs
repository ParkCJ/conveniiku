using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawSphearRadius : MonoBehaviour
{
    // 이 스크립트를 확인할 Object에 적용하고, radius를 지정한다.
    // Use the same vars you use to draw your Overlap SPhere to draw your Wire Sphere.
    public float radius;

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, radius);
    }
}
