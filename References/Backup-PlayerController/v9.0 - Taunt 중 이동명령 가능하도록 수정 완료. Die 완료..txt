using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    // Move
    float maxSpeed = 1;          // 최대 속도
    float maxVelocity = 1;       // 최대 가속도 (0일 경우, 가속도가 0가 된다.)
    float maxNgSpeed = 0.1f;     // 최대 속도 (무중력)
    float maxNgVelocity = 0.1f;  // 최대 가속도 (무중력, 0일 경우, 가속도가 0가 된다.)

    // Jump
    float jumpForce = 5;
    float jumpInterval = 1f;
    bool isJumping = false;

    // Attack
    float attackForce = 300f;
    float attackInterval = 1f;
    float attackDistanceRate = 1.2f;
    bool isAttacking = false;

    // Taunt
    float tauntDuration = 5f;
    bool isTaunting = false;
    Coroutine tauntCrout;

    // Die
    float dieDuration = 5f;
    bool isDead = false;

    BoxCollider cl;
    Rigidbody rb;

    void Awake()
    {
        cl = GetComponent<BoxCollider>();
        rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        if (isDead) // Constraints
            return;

        if (rb.useGravity)  // Constraints
        {
            if (IsOnTheGround()) // Constraints
            {
                // Move
                if (Input.GetButton("Horizontal") || Input.GetButton("Vertical"))
                    Move();

                // Jump
                if (Input.GetButton("Jump") && !isJumping)
                    StartCoroutine(Jump());

                // Attack
                if (Input.GetButton("Fire1") && !isAttacking)
                    StartCoroutine(Attack());

                // Taunt
                if (Input.GetButton("Fire2") && !isTaunting)
                    tauntCrout = StartCoroutine(Taunt());

                // Die
                if (Input.GetButton("Fire3"))
                    StartCoroutine(Die());
            }
            else
            {
                // do nothing
            }
        }
        else
        {
            // Move (No gravity)
            if (Input.GetButton("Horizontal") || Input.GetButton("Vertical"))
                MoveNoGravity();
        }
    }

    void Move()
    {
        if (isTaunting)
            StopTaunt();

        Vector3 dir = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical")).normalized;

        // Head to direction
        transform.LookAt(transform.position + dir);

        // Move to direction
        rb.AddForce(dir * maxSpeed, ForceMode.Impulse);
        rb.velocity = Vector3.ClampMagnitude(rb.velocity, maxVelocity);
    }

    void MoveNoGravity()
    {
        if (isTaunting)
            StopTaunt();

        Vector3 dir = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical")).normalized;

        // Head to direction
        transform.LookAt(transform.position + dir);

        // Move to direction
        rb.AddForce(dir * maxNgSpeed, ForceMode.Impulse);
        rb.velocity = Vector3.ClampMagnitude(rb.velocity, maxNgVelocity);
    }

    IEnumerator Jump()
    {
        if (isTaunting)
            StopTaunt();

        isJumping = true;

        rb.AddForce(transform.up * jumpForce, ForceMode.Impulse);

        yield return new WaitForSeconds(jumpInterval);

        isJumping = false;
    }

    IEnumerator Attack()
    {
        if (isTaunting)
            StopTaunt();

        isAttacking = true;

        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, cl.bounds.extents.z * attackDistanceRate))
        {
            if (hit.rigidbody != null)
                hit.rigidbody.AddForce(transform.forward * attackForce, ForceMode.Impulse);
        }

        yield return new WaitForSeconds(attackInterval);

        isAttacking = false;
    }

    IEnumerator Taunt()
    {
        Debug.Log("Taunt started");

        isTaunting = true;

        float tempDuration = tauntDuration * 1.0f / Time.deltaTime;
        Debug.Log(tempDuration);

        while (tempDuration > 0)
        {
            transform.Rotate(transform.up);
            yield return new WaitForFixedUpdate();
            tempDuration--;
        }

        Vector3 defRotation = transform.rotation.eulerAngles;
        defRotation.x = 0;
        defRotation.z = 0;
        transform.rotation = Quaternion.Euler(defRotation);

        isTaunting = false;
    }

    IEnumerator Die()
    {
        Debug.Log("Die started");

        if (isTaunting)
            StopTaunt();

        isDead = true;

        // Tell to gameManager that player is dead
        // gameManager.HandlePlayerDead();

        transform.Rotate(transform.right, 90);

        yield return new WaitForSeconds(dieDuration);

        Destroy(this);
    }

    bool IsOnTheGround()
    {
        /*******************************
         * 현재의 한계: z, x축으로 기울어진 경우는 꼭지점이 땅에 붙어있어도, false를 리턴한다.
         * 그래서 Taunt 중 z, x축으로 기울어지는 Taunt를 할 경우, 바로 이동이 안되게 된다.
        *******************************/
        return Physics.Raycast(transform.position, -transform.up, cl.bounds.extents.y);
        /*
        RaycastHit hit;
        Ray downRay = new Ray(transform.position, -transform.up);

        return Physics.Raycast(downRay, out hit) && Mathf.Approximately(cl.bounds.extents.y, hit.distance);
        */
    }

    void StopTaunt()
    {
        StopCoroutine(tauntCrout);

        // Restore rotation except for y
        Vector3 defRotation = transform.rotation.eulerAngles;
        defRotation.x = 0;
        defRotation.z = 0;
        transform.rotation = Quaternion.Euler(defRotation);

        isTaunting = false;
    }
}
