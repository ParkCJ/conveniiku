﻿![picture](WebFiles/Re-TitleImage.JPG) 

田舎にすむひよりはおかしが好きです。

今日はおかしを買いにコンビニに行きます。（遠いけど）

ひよりはコンビニにうまくいけますでしょうか。

---

## Unity Play Showcase

■ v2がMade With Unity 2021 December ShowcaseでFeatured作品に選ばれました。

https://play.unity.com/discover/showcases/december-showcase-2021


■ v2が引き続きUnityTechのTwitchチャンネル「Let's Play! December Showcase | Made With Unity」で紹介されました。

https://www.twitch.tv/videos/1262296016

（ 21 分 から。）


■ v1がUnityTechのTwitchチャンネル「Let's Play! November Showcase | Made With Unity」で紹介されました。:)

凄く嬉しいです。:)

https://www.twitch.tv/videos/1229653439

（ 21 分 09 秒から。)

---

## Play on the

Unity Play

Re [https://play.unity.com/mg/other/i-m-going-shopping-re](https://play.unity.com/mg/other/i-m-going-shopping-re)

Old versions

v3 [https://play.unity.com/mg/other/i-m-going-shopping-v3](https://play.unity.com/mg/other/i-m-going-shopping-v3)

v2 [https://play.unity.com/mg/other/i-m-going-shopping-v2](https://play.unity.com/mg/other/i-m-going-shopping-v2)

v1 [https://play.unity.com/mg/other/i-m-going-shopping](https://play.unity.com/mg/other/i-m-going-shopping)

---

## Made with

Unity 2020.3.20f1 (LTS), C#

---

## About a game

ひよりになってコンビニに行くアドベンチャーゲームです。

途中で1x1の大きさから5x5の大きさまで様々なトラップが待っています。

ドキドキしながらトラップを楽しんで見ましょう。

---

## Features

3D

アドベンチャー

様々なトラップ

マップ自動生成

ローカライズ (日本語、英語、韓国語)

オープニングテーマ (Rizwan Ashraf - POTATO)

エンディングテーマ (Rizwan Ashraf - CHUNKY MONKEY)

---

## Platform

WebGL (PC)

---

## Source code

[https://bitbucket.org/ParkCJ/conveniiku/src/master/](https://bitbucket.org/ParkCJ/conveniiku/src/master/)

---

## Created by

Park Changjoo (パク チャングジュ)

---

## Asset, Font

Character

1. Course Library - Unity Learn Junior Programmer Pathway
2. Free Low Polygon_Animal - GLOOMY STUDIO

Animation

1. Course Library - Unity Learn Junior Programmer Pathway
2. Animations - Mixamo

Particle

1. Course Library - Unity Learn Junior Programmer Pathway
2. Polygonal's Low-Poly Particle Pack - Polygonal Stuff

Sound

1. Course Library - Unity Learn Junior Programmer Pathway
2. 8-Bit Sfx - Little Robot Sound Factory
3. FREE Casual Game SFX Pack - Dustyroom
4. Free Footsteps System - Pavel Cristian
5. Monster SFX - 111518 - The Octoverse
6. Free Music Tracks For Games - Rizwan Ashraf
7. Post Apocalypse Guns Demo - Sound Earth Game Audio
8. Shooting Sound - B.G.M
9. Mai Free Voices - nagisa.f (nagisa.f creative)

Font

1. HachiMaruPop-Regular - Nonty
2. HiMelody-Regular - YoonDesign Inc
3. NotoSansJP-Regular - Google
4. NotoSansKR-Regular - Google

---

## References

1. Frozen [https://en.wikipedia.org/wiki/Frozen_(2013_film)](https://en.wikipedia.org/wiki/Frozen_(2013_film))
2. Nadia: The Secret of Blue Water [https://en.wikipedia.org/wiki/Nadia:_The_Secret_of_Blue_Water](https://en.wikipedia.org/wiki/Nadia:_The_Secret_of_Blue_Water)

---

## Screenshot

ローカライズ

![img1](WebFiles/Re-MultiLanguageImage.JPG)

プレイ

![img3](WebFiles/Re-PlayImage1.JPG)

ツアーモード

![img4](WebFiles/Re-PlayImage2.JPG)

時間変更

![img4](WebFiles/Re-PlayImage4.JPG)

スタッフロール

![img5](WebFiles/Re-PlayImage3.JPG)